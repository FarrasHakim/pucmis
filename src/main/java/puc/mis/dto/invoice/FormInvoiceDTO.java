package puc.mis.dto.invoice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import puc.mis.model.Views;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonView(Views.Public.class)
public class FormInvoiceDTO {
    private Integer id;
    private String jenis;
    private Integer beritaAcaraPoId;
    private Integer beritaAcaraServisId;
    private String penerimaBaId;
    private String nomor;
    private String tanggalTerbit;
    private String tanggalKirim;
    private String tanggalKembali;
    private String tanggalJatuhTempo;
    private Long jumlahTagihan;
    private Long jumlahPembayaran;
    private Integer statusInvoiceId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public Integer getBeritaAcaraPoId() {
        return beritaAcaraPoId;
    }

    public void setBeritaAcaraPoId(Integer beritaAcaraPoId) {
        this.beritaAcaraPoId = beritaAcaraPoId;
    }

    public Integer getBeritaAcaraServisId() {
        return beritaAcaraServisId;
    }

    public void setBeritaAcaraServisId(Integer beritaAcaraServisId) {
        this.beritaAcaraServisId = beritaAcaraServisId;
    }

    public String getPenerimaBaId() {
        return penerimaBaId;
    }

    public void setPenerimaBaId(String penerimaBaId) {
        this.penerimaBaId = penerimaBaId;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getTanggalTerbit() {
        return tanggalTerbit;
    }

    public void setTanggalTerbit(String tanggalTerbit) {
        this.tanggalTerbit = tanggalTerbit;
    }

    public String getTanggalKirim() {
        return tanggalKirim;
    }

    public void setTanggalKirim(String tanggalKirim) {
        this.tanggalKirim = tanggalKirim;
    }

    public String getTanggalKembali() {
        return tanggalKembali;
    }

    public void setTanggalKembali(String tanggalKembali) {
        this.tanggalKembali = tanggalKembali;
    }

    public String getTanggalJatuhTempo() {
        return tanggalJatuhTempo;
    }

    public void setTanggalJatuhTempo(String tanggalJatuhTempo) {
        this.tanggalJatuhTempo = tanggalJatuhTempo;
    }

    public Long getJumlahTagihan() {
        return jumlahTagihan;
    }

    public void setJumlahTagihan(Long jumlahTagihan) {
        this.jumlahTagihan = jumlahTagihan;
    }

    public Long getJumlahPembayaran() {
        return jumlahPembayaran;
    }

    public void setJumlahPembayaran(Long jumlahPembayaran) {
        this.jumlahPembayaran = jumlahPembayaran;
    }

    public Integer getStatusInvoiceId() {
        return statusInvoiceId;
    }

    public void setStatusInvoiceId(Integer statusInvoiceId) {
        this.statusInvoiceId = statusInvoiceId;
    }
}
