package puc.mis.dto.beritaAcara;

public class FormBeritaAcaraServisDTO {
    private Integer id;
    private Integer kontrakServisId;
    private String nomorSpk;
    private Integer periodeSpk;
    private Integer statusBeritaAcaraId;
    private Integer invoiceId;
    private String tanggalTerbit;
    private String tanggalKirim;
    private String tanggalKembali;
    private String periodeBeritaAcara;
    private boolean isApprove;

    public String getTanggalTerbit() {
        return tanggalTerbit;
    }

    public void setTanggalTerbit(String tanggalTerbit) {
        this.tanggalTerbit = tanggalTerbit;
    }

    public String getTanggalKirim() {
        return tanggalKirim;
    }

    public void setTanggalKirim(String tanggalKirim) {
        this.tanggalKirim = tanggalKirim;
    }

    public String getTanggalKembali() {
        return tanggalKembali;
    }

    public void setTanggalKembali(String tanggalKembali) {
        this.tanggalKembali = tanggalKembali;
    }

    public String getPeriodeBeritaAcara() {
        return periodeBeritaAcara;
    }

    public void setPeriodeBeritaAcara(String periodeBeritaAcara) {
        this.periodeBeritaAcara = periodeBeritaAcara;
    }

    public boolean isApprove() {
        return isApprove;
    }

    public boolean getApprove() {
        return isApprove;
    }

    public void setApprove(boolean approve) {
        isApprove = approve;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKontrakServisId() {
        return kontrakServisId;
    }

    public void setKontrakServisId(Integer kontrakServisId) {
        this.kontrakServisId = kontrakServisId;
    }

    public String getNomorSpk() {
        return nomorSpk;
    }

    public void setNomorSpk(String nomorSpk) {
        this.nomorSpk = nomorSpk;
    }

    public Integer getPeriodeSpk() {
        return periodeSpk;
    }

    public void setPeriodeSpk(Integer periodeSpk) {
        this.periodeSpk = periodeSpk;
    }

    public Integer getStatusBeritaAcaraId() {
        return statusBeritaAcaraId;
    }

    public void setStatusBeritaAcaraId(Integer statusBeritaAcaraId) {
        this.statusBeritaAcaraId = statusBeritaAcaraId;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }
}
