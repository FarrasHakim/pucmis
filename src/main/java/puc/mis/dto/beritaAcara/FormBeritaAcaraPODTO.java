package puc.mis.dto.beritaAcara;

public class FormBeritaAcaraPODTO {
    private Integer id;
    private Integer purchaseOrderId;
    private String nomorBa;
    private String jenisPekerjaan;
    private String jenisBeritaAcara;
    private String tanggalTerbit;
    private String tanggalKirim;
    private String tanggalKembali;
    private Integer statusBeritaAcaraId;
    private Integer invoiceId;
    private String periodeBeritaAcara;
    private boolean isApprove;

    public boolean isApprove() {
        return isApprove;
    }

    public boolean getApprove() {
        return isApprove;
    }

    public void setApprove(boolean approve) {
        isApprove = approve;
    }

    public String getPeriodeBeritaAcara() {
        return periodeBeritaAcara;
    }

    public void setPeriodeBeritaAcara(String periodeBeritaAcara) {
        this.periodeBeritaAcara = periodeBeritaAcara;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(Integer purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public String getNomorBa() {
        return nomorBa;
    }

    public void setNomorBa(String nomorBa) {
        this.nomorBa = nomorBa;
    }

    public String getJenisPekerjaan() {
        return jenisPekerjaan;
    }

    public void setJenisPekerjaan(String jenisPekerjaan) {
        this.jenisPekerjaan = jenisPekerjaan;
    }

    public String getJenisBeritaAcara() {
        return jenisBeritaAcara;
    }

    public void setJenisBeritaAcara(String jenisBeritaAcara) {
        this.jenisBeritaAcara = jenisBeritaAcara;
    }

    public String getTanggalTerbit() {
        return tanggalTerbit;
    }

    public void setTanggalTerbit(String tanggalTerbit) {
        this.tanggalTerbit = tanggalTerbit;
    }

    public String getTanggalKirim() {
        return tanggalKirim;
    }

    public void setTanggalKirim(String tanggalKirim) {
        this.tanggalKirim = tanggalKirim;
    }

    public String getTanggalKembali() {
        return tanggalKembali;
    }

    public void setTanggalKembali(String tanggalKembali) {
        this.tanggalKembali = tanggalKembali;
    }

    public Integer getStatusBeritaAcaraId() {
        return statusBeritaAcaraId;
    }

    public void setStatusBeritaAcaraId(Integer statusBeritaAcaraId) {
        this.statusBeritaAcaraId = statusBeritaAcaraId;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }
}
