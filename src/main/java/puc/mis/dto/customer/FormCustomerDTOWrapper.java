package puc.mis.dto.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import puc.mis.model.Views;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonView(Views.Public.class)
public class FormCustomerDTOWrapper {
    private List<FormCustomerDTO> customerList;
    private Integer idCabang;
    private Integer idArea;

    public List<FormCustomerDTO> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<FormCustomerDTO> customerList) {
        this.customerList = customerList;
    }

    public Integer getIdCabang() {
        return idCabang;
    }

    public void setIdCabang(Integer idCabang) {
        this.idCabang = idCabang;
    }

    public Integer getIdArea() {
        return idArea;
    }

    public void setIdArea(Integer idArea) {
        this.idArea = idArea;
    }
}
