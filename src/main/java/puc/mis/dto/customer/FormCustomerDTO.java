package puc.mis.dto.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import puc.mis.model.Views;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonView(Views.Public.class)
public class FormCustomerDTO {
    private Integer id;
    private String nama;
    private String alamat;
    private String nomorTelepon;
    private String nomorFax;
    private String email;
    private Integer cabangId;
    private Integer areaId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNomorTelepon() {
        return nomorTelepon;
    }

    public void setNomorTelepon(String nomorTelepon) {
        this.nomorTelepon = nomorTelepon;
    }

    public String getNomorFax() {
        return nomorFax;
    }

    public void setNomorFax(String nomorFax) {
        this.nomorFax = nomorFax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getCabangId() {
        return cabangId;
    }

    public void setCabangId(Integer cabangId) {
        this.cabangId = cabangId;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }
}
