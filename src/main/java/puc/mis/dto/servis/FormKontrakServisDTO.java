package puc.mis.dto.servis;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import puc.mis.model.Views;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonView(Views.Public.class)
public class FormKontrakServisDTO {
    private Integer id;
    private String nomorKontrak;
    private Integer customerId;
    private List<UnitServisDTO> daftarUnitServis;
    private String periodeAwal;
    private String periodeAkhir;
    private Integer frekuensiPerBulan;
    private String penanggungJawabId;
    private Integer statusKontrakServisId;
    private String pembuatKontrakId;
    private String alamatServis;

    public String getAlamatServis() {
        return alamatServis;
    }

    public void setAlamatServis(String alamatServis) {
        this.alamatServis = alamatServis;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomorKontrak() {
        return nomorKontrak;
    }

    public void setNomorKontrak(String nomorKontrak) {
        this.nomorKontrak = nomorKontrak;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public List<UnitServisDTO> getDaftarUnitServis() {
        return daftarUnitServis;
    }

    public void setDaftarUnitServis(List<UnitServisDTO> daftarUnitServis) {
        this.daftarUnitServis = daftarUnitServis;
    }

    public String getPeriodeAwal() {
        return periodeAwal;
    }

    public void setPeriodeAwal(String periodeAwal) {
        this.periodeAwal = periodeAwal;
    }

    public String getPeriodeAkhir() {
        return periodeAkhir;
    }

    public void setPeriodeAkhir(String periodeAkhir) {
        this.periodeAkhir = periodeAkhir;
    }

    public Integer getFrekuensiPerBulan() {
        return frekuensiPerBulan;
    }

    public void setFrekuensiPerBulan(Integer frekuensiPerBulan) {
        this.frekuensiPerBulan = frekuensiPerBulan;
    }

    public String getPenanggungJawabId() {
        return penanggungJawabId;
    }

    public void setPenanggungJawabId(String penanggungJawabId) {
        this.penanggungJawabId = penanggungJawabId;
    }

    public Integer getStatusKontrakServisId() {
        return statusKontrakServisId;
    }

    public void setStatusKontrakServisId(Integer statusKontrakServisId) {
        this.statusKontrakServisId = statusKontrakServisId;
    }

    public String getPembuatKontrakId() {
        return pembuatKontrakId;
    }

    public void setPembuatKontrakId(String pembuatKontrakId) {
        this.pembuatKontrakId = pembuatKontrakId;
    }
}
