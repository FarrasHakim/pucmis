package puc.mis.dto.servis;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import puc.mis.model.Views;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonView(Views.Public.class)
public class FormJadwalServisDTO {
    private Integer id;
    private Integer kontrakServisId;
    private String tanggalServis;
    private String tanggalPelaksanaan;
    private String tanggalTerimaFormFisik;
    private Integer statusJadwalServisId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKontrakServisId() {
        return kontrakServisId;
    }

    public void setKontrakServisId(Integer kontrakServisId) {
        this.kontrakServisId = kontrakServisId;
    }

    public String getTanggalServis() {
        return tanggalServis;
    }

    public void setTanggalServis(String tanggalServis) {
        this.tanggalServis = tanggalServis;
    }

    public String getTanggalPelaksanaan() {
        return tanggalPelaksanaan;
    }

    public void setTanggalPelaksanaan(String tanggalPelaksanaan) {
        this.tanggalPelaksanaan = tanggalPelaksanaan;
    }

    public String getTanggalTerimaFormFisik() {
        return tanggalTerimaFormFisik;
    }

    public void setTanggalTerimaFormFisik(String tanggalTerimaFormFisik) {
        this.tanggalTerimaFormFisik = tanggalTerimaFormFisik;
    }

    public Integer getStatusJadwalServisId() {
        return statusJadwalServisId;
    }

    public void setStatusJadwalServisId(Integer statusJadwalServisId) {
        this.statusJadwalServisId = statusJadwalServisId;
    }
}
