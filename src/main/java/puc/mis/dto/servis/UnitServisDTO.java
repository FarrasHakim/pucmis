package puc.mis.dto.servis;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import puc.mis.model.Views;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonView(Views.Public.class)
public class UnitServisDTO {
    private Integer quantity;
    private String unit;
    private String merk;

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }
}
