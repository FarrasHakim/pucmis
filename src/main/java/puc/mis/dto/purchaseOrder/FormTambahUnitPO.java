package puc.mis.dto.purchaseOrder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import puc.mis.model.Views;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonView(Views.Public.class)
public class FormTambahUnitPO {
    private Integer id;
    private Integer unitPOId;
    private Integer jumlahDipesan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getJumlahDipesan() {
        return jumlahDipesan;
    }

    public void setJumlahDipesan(Integer jumlahDipesan) {
        this.jumlahDipesan = jumlahDipesan;
    }

    public Integer getUnitPOId() {
        return unitPOId;
    }

    public void setUnitPOId(Integer unitPOId) {
        this.unitPOId = unitPOId;
    }
}
