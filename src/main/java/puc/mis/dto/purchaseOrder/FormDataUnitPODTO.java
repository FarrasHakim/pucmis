package puc.mis.dto.purchaseOrder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import puc.mis.model.Views;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonView(Views.Public.class)
public class FormDataUnitPODTO {
    private Integer id;
    private Integer jumlahDipesan;
    private Integer purchaseOrderId;
    private Integer unitPoId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getJumlahDipesan() {
        return jumlahDipesan;
    }

    public void setJumlahDipesan(Integer jumlahDipesan) {
        this.jumlahDipesan = jumlahDipesan;
    }

    public Integer getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(Integer purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public Integer getUnitPoId() {
        return unitPoId;
    }

    public void setUnitPoId(Integer unitPoId) {
        this.unitPoId = unitPoId;
    }
}
