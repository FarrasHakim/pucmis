package puc.mis.dto.purchaseOrder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import puc.mis.model.Views;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonView(Views.Public.class)
public class FormPurchaseOrderDTO {
    private Integer id;
    private String nomor;
    private Integer customerId;
    private String jenisPekerjaan;
    private String keterangan;
    private String tanggalTerima;
    private String nomorFaktur;
    private String tanggalPemasanganSparepart;
    private String tanggalPengirimanSparepart;
    private String penanggungjawabId;
    private Integer statusPOId;
    private Integer statusSparepartId;
    private String alamatPemasangan;
    private List<FormTambahUnitPO> daftarDataUnitPO;

    public List<FormTambahUnitPO> getDaftarDataUnitPO() {
        return daftarDataUnitPO;
    }

    public void setDaftarDataUnitPO(List<FormTambahUnitPO> daftarDataUnitPO) {
        this.daftarDataUnitPO = daftarDataUnitPO;
    }

    public String getAlamatPemasangan() {
        return alamatPemasangan;
    }

    public void setAlamatPemasangan(String alamatPemasangan) {
        this.alamatPemasangan = alamatPemasangan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getJenisPekerjaan() {
        return jenisPekerjaan;
    }

    public void setJenisPekerjaan(String jenisPekerjaan) {
        this.jenisPekerjaan = jenisPekerjaan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getTanggalTerima() {
        return tanggalTerima;
    }

    public void setTanggalTerima(String tanggalTerima) {
        this.tanggalTerima = tanggalTerima;
    }

    public String getNomorFaktur() {
        return nomorFaktur;
    }

    public void setNomorFaktur(String nomorFaktur) {
        this.nomorFaktur = nomorFaktur;
    }

    public String getTanggalPemasanganSparepart() {
        return tanggalPemasanganSparepart;
    }

    public void setTanggalPemasanganSparepart(String tanggalPemasanganSparepart) {
        this.tanggalPemasanganSparepart = tanggalPemasanganSparepart;
    }

    public String getTanggalPengirimanSparepart() {
        return tanggalPengirimanSparepart;
    }

    public void setTanggalPengirimanSparepart(String tanggalPengirimanSparepart) {
        this.tanggalPengirimanSparepart = tanggalPengirimanSparepart;
    }

    public String getPenanggungjawabId() {
        return penanggungjawabId;
    }

    public void setPenanggungjawabId(String penanggungjawabId) {
        this.penanggungjawabId = penanggungjawabId;
    }

    public Integer getStatusPOId() {
        return statusPOId;
    }

    public void setStatusPOId(Integer statusPoId) {
        this.statusPOId = statusPoId;
    }

    public Integer getStatusSparepartId() {
        return statusSparepartId;
    }

    public void setStatusSparepartId(Integer statusSparepartId) {
        this.statusSparepartId = statusSparepartId;
    }
}
