package puc.mis.dto.purchaseOrder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import puc.mis.model.DataUnitPOModel;
import puc.mis.model.Views;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonView(Views.Public.class)
public class FormUnitPODTO {
    private Integer id;
    private String nama;
    private List<DataUnitPOModel> daftarUnit;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<DataUnitPOModel> getDaftarUnit() {
        return daftarUnit;
    }

    public void setDaftarUnit(List<DataUnitPOModel> daftarUnit) {
        this.daftarUnit = daftarUnit;
    }
}
