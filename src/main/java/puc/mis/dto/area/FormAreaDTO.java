package puc.mis.dto.area;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import puc.mis.model.Views;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonView(Views.Public.class)
public class FormAreaDTO {
    private Integer id;
    private String nama;
    private String alamat;
    private Integer cabang;
    private String kepalaArea;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Integer getCabang() {
        return cabang;
    }

    public void setCabang(Integer cabang) {
        this.cabang = cabang;
    }

    public String getKepalaArea() {
        return kepalaArea;
    }

    public void setKepalaArea(String kepalaArea) {
        this.kepalaArea = kepalaArea;
    }
}
