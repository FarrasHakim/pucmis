package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "purchaseOrder")
public class PurchaseOrderModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(name = "nomor", nullable = false)
    @JsonView(Views.Public.class)
    private String nomor;

    @ManyToOne
    @JoinColumn(name = "id_customer", nullable = false)
    @JsonView(Views.PurchaseOrder.class)
    private CustomerModel customer;

    @NotNull
    @Column(name = "jenisPekerjaan", nullable = false)
    @JsonView(Views.Public.class)
    private String jenisPekerjaan;

    @OneToMany(mappedBy = "purchaseOrder", cascade = CascadeType.ALL)
    @JsonView(Views.PurchaseOrder.class)
    private List<DataUnitPOModel> daftarUnit;

    @Column(name = "keterangan")
    @JsonView(Views.Public.class)
    private String keterangan;

    @NotNull
    @Column(name = "tanggalTerima", nullable = false)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalTerima;

    @NotNull
    @Column(name = "nomorFaktur", nullable = false)
    @JsonView(Views.Public.class)
    private String nomorFaktur;

    @Column(name = "tanggalPemasanganSparepart")
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalPemasanganSparepart;

    @Column(name = "tanggalPengirimanSparepart")
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalPengirimanSparepart;

    @OneToMany(mappedBy = "purchaseOrder", cascade = CascadeType.ALL)
    @JsonView(Views.PurchaseOrder.class)
    private List<BeritaAcaraPOModel> daftarBeritaAcara;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_penanggungjawab", nullable = false)
    @JsonView(Views.Public.class)
    private KaryawanModel penanggungjawab;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_statusPo", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(Views.Public.class)
    private StatusPOModel statusPo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_statusSparepart", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(Views.Public.class)
    private StatusSparepartModel statusSparepart;

    @NotNull
    @Column(name = "alamatPemasangan", nullable = false)
    @JsonView(Views.Public.class)
    private String alamatPemasangan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public CustomerModel getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerModel customer) {
        this.customer = customer;
    }

    public String getJenisPekerjaan() {
        return jenisPekerjaan;
    }

    public void setJenisPekerjaan(String jenisPekerjaan) {
        this.jenisPekerjaan = jenisPekerjaan;
    }

    public List<DataUnitPOModel> getDaftarUnit() {
        return daftarUnit;
    }

    public void setDaftarUnit(List<DataUnitPOModel> daftarUnit) {
        this.daftarUnit = daftarUnit;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public LocalDate getTanggalTerima() {
        return tanggalTerima;
    }

    public void setTanggalTerima(LocalDate tanggalTerima) {
        this.tanggalTerima = tanggalTerima;
    }

    public String getNomorFaktur() {
        return nomorFaktur;
    }

    public void setNomorFaktur(String nomorFaktur) {
        this.nomorFaktur = nomorFaktur;
    }

    public LocalDate getTanggalPemasanganSparepart() {
        return tanggalPemasanganSparepart;
    }

    public void setTanggalPemasanganSparepart(LocalDate tanggalPemasanganSparepart) {
        this.tanggalPemasanganSparepart = tanggalPemasanganSparepart;
    }

    public LocalDate getTanggalPengirimanSparepart() {
        return tanggalPengirimanSparepart;
    }

    public void setTanggalPengirimanSparepart(LocalDate tanggalPengirimanSparepart) {
        this.tanggalPengirimanSparepart = tanggalPengirimanSparepart;
    }

    public List<BeritaAcaraPOModel> getDaftarBeritaAcara() {
        return daftarBeritaAcara;
    }

    public void setDaftarBeritaAcara(List<BeritaAcaraPOModel> daftarBeritaAcara) {
        this.daftarBeritaAcara = daftarBeritaAcara;
    }

    public KaryawanModel getPenanggungjawab() {
        return penanggungjawab;
    }

    public void setPenanggungjawab(KaryawanModel penanggungjawab) {
        this.penanggungjawab = penanggungjawab;
    }

    public StatusPOModel getStatusPo() {
        return statusPo;
    }

    public void setStatusPo(StatusPOModel statusPo) {
        this.statusPo = statusPo;
    }

    public StatusSparepartModel getStatusSparepart() {
        return statusSparepart;
    }

    public void setStatusSparepart(StatusSparepartModel statusSparepart) {
        this.statusSparepart = statusSparepart;
    }

    public String getAlamatPemasangan() {
        return alamatPemasangan;
    }

    public void setAlamatPemasangan(String alamatPemasangan) {
        this.alamatPemasangan = alamatPemasangan;
    }
}
