package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "statusBeritaAcara")
public class StatusBeritaAcaraModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(name = "status", nullable = false)
    @JsonView(Views.Public.class)
    private String status;

    @OneToMany(mappedBy = "status", fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(Views.StatusBeritaAcara.class)
    private List<BeritaAcaraPOModel> beritaAcaraPo;

    @OneToMany(mappedBy = "status", fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(Views.StatusBeritaAcara.class)
    private List<BeritaAcaraServisModel> beritaAcaraServis;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<BeritaAcaraPOModel> getBeritaAcaraPo() {
        return beritaAcaraPo;
    }

    public void setBeritaAcaraPo(List<BeritaAcaraPOModel> beritaAcaraPo) {
        this.beritaAcaraPo = beritaAcaraPo;
    }

    public List<BeritaAcaraServisModel> getBeritaAcaraServis() {
        return beritaAcaraServis;
    }

    public void setBeritaAcaraServis(List<BeritaAcaraServisModel> beritaAcaraServis) {
        this.beritaAcaraServis = beritaAcaraServis;
    }
}
