package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "area")
public class AreaModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(name = "nama", nullable = false)
    @JsonView(Views.Public.class)
    private String nama;

    @NotNull
    @Column(name = "alamat", nullable = false)
    @JsonView(Views.Public.class)
    private String alamat;

    @ManyToOne
    @JoinColumn(name = "cabang", nullable = false)
    @JsonView(Views.Public.class)
    private CabangModel cabang;

    @OneToMany(mappedBy = "area")
    @JsonView(Views.Area.class)
    private List<CustomerModel> daftarCustomer;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_kepalaArea", referencedColumnName = "uuid")
    @JsonView(Views.Area.class)
    private KaryawanModel kepalaArea;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public CabangModel getCabang() {
        return cabang;
    }

    public void setCabang(CabangModel cabang) {
        this.cabang = cabang;
    }

    public List<CustomerModel> getDaftarCustomer() {
        return daftarCustomer;
    }

    public void setDaftarCustomer(List<CustomerModel> daftarCustomer) {
        this.daftarCustomer = daftarCustomer;
    }

    public KaryawanModel getKepalaArea() {
        return kepalaArea;
    }

    public void setKepalaArea(KaryawanModel kepalaArea) {
        this.kepalaArea = kepalaArea;
    }
}
