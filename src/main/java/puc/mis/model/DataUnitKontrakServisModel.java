package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "dataUnitKontrakServis")
@Embeddable
public class DataUnitKontrakServisModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(name = "quantity", nullable = false)
    @JsonView(Views.Public.class)
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "id_kontrakServis")
    @JsonView(Views.DataUnitServis.class)
    private KontrakServisModel kontrakServis;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_unitKontrakServis")
    @JsonView(Views.DataUnitServis.class)
    private UnitKontrakServisModel unitKontrakServis;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_merkKontrakServis")
    @JsonView(Views.DataUnitServis.class)
    private MerkKontrakServisModel merkKontrakServis;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public KontrakServisModel getKontrakServis() {
        return kontrakServis;
    }

    public void setKontrakServis(KontrakServisModel kontrakServis) {
        this.kontrakServis = kontrakServis;
    }

    public UnitKontrakServisModel getUnitKontrakServis() {
        return unitKontrakServis;
    }

    public void setUnitKontrakServis(UnitKontrakServisModel unitKontrakServis) {
        this.unitKontrakServis = unitKontrakServis;
    }

    public MerkKontrakServisModel getMerkKontrakServis() {
        return merkKontrakServis;
    }

    public void setMerkKontrakServis(MerkKontrakServisModel merkKontrakServis) {
        this.merkKontrakServis = merkKontrakServis;
    }
}
