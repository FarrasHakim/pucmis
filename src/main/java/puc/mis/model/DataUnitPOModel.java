package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "dataUnitPo")
@Embeddable
public class DataUnitPOModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(name = "jumlahDipesan")
    @JsonView(Views.Public.class)
    private Integer jumlahDipesan;

    @ManyToOne
    @JoinColumn(name = "id_purchaseOrder")
    @JsonView(Views.DataUnitPO.class)
    private PurchaseOrderModel purchaseOrder;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_unitPo")
    @JsonView(Views.DataUnitPO.class)
    private UnitPOModel unitPo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getJumlahDipesan() {
        return jumlahDipesan;
    }

    public void setJumlahDipesan(Integer jumlahDipesan) {
        this.jumlahDipesan = jumlahDipesan;
    }

    public PurchaseOrderModel getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrderModel purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public UnitPOModel getUnitPo() {
        return unitPo;
    }

    public void setUnitPo(UnitPOModel unitPo) {
        this.unitPo = unitPo;
    }
}
