package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "bacaNotifikasi")
@Embeddable
public class BacaNotifikasiModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(columnDefinition = "boolean default false", name = "isRead", nullable = false)
    @JsonView(Views.Public.class)
    private Boolean isRead;

    @ManyToOne
    @JoinColumn(name = "uuid_karyawan")
    @JsonView(Views.BacaNotifikasi.class)
    private KaryawanModel karyawan;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_notifikasi")
    @JsonView(Views.BacaNotifikasi.class)
    private NotifikasiModel notifikasi;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public KaryawanModel getKaryawan() {
        return karyawan;
    }

    public void setKaryawan(KaryawanModel karyawan) {
        this.karyawan = karyawan;
    }

    public NotifikasiModel getNotifikasi() {
        return notifikasi;
    }

    public void setNotifikasi(NotifikasiModel notifikasi) {
        this.notifikasi = notifikasi;
    }
}
