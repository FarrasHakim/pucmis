package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "customer")
public class CustomerModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(name = "nama", nullable = false)
    @JsonView(Views.Public.class)
    private String nama;

    @NotNull
    @Column(name = "alamat", nullable = false)
    @JsonView(Views.Public.class)
    private String alamat;

    @NotNull
    @Column(name = "nomorTelepon", nullable = false)
    @JsonView(Views.Public.class)
    private String nomorTelepon;

    @Column(name = "nomorFax")
    @JsonView(Views.Public.class)
    private String nomorFax;

    @Column(name = "email")
    @JsonView(Views.Public.class)
    private String email;

    @ManyToOne
    @JoinColumn(name = "cabang", nullable = false)
    @JsonView(Views.Public.class)
    private CabangModel cabang;

    @ManyToOne
    @JoinColumn(name = "area")
    @JsonView(Views.Public.class)
    private AreaModel area;

    @OneToMany(mappedBy = "customer")
    @JsonView(Views.Customer.class)
    private List<PurchaseOrderModel> daftarPo;

    @OneToMany(mappedBy = "customer")
    @JsonView(Views.Customer.class)
    private List<KontrakServisModel> daftarKontrakServis;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNomorTelepon() {
        return nomorTelepon;
    }

    public void setNomorTelepon(String nomorTelepon) {
        this.nomorTelepon = nomorTelepon;
    }

    public String getNomorFax() {
        return nomorFax;
    }

    public void setNomorFax(String nomorFax) {
        this.nomorFax = nomorFax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CabangModel getCabang() {
        return cabang;
    }

    public void setCabang(CabangModel cabang) {
        this.cabang = cabang;
    }

    public AreaModel getArea() {
        return area;
    }

    public void setArea(AreaModel area) {
        this.area = area;
    }

    public List<PurchaseOrderModel> getDaftarPo() {
        return daftarPo;
    }

    public void setDaftarPo(List<PurchaseOrderModel> daftarPo) {
        this.daftarPo = daftarPo;
    }

    public List<KontrakServisModel> getDaftarKontrakServis() {
        return daftarKontrakServis;
    }

    public void setDaftarKontrakServis(List<KontrakServisModel> daftarKontrakServis) {
        this.daftarKontrakServis = daftarKontrakServis;
    }
}
