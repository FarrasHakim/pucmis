package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "merkKontrakServis")
public class MerkKontrakServisModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(name = "merk", nullable = false)
    @JsonView(Views.Public.class)
    private String merk;

    @OneToMany(mappedBy = "merkKontrakServis", cascade = CascadeType.ALL)
    @JsonView(Views.UnitServis.class)
    private List<DataUnitKontrakServisModel> daftarUnit;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public List<DataUnitKontrakServisModel> getDaftarUnit() {
        return daftarUnit;
    }

    public void setDaftarUnit(List<DataUnitKontrakServisModel> daftarUnit) {
        this.daftarUnit = daftarUnit;
    }
}
