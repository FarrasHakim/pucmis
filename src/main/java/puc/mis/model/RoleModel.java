package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "role")
public class RoleModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Size(max = 20)
    @Column(name = "nama", nullable = false)
    @JsonView(Views.Public.class)
    private String nama;

    @OneToMany(mappedBy = "role")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(Views.Role.class)
    private List<KaryawanModel> daftarKaryawan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public List<KaryawanModel> getDaftarKaryawan() {
        return daftarKaryawan;
    }

    public void setDaftarKaryawan(List<KaryawanModel> daftarKaryawan) {
        this.daftarKaryawan = daftarKaryawan;
    }
}
