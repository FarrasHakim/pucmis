package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "beritaAcaraServis")
public class BeritaAcaraServisModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "kontrakServis", nullable = false)
    @JsonView(Views.Public.class)
    private KontrakServisModel kontrakServis;

    @Column(name = "nomorSpk", nullable = true)
    @JsonView(Views.Public.class)
    private String nomorSpk;

    @Column(name = "periodeSpk", nullable = true)
    @JsonView(Views.Public.class)
    private Integer periodeSpk;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_status", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(Views.Public.class)
    private StatusBeritaAcaraModel status;

    @OneToOne(mappedBy = "beritaAcaraServis")
    @JsonView(Views.Public.class)
    private InvoiceModel invoice;

    @Column(name = "tanggalTerbit", nullable = true)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalTerbit;

    @Column(name = "tanggalKirim", nullable = true)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalKirim;

    @Column(name = "tanggalKembali")
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalKembali;

    @Column(name = "periodeBeritaAcara")
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate periodeBeritaAcara;

    @NotNull
    @Column(columnDefinition = "boolean default false", name = "isApprove", nullable = false)
    @JsonView(Views.Public.class)
    private Boolean isApprove;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public KontrakServisModel getKontrakServis() {
        return kontrakServis;
    }

    public void setKontrakServis(KontrakServisModel kontrakServis) {
        this.kontrakServis = kontrakServis;
    }

    public String getNomorSpk() {
        return nomorSpk;
    }

    public void setNomorSpk(String nomorSpk) {
        this.nomorSpk = nomorSpk;
    }

    public Integer getPeriodeSpk() {
        return periodeSpk;
    }

    public void setPeriodeSpk(Integer periodeSpk) {
        this.periodeSpk = periodeSpk;
    }

    public StatusBeritaAcaraModel getStatus() {
        return status;
    }

    public void setStatus(StatusBeritaAcaraModel status) {
        this.status = status;
    }

    public InvoiceModel getInvoice() {
        return invoice;
    }

    public void setInvoice(InvoiceModel invoice) {
        this.invoice = invoice;
    }

    public LocalDate getTanggalTerbit() {
        return tanggalTerbit;
    }

    public void setTanggalTerbit(LocalDate tanggalTerbit) {
        this.tanggalTerbit = tanggalTerbit;
    }

    public LocalDate getTanggalKirim() {
        return tanggalKirim;
    }

    public void setTanggalKirim(LocalDate tanggalKirim) {
        this.tanggalKirim = tanggalKirim;
    }

    public LocalDate getTanggalKembali() {
        return tanggalKembali;
    }

    public void setTanggalKembali(LocalDate tanggalKembali) {
        this.tanggalKembali = tanggalKembali;
    }

    public LocalDate getPeriodeBeritaAcara() {
        return periodeBeritaAcara;
    }

    public void setPeriodeBeritaAcara(LocalDate periodeBeritaAcara) {
        this.periodeBeritaAcara = periodeBeritaAcara;
    }

    public Boolean getApprove() {
        return isApprove;
    }

    public void setApprove(Boolean approve) {
        isApprove = approve;
    }
}
