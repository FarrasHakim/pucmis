package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "karyawan")
public class KaryawanModel implements Serializable {

    @Id
    @Size(max = 200)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @JsonView(Views.Karyawan.class)
    private String uuid;

    @NotNull
    @Size(max = 120)
    @Column(name = "nama", nullable = false)
    @JsonView(Views.Public.class)
    private String nama;

    @NotNull
    @Column(name = "alamat", nullable = false)
    @JsonView(Views.Public.class)
    private String alamat;

    @NotNull
    @Size(max = 20)
    @Column(name = "nomorTelepon", nullable = false)
    @JsonView(Views.Public.class)
    private String nomorTelepon;

    @NotNull
    @Size(max = 150)
    @Column(name = "email", nullable = false)
    @JsonView(Views.Public.class)
    private String email;

    @NotNull
    @Size(max = 120)
    @Column(name = "username", nullable = false)
    @JsonView(Views.Public.class)
    private String username;

    @NotNull
    @Lob
    @Size(max = 200)
    @Column(name = "password", nullable = false)
    @JsonIgnore
    private String password;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_role", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(Views.Public.class)
    private RoleModel role;

    @OneToMany(mappedBy = "karyawan", cascade = CascadeType.ALL)
    @JsonView(Views.Karyawan.class)
    private List<BacaNotifikasiModel> daftarNotifikasi;

    @ManyToOne
    @JoinColumn(name = "id_cabang", referencedColumnName = "id")
    @JsonView(Views.Karyawan.class)
    private CabangModel cabang;

    @OneToOne(mappedBy = "kepalaArea")
    @JsonView(Views.Karyawan.class)
    private AreaModel area;

    @OneToOne(mappedBy = "koordinator")
    @JsonView(Views.Karyawan.class)
    private CabangModel koorCabang;

    @OneToMany(mappedBy = "penanggungjawab", cascade = CascadeType.ALL)
    @JsonView(Views.Karyawan.class)
    private List<PurchaseOrderModel> daftarPo;

    @OneToMany(mappedBy = "penanggungJawab", cascade = CascadeType.ALL)
    @JsonView(Views.Karyawan.class)
    private List<KontrakServisModel> daftarKontrakServis;

    @OneToMany(mappedBy = "penerimaBa", cascade = CascadeType.ALL)
    @JsonView(Views.Karyawan.class)
    private List<InvoiceModel> daftarInvoice;

    @OneToMany(mappedBy = "pembuatKontrak", cascade = CascadeType.ALL)
    @JsonView(Views.Karyawan.class)
    private List<KontrakServisModel> daftarKontrakDibuat;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_status", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(Views.Public.class)
    private StatusKaryawanModel status;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNomorTelepon() {
        return nomorTelepon;
    }

    public void setNomorTelepon(String nomorTelepon) {
        this.nomorTelepon = nomorTelepon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleModel getRole() {
        return role;
    }

    public void setRole(RoleModel role) {
        this.role = role;
    }

    public List<BacaNotifikasiModel> getDaftarNotifikasi() {
        return daftarNotifikasi;
    }

    public void setDaftarNotifikasi(List<BacaNotifikasiModel> daftarNotifikasi) {
        this.daftarNotifikasi = daftarNotifikasi;
    }

    public CabangModel getCabang() {
        return cabang;
    }

    public void setCabang(CabangModel cabang) {
        this.cabang = cabang;
    }

    public AreaModel getArea() {
        return area;
    }

    public void setArea(AreaModel area) {
        this.area = area;
    }

    public CabangModel getKoorCabang() {
        return koorCabang;
    }

    public void setKoorCabang(CabangModel koorCabang) {
        this.koorCabang = koorCabang;
    }

    public List<PurchaseOrderModel> getDaftarPo() {
        return daftarPo;
    }

    public void setDaftarPo(List<PurchaseOrderModel> daftarPo) {
        this.daftarPo = daftarPo;
    }

    public List<KontrakServisModel> getDaftarKontrakServis() {
        return daftarKontrakServis;
    }

    public void setDaftarKontrakServis(List<KontrakServisModel> daftarKontrakServis) {
        this.daftarKontrakServis = daftarKontrakServis;
    }

    public List<InvoiceModel> getDaftarInvoice() {
        return daftarInvoice;
    }

    public void setDaftarInvoice(List<InvoiceModel> daftarInvoice) {
        this.daftarInvoice = daftarInvoice;
    }

    public List<KontrakServisModel> getDaftarKontrakDibuat() {
        return daftarKontrakDibuat;
    }

    public void setDaftarKontrakDibuat(List<KontrakServisModel> daftarKontrakDibuat) {
        this.daftarKontrakDibuat = daftarKontrakDibuat;
    }

    public StatusKaryawanModel getStatus() {
        return status;
    }

    public void setStatus(StatusKaryawanModel status) {
        this.status = status;
    }
}