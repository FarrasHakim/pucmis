package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "cabang")
public class CabangModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(name = "nama", nullable = false)
    @JsonView(Views.Public.class)
    private String nama;

    @NotNull
    @Column(name = "alamat", nullable = false)
    @JsonView(Views.Public.class)
    private String alamat;

    @OneToMany(mappedBy = "cabang")
    @JsonView(Views.Cabang.class)
    private List<KaryawanModel> daftarKaryawan;

    @OneToMany(mappedBy = "cabang")
    @JsonView(Views.Cabang.class)
    private List<AreaModel> daftarArea;

    @OneToMany(mappedBy = "cabang")
    @JsonView(Views.Cabang.class)
    private List<CustomerModel> daftarCustomer;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_koordinator", referencedColumnName = "uuid")
    @JsonView(Views.Cabang.class)
    private KaryawanModel koordinator;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public List<KaryawanModel> getDaftarKaryawan() {
        return daftarKaryawan;
    }

    public void setDaftarKaryawan(List<KaryawanModel> daftarKaryawan) {
        this.daftarKaryawan = daftarKaryawan;
    }

    public List<AreaModel> getDaftarArea() {
        return daftarArea;
    }

    public void setDaftarArea(List<AreaModel> daftarArea) {
        this.daftarArea = daftarArea;
    }

    public List<CustomerModel> getDaftarCustomer() {
        return daftarCustomer;
    }

    public void setDaftarCustomer(List<CustomerModel> daftarCustomer) {
        this.daftarCustomer = daftarCustomer;
    }

    public KaryawanModel getKoordinator() {
        return koordinator;
    }

    public void setKoordinator(KaryawanModel koordinator) {
        this.koordinator = koordinator;
    }
}
