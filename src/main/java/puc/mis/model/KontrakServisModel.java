package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "kontrakServis")
public class KontrakServisModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(name = "nomorKontrak", nullable = false)
    @JsonView(Views.Public.class)
    private String nomorKontrak;

    @ManyToOne
    @JoinColumn(name = "id_customer", nullable = false)
    @JsonView(Views.KontrakServis.class)
    private CustomerModel customer;

    @OneToMany(mappedBy = "kontrakServis", cascade = CascadeType.ALL)
    @JsonView(Views.KontrakServis.class)
    private List<DataUnitKontrakServisModel> daftarUnit;

    @NotNull
    @Column(name = "periodeAwal", nullable = false)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate periodeAwal;

    @NotNull
    @Column(name = "periodeAkhir", nullable = false)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate periodeAkhir;

    @NotNull
    @Column(name = "frekuensiPerBulan", nullable = false)
    @JsonView(Views.Public.class)
    private Integer frekuensiPerBulan;

    @OneToMany(mappedBy = "kontrakServis")
    @JsonView(Views.KontrakServis.class)
    private List<JadwalServisModel> daftarJadwalServis;

    @OneToMany(mappedBy = "kontrakServis")
    @JsonView(Views.KontrakServis.class)
    private List<BeritaAcaraServisModel> daftarBeritaAcara;

    @ManyToOne
    @JoinColumn(name = "penanggungJawab", nullable = false)
    @JsonView(Views.Public.class)
    private KaryawanModel penanggungJawab;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_status", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(Views.Public.class)
    private StatusKontrakServisModel status;

    @ManyToOne
    @JoinColumn(name = "pembuatKontrak", nullable = false)
    @JsonView(Views.Public.class)
    private KaryawanModel pembuatKontrak;

    @NotNull
    @Column(name = "alamatServis", nullable = false)
    @JsonView(Views.Public.class)
    private String alamatServis;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomorKontrak() {
        return nomorKontrak;
    }

    public void setNomorKontrak(String nomorKontrak) {
        this.nomorKontrak = nomorKontrak;
    }

    public CustomerModel getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerModel customer) {
        this.customer = customer;
    }

    public LocalDate getPeriodeAwal() {
        return periodeAwal;
    }

    public void setPeriodeAwal(LocalDate periodeAwal) {
        this.periodeAwal = periodeAwal;
    }

    public LocalDate getPeriodeAkhir() {
        return periodeAkhir;
    }

    public void setPeriodeAkhir(LocalDate periodeAkhir) {
        this.periodeAkhir = periodeAkhir;
    }

    public Integer getFrekuensiPerBulan() {
        return frekuensiPerBulan;
    }

    public void setFrekuensiPerBulan(Integer frekuensiPerBulan) {
        this.frekuensiPerBulan = frekuensiPerBulan;
    }

    public List<JadwalServisModel> getDaftarJadwalServis() {
        return daftarJadwalServis;
    }

    public void setDaftarJadwalServis(List<JadwalServisModel> daftarJadwalServis) {
        this.daftarJadwalServis = daftarJadwalServis;
    }

    public List<BeritaAcaraServisModel> getDaftarBeritaAcara() {
        return daftarBeritaAcara;
    }

    public void setDaftarBeritaAcara(List<BeritaAcaraServisModel> daftarBeritaAcara) {
        this.daftarBeritaAcara = daftarBeritaAcara;
    }

    public KaryawanModel getPenanggungJawab() {
        return penanggungJawab;
    }

    public void setPenanggungJawab(KaryawanModel penanggungJawab) {
        this.penanggungJawab = penanggungJawab;
    }

    public StatusKontrakServisModel getStatus() {
        return status;
    }

    public void setStatus(StatusKontrakServisModel status) {
        this.status = status;
    }

    public KaryawanModel getPembuatKontrak() {
        return pembuatKontrak;
    }

    public void setPembuatKontrak(KaryawanModel pembuatKontrak) {
        this.pembuatKontrak = pembuatKontrak;
    }

    public String getAlamatServis() {
        return alamatServis;
    }

    public void setAlamatServis(String alamatServis) {
        this.alamatServis = alamatServis;
    }

    public List<DataUnitKontrakServisModel> getDaftarUnit() {
        return daftarUnit;
    }

    public void setDaftarUnit(List<DataUnitKontrakServisModel> daftarUnit) {
        this.daftarUnit = daftarUnit;
    }
}
