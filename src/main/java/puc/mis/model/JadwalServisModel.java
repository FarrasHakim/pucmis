package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "jadwalServis")
public class JadwalServisModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "kontrakServis", nullable = false)
    @JsonView(Views.Public.class)
    private KontrakServisModel kontrakServis;

    @NotNull
    @Column(name = "tanggalDeadlineServis", nullable = false)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalDeadlineServis;

    @NotNull
    @Column(name = "tanggalPelaksanaan", nullable = false)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalPelaksanaan;

    @NotNull
    @Column(name = "tanggalTerimaFormFisik", nullable = false)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalTerimaFormFisik;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_status", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(Views.Public.class)
    private StatusJadwalServisModel status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public KontrakServisModel getKontrakServis() {
        return kontrakServis;
    }

    public void setKontrakServis(KontrakServisModel kontrakServis) {
        this.kontrakServis = kontrakServis;
    }

    public LocalDate getTanggalDeadlineServis() {
        return tanggalDeadlineServis;
    }

    public void setTanggalDeadlineServis(LocalDate tanggalDeadlineServis) {
        this.tanggalDeadlineServis = tanggalDeadlineServis;
    }

    public LocalDate getTanggalPelaksanaan() {
        return tanggalPelaksanaan;
    }

    public void setTanggalPelaksanaan(LocalDate tanggalPelaksanaan) {
        this.tanggalPelaksanaan = tanggalPelaksanaan;
    }

    public LocalDate getTanggalTerimaFormFisik() {
        return tanggalTerimaFormFisik;
    }

    public void setTanggalTerimaFormFisik(LocalDate tanggalTerimaFormFisik) {
        this.tanggalTerimaFormFisik = tanggalTerimaFormFisik;
    }

    public StatusJadwalServisModel getStatus() {
        return status;
    }

    public void setStatus(StatusJadwalServisModel status) {
        this.status = status;
    }
}
