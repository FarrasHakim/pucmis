package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "beritaAcaraPo")
public class BeritaAcaraPOModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "purchaseOrder", nullable = false)
    @JsonView(Views.Public.class)
    private PurchaseOrderModel purchaseOrder;

    @Column(name = "nomorBa", nullable = true)
    @JsonView(Views.Public.class)
    private String nomorBa;

    @NotNull
    @Column(name = "jenisPekerjaan", nullable = false)
    @JsonView(Views.Public.class)
    private String jenisPekerjaan;

    @Column(name = "jenisBeritaAcara", nullable = true)
    @JsonView(Views.Public.class)
    private String jenisBeritaAcara;

    @Column(name = "tanggalTerbit", nullable = true)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalTerbit;

    @Column(name = "tanggalKirim", nullable = true)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalKirim;

    @Column(name = "tanggalKembali", nullable = true)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalKembali;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_status", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(Views.Public.class)
    private StatusBeritaAcaraModel status;

    @OneToOne(mappedBy = "beritaAcaraPo")
    @JsonView(Views.Public.class)
    private InvoiceModel invoice;

    @Column(name = "periodeBeritaAcara")
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate periodeBeritaAcara;

    @NotNull
    @Column(columnDefinition = "boolean default false", name = "isApprove", nullable = false)
    @JsonView(Views.Public.class)
    private Boolean isApprove;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PurchaseOrderModel getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrderModel purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public String getNomorBa() {
        return nomorBa;
    }

    public void setNomorBa(String nomorBa) {
        this.nomorBa = nomorBa;
    }

    public String getJenisPekerjaan() {
        return jenisPekerjaan;
    }

    public void setJenisPekerjaan(String jenisPekerjaan) {
        this.jenisPekerjaan = jenisPekerjaan;
    }

    public String getJenisBeritaAcara() {
        return jenisBeritaAcara;
    }

    public void setJenisBeritaAcara(String jenisBeritaAcara) {
        this.jenisBeritaAcara = jenisBeritaAcara;
    }

    public LocalDate getTanggalTerbit() {
        return tanggalTerbit;
    }

    public void setTanggalTerbit(LocalDate tanggalTerbit) {
        this.tanggalTerbit = tanggalTerbit;
    }

    public LocalDate getTanggalKirim() {
        return tanggalKirim;
    }

    public void setTanggalKirim(LocalDate tanggalKirim) {
        this.tanggalKirim = tanggalKirim;
    }

    public LocalDate getTanggalKembali() {
        return tanggalKembali;
    }

    public void setTanggalKembali(LocalDate tanggalKembali) {
        this.tanggalKembali = tanggalKembali;
    }

    public StatusBeritaAcaraModel getStatus() {
        return status;
    }

    public void setStatus(StatusBeritaAcaraModel status) {
        this.status = status;
    }

    public InvoiceModel getInvoice() {
        return invoice;
    }

    public void setInvoice(InvoiceModel invoice) {
        this.invoice = invoice;
    }

    public LocalDate getPeriodeBeritaAcara() {
        return periodeBeritaAcara;
    }

    public void setPeriodeBeritaAcara(LocalDate periodeBeritaAcara) {
        this.periodeBeritaAcara = periodeBeritaAcara;
    }

    public Boolean getApprove() {
        return isApprove;
    }

    public void setApprove(Boolean approve) {
        isApprove = approve;
    }
}
