package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "unitKontrakServis")
public class UnitKontrakServisModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(name = "unit", nullable = false)
    @JsonView(Views.Public.class)
    private String unit;

    @OneToMany(mappedBy = "unitKontrakServis", cascade = CascadeType.ALL)
    @JsonView(Views.UnitServis.class)
    private List<DataUnitKontrakServisModel> daftarUnit;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public List<DataUnitKontrakServisModel> getDaftarUnit() {
        return daftarUnit;
    }

    public void setDaftarUnit(List<DataUnitKontrakServisModel> daftarUnit) {
        this.daftarUnit = daftarUnit;
    }
}
