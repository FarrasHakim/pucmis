package puc.mis.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import puc.mis.repository.*;
import puc.mis.service.karyawan.KaryawanService;

import java.util.NoSuchElementException;
import java.util.Random;

@Component
public class DatabaseLoader implements CommandLineRunner {

    @Autowired
    private RoleDb roleDb;

    @Autowired
    private CabangDb cabangDb;

    @Autowired
    private KaryawanService karyawanService;

    @Autowired
    private KaryawanDb karyawanDb;

    @Autowired
    private StatusBeritaAcaraDb statusBeritaAcaraDb;

    @Autowired
    private StatusInvoiceDb statusInvoiceDb;

    @Autowired
    private StatusJadwalServisDb statusJadwalServisDb;

    @Autowired
    private StatusKontrakServisDb statusKontrakServisDb;

    @Autowired
    private StatusPODb statusPODb;

    @Autowired
    private StatusSparepartDb statusSparepartDb;

    @Autowired
    private StatusKaryawanDb statusKaryawanDb;

    private static void disableWarning() {
        System.err.close();
        System.setErr(System.out);
    }

    @Override
    public void run(String... strings) throws Exception {

        //initiate essentials
        roles();
        status();

        ObjectMapper objectMapper = new ObjectMapper();
        disableWarning();

        cabang(objectMapper);

        karyawan();
    }

    private void status() {

        String[] beritaAcaras = {"Disetujui", "Menunggu Persetujuan", "Tidak Disetujui"};
        String[] invoices = {"Terbit", "Terkirim - Belum Lunas", "Terkirim - Sudah Lunas"};
        String[] jadwalServiss = {"Belum Diservis", "Sudah Diservis", "Dibatalkan"};
        String[] kontrakServiss = {"Berjalan", "Sudah Selesai", "Dibatalkan"};
        String[] purchaseOrders = {"Dibuat", "Diproses", "Dibatalkan", "Selesai"};
        String[] spareparts = {"Menunggu Pengadaan Sparepart", "Tersedia - Belum Terpasang", "Tersedia - Terpasang", "Tidak Tersedia"};
        String[] karyawans = {"Aktif", "Tidak Aktif"};

        int counter = 1;
        for (String statusStr : beritaAcaras) {

            StatusBeritaAcaraModel status = new StatusBeritaAcaraModel();
            status.setId(counter);
            status.setStatus(statusStr);
            statusBeritaAcaraDb.save(status);
            counter += 1;
        }

        counter = 1;
        for (String statusStr : invoices) {

            StatusInvoiceModel status = new StatusInvoiceModel();
            status.setId(counter);
            status.setStatus(statusStr);
            statusInvoiceDb.save(status);
            counter += 1;
        }

        counter = 1;
        for (String statusStr : jadwalServiss) {

            StatusJadwalServisModel status = new StatusJadwalServisModel();
            status.setId(counter);
            status.setStatus(statusStr);
            statusJadwalServisDb.save(status);
            counter += 1;
        }

        counter = 1;
        for (String statusStr : kontrakServiss) {

            StatusKontrakServisModel status = new StatusKontrakServisModel();
            status.setId(counter);
            status.setStatus(statusStr);
            statusKontrakServisDb.save(status);
            counter += 1;
        }

        counter = 1;
        for (String statusStr : purchaseOrders) {

            StatusPOModel status = new StatusPOModel();
            status.setId(counter);
            status.setStatus(statusStr);
            statusPODb.save(status);
            counter += 1;
        }

        counter = 1;
        for (String statusStr : spareparts) {

            StatusSparepartModel status = new StatusSparepartModel();
            status.setId(counter);
            status.setStatus(statusStr);
            statusSparepartDb.save(status);
            counter += 1;
        }

        counter = 1;
        for (String statusStr : karyawans) {

            StatusKaryawanModel status = new StatusKaryawanModel();
            status.setId(counter);
            status.setStatus(statusStr);
            statusKaryawanDb.save(status);
            counter += 1;
        }

    }

    private void roles() {

        RoleModel role;

        String[] roles = {"Admin", "Manajemen", "Marketing", "Koordinator Maintenance", "Direktur Keuangan",
                "Finance PO", "Finance Servis", "Bagian PO", "Bagian Servis", "Bagian Pemasangan", "Bagian Sparepart", "Kepala Area"};

        for (int i = 0; i < roles.length; i++) {
            if (roleDb.findByNama(roles[i]).isPresent()) {
                continue;
            }
            role = new RoleModel();
            role.setId(i + 1);
            role.setNama(roles[i]);
            roleDb.save(role);
        }
    }

    private void karyawan() {
        KaryawanModel karyawan;

        String[] roles = {"Admin", "Manajemen", "Marketing", "Koordinator Maintenance", "Direktur Keuangan",
                "Finance PO", "Finance Servis", "Bagian PO", "Bagian Servis", "Bagian Pemasangan", "Bagian Sparepart", "Kepala Area"};

        String uname;
        String randomNumber;

        for (String role : roles) {
            uname = role.replaceAll("\\s+", "");
            try {
                karyawan = karyawanService.getKaryawanByUsername(uname);
            } catch (NoSuchElementException e) {
                karyawan = new KaryawanModel();
                karyawan.setNama("Pak " + role);
                karyawan.setAlamat("Jakarta");
                randomNumber = "";
                for (int i = 0; i < 10; i++) {
                    randomNumber += new Random().nextInt(10);
                }
                karyawan.setNomorTelepon("08" + randomNumber);
                karyawan.setEmail(uname.toLowerCase() + "@ptpuc.com");
                karyawan.setUsername(uname);
                karyawan.setPassword(karyawanService.encrypt("12345"));
                karyawan.setRole(roleDb.findByNama(role).get());
                karyawan.setCabang(cabangDb.findByNama("PT PILLAR PUSAT").get());
                if (cabangDb.findByNama("PT PILLAR PUSAT").get().getKoordinator() == null) {
                    karyawan.setKoorCabang(cabangDb.findByNama("PT PILLAR PUSAT").get());
                }
                karyawan.setStatus(statusKaryawanDb.findByStatus("Aktif").get());

                karyawan = karyawanDb.save(karyawan);
            }
        }
    }

    private CabangModel cabang(ObjectMapper objectMapper) {

        if (!cabangDb.findAll().isEmpty()) {
            return cabangDb.findAll().get(0);
        }

        CabangModel cabang = new CabangModel();

        try {
            cabang.setNama("PT PILLAR PUSAT");
            cabang.setAlamat("Jonggol");
        } catch (Exception e) {
            System.out.println(e);
        }

        return cabangDb.save(cabang);
    }
}
