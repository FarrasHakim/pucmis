package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "unitPo")
public class UnitPOModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(name = "nama", nullable = false)
    @JsonView(Views.Public.class)
    private String nama;
    @OneToMany(mappedBy = "unitPo", cascade = CascadeType.ALL)
    @JsonView(Views.UnitPO.class)
    private List<DataUnitPOModel> daftarUnit;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<DataUnitPOModel> getDaftarUnit() {
        return daftarUnit;
    }

    public void setDaftarUnit(List<DataUnitPOModel> daftarUnit) {
        this.daftarUnit = daftarUnit;
    }
}
