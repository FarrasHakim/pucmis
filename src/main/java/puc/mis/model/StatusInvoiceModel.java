package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "statusInvoice")
public class StatusInvoiceModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(name = "status", nullable = false)
    @JsonView(Views.Public.class)
    private String status;

    @OneToMany(mappedBy = "status", fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(Views.StatusBeritaAcara.class)
    private List<InvoiceModel> invoice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<InvoiceModel> getInvoice() {
        return invoice;
    }

    public void setInvoice(List<InvoiceModel> invoice) {
        this.invoice = invoice;
    }
}
