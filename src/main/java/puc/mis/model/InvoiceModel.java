package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "invoice")
public class InvoiceModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Column(name = "jenis", nullable = false)
    @JsonView(Views.Public.class)
    private String jenis;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_beritaAcaraPo", referencedColumnName = "id")
    @JsonView(Views.Invoice.class)
    private BeritaAcaraPOModel beritaAcaraPo;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_beritaAcaraServis", referencedColumnName = "id")
    @JsonView(Views.Invoice.class)
    private BeritaAcaraServisModel beritaAcaraServis;

    @ManyToOne
    @JoinColumn(name = "penerimaBa", nullable = false)
    @JsonView(Views.Public.class)
    private KaryawanModel penerimaBa;

    @NotNull
    @Column(name = "nomor", nullable = false)
    @JsonView(Views.Public.class)
    private String nomor;

    @NotNull
    @Column(name = "tanggalTerbit", nullable = false)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalTerbit;

    @NotNull
    @Column(name = "tanggalKirim", nullable = false)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalKirim;

    @NotNull
    @Column(name = "tanggalKembali", nullable = false)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalKembali;

    @NotNull
    @Column(name = "tanggalJatuhTempo", nullable = false)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDate tanggalJatuhTempo;

    @NotNull
    @Column(name = "jumlahTagihan", nullable = false)
    @JsonView(Views.Public.class)
    private Long jumlahTagihan;

    @NotNull
    @Column(name = "jumlahPembayaran", nullable = false)
    @JsonView(Views.Public.class)
    private Long jumlahPembayaran;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_status", referencedColumnName = "id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonView(Views.Public.class)
    private StatusInvoiceModel status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public BeritaAcaraPOModel getBeritaAcaraPo() {
        return beritaAcaraPo;
    }

    public void setBeritaAcaraPo(BeritaAcaraPOModel beritaAcaraPo) {
        this.beritaAcaraPo = beritaAcaraPo;
    }

    public BeritaAcaraServisModel getBeritaAcaraServis() {
        return beritaAcaraServis;
    }

    public void setBeritaAcaraServis(BeritaAcaraServisModel beritaAcaraServis) {
        this.beritaAcaraServis = beritaAcaraServis;
    }

    public KaryawanModel getPenerimaBa() {
        return penerimaBa;
    }

    public void setPenerimaBa(KaryawanModel penerimaBa) {
        this.penerimaBa = penerimaBa;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public LocalDate getTanggalTerbit() {
        return tanggalTerbit;
    }

    public void setTanggalTerbit(LocalDate tanggalTerbit) {
        this.tanggalTerbit = tanggalTerbit;
    }

    public LocalDate getTanggalKirim() {
        return tanggalKirim;
    }

    public void setTanggalKirim(LocalDate tanggalKirim) {
        this.tanggalKirim = tanggalKirim;
    }

    public LocalDate getTanggalKembali() {
        return tanggalKembali;
    }

    public void setTanggalKembali(LocalDate tanggalKembali) {
        this.tanggalKembali = tanggalKembali;
    }

    public LocalDate getTanggalJatuhTempo() {
        return tanggalJatuhTempo;
    }

    public void setTanggalJatuhTempo(LocalDate tanggalJatuhTempo) {
        this.tanggalJatuhTempo = tanggalJatuhTempo;
    }

    public Long getJumlahTagihan() {
        return jumlahTagihan;
    }

    public void setJumlahTagihan(Long jumlahTagihan) {
        this.jumlahTagihan = jumlahTagihan;
    }

    public Long getJumlahPembayaran() {
        return jumlahPembayaran;
    }

    public void setJumlahPembayaran(Long jumlahPembayaran) {
        this.jumlahPembayaran = jumlahPembayaran;
    }

    public StatusInvoiceModel getStatus() {
        return status;
    }

    public void setStatus(StatusInvoiceModel status) {
        this.status = status;
    }
}
