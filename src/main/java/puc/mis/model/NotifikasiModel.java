package puc.mis.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "notifikasi")
public class NotifikasiModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private Integer id;

    @NotNull
    @Size(max = 120)
    @Column(name = "judul", nullable = false)
    @JsonView(Views.Public.class)
    private String judul;

    @Size(max = 500)
    @Column(name = "deskripsi")
    @JsonView(Views.Public.class)
    private String deskripsi;

    @NotNull
    @Column(name = "tanggal", nullable = false)
    @JsonFormat(pattern = "HH:mm dd-MM-yyyy")
    @JsonView(Views.Public.class)
    private LocalDateTime tanggal;

    @Lob
    @Column(name = "source")
    @JsonView(Views.Public.class)
    private String source;

    @OneToMany(mappedBy = "notifikasi", cascade = CascadeType.ALL)
    @JsonView(Views.Notifikasi.class)
    private List<BacaNotifikasiModel> daftarPenerima;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public LocalDateTime getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDateTime tanggal) {
        this.tanggal = tanggal;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public List<BacaNotifikasiModel> getDaftarPenerima() {
        return daftarPenerima;
    }

    public void setDaftarPenerima(List<BacaNotifikasiModel> daftarPenerima) {
        this.daftarPenerima = daftarPenerima;
    }
}
