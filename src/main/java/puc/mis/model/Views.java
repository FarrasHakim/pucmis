package puc.mis.model;

public class Views {
    public static class Public {
    }

    public static class Area extends Public {
    }

    public static class BacaNotifikasi extends Public {
    }

    public static class BeritaAcaraPO extends Public {
    }

    public static class BeritaAcaraServis extends Public {
    }

    public static class Cabang extends Public {
    }

    public static class Customer extends Public {
    }

    public static class DataUnitPO extends Public {
    }

    public static class DataUnitServis extends Public {
    }

    public static class Invoice extends Public {
    }

    public static class JadwalServis extends Public {
    }

    public static class Karyawan extends Public {

    }

    public static class KontrakServis extends Public {
    }

    public static class Notifikasi extends Public {
    }

    public static class PurchaseOrder extends Public {
    }

    public static class Role extends Public {
    }

    public static class StatusBeritaAcara extends Public {
    }

    public static class StatusInvoice extends Public {
    }

    public static class StatusJadwalServis extends Public {
    }

    public static class StatusKontrakServis extends Public {
    }

    public static class StatusPO extends Public {
    }

    public static class StatusSparepart extends Public {
    }

    public static class UnitPO extends Public {
    }

    public static class UnitServis extends Public {
    }
}
