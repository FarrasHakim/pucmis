package puc.mis.service.area;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import puc.mis.dto.area.FormAreaDTO;
import puc.mis.model.*;
import puc.mis.repository.AreaDb;
import puc.mis.service.cabang.CabangService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.log.LogService;
import puc.mis.service.notifikasi.NotifikasiService;
import puc.mis.service.role.RoleService;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class AreaServiceImpl implements AreaService {

    @Autowired
    private AreaDb areaDb;

    @Autowired
    private KaryawanService karyawanService;

    @Autowired
    private NotifikasiService notifikasiService;

    @Autowired
    private CabangService cabangService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private LogService logService;

    @Override
    public AreaModel getById(Integer id) {
        Optional<AreaModel> area = areaDb.findById(id);
        if (area.isPresent()) {
            return area.get();
        } else {
            throw new NoSuchElementException("Area dengan id " + id + " tidak dapat ditemukan");
        }
    }

    @Override
    public List<AreaModel> getAll() {
        return areaDb.findAll();
    }

    @Override
    public List<AreaModel> getDaftarArea(Integer idCabang) {
        CabangModel cabang = cabangService.getById(idCabang);

        return areaDb.findAllByCabang(cabang);
    }

    @Override
    public void deleteById(Integer id) {
        Optional<AreaModel> area = areaDb.findById(id);
        if (area.isPresent()) {
            String nama = area.get().getNama();

            if (!conditionChecker(area.get(), "delete")) {
                throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Tidak dapat menghapus area");
            }

            areaDb.delete(area.get());

            //notifikasi
            String isi = "Area dengan nama " + nama + " telah dihapus oleh " + karyawanService.getUserLogin().getNama();
            NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penghapusan Area", isi, "/", "Admin");
            //

            //logs
            logService.deleteAll(id.toString(), "Area");
            //
        } else {
            throw new NoSuchElementException("Area with id " + id + " doesn't exist");
        }
    }

    @Override
    public AreaModel save(FormAreaDTO area) {
        AreaModel newArea = new AreaModel();
        newArea.setNama(area.getNama());
        newArea.setAlamat(area.getAlamat());
        newArea.setCabang(cabangService.getById(area.getCabang()));
        newArea.setDaftarCustomer(new ArrayList<CustomerModel>());

        newArea = areaDb.save(newArea);

        if (area.getKepalaArea() != null && !area.getKepalaArea().equals("")) {
            area.setId(newArea.getId());
            changeKepala(area);
        }

        //notifikasi
        String isi = "Area dengan nama " + newArea.getNama() + " berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        String src = "/area/profil/" + newArea.getId();
        String roles = "Admin,Koordinator Maintenance,Manajemen";
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penambahan Area", isi, src, roles);
        //

        //logs
        String judul = "Area dibuat";
        String deskripsi = "Area baru dibuat oleh " + karyawanService.getUserLogin().getNama();
        logService.add(judul, deskripsi, newArea.getId().toString(), "Area");
        //

        return newArea;
    }

    @Override
    public AreaModel update(FormAreaDTO area) {
        Optional<AreaModel> newArea = areaDb.findById(area.getId());
        if (newArea.isPresent()) {
            newArea.get().setNama(area.getNama());
            newArea.get().setAlamat(area.getAlamat());
//            newArea.get().setCabang(cabangService.getById(area.getCabang()));
            //newArea.get().setKepalaArea(karyawanService.getKaryawanById(area.getId()));
        } else {
            throw new NoSuchElementException("Area with id " + area.getId() + " doesn't exist");
        }

        //notifikasi
        String isi = "Area dengan nama " + area.getNama() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        String src = "/area/profil/" + newArea.get().getId();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRoleInCabang("Perubahan Area", isi, src, "Admin,Kepala Area", newArea.get().getCabang().getId());
        //

        //logs
        String judul = "Area diubah";
        String deskripsi = "Area diubah oleh " + karyawanService.getUserLogin().getNama();
        logService.add(judul, deskripsi, newArea.get().getId().toString(), "Area");
        //

        return areaDb.save(newArea.get());
    }

    @Override
    public AreaModel changeKepala(FormAreaDTO area) {
        Optional<AreaModel> targetArea = areaDb.findById(area.getId());
        KaryawanModel targetKaryawan = karyawanService.getKaryawanById(area.getKepalaArea());

        if (targetArea.isPresent()) {
            if (targetArea.get().getKepalaArea() != null) {
                targetArea.get().getKepalaArea().setArea(null);
            }
            targetKaryawan.setArea(targetArea.get());
            targetKaryawan.setRole(roleService.getByNama("Kepala Area"));
            targetArea.get().setKepalaArea(targetKaryawan);
        } else {
            throw new NoSuchElementException();
        }

        //notifikasi
        String isi = "Karyawan dengan nama " + targetKaryawan.getNama() + " telah ditunjuk menjadi Kepala Area " + targetArea.get().getNama() + " oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penunjukan Koordinator", isi, "/", "Admin");
        //

        //logs
        String judul = "Pergantian Kepala Area";
        String deskripsi = "Area memiliki kepala baru yaitu " + targetKaryawan.getNama();
        logService.add(judul, deskripsi, targetArea.get().getId().toString(), "Area");

        judul = "Perubahan role";
        deskripsi = "Karyawan ditunjuk untuk menjadi kepala area " + targetArea.get().getNama();
        logService.add(judul, deskripsi, targetKaryawan.getUuid(), "Karyawan");
        //

        return areaDb.save(targetArea.get());
    }

    private Boolean conditionChecker(AreaModel area, String source) {

        boolean con = true;

        if (source.contains("delete")) {

            boolean con1 = true;
            for (CustomerModel customer : area.getDaftarCustomer()) {

                for (KontrakServisModel kontrak : customer.getDaftarKontrakServis()) {
                    if (kontrak.getStatus().getStatus().equals("Berjalan")) {
                        con1 = false;
                        break;
                    }
                }

                if (!con1) {
                    break;
                }
            }

            con &= con1;
        }

        return con;
    }
}
