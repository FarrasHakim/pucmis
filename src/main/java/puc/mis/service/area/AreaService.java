package puc.mis.service.area;

import puc.mis.dto.area.FormAreaDTO;
import puc.mis.model.AreaModel;

import java.util.List;

public interface AreaService {
    AreaModel getById(Integer id);

    List<AreaModel> getAll();

    List<AreaModel> getDaftarArea(Integer idCabang);

    void deleteById(Integer id);

    AreaModel save(FormAreaDTO area);

    AreaModel update(FormAreaDTO area);

    AreaModel changeKepala(FormAreaDTO area);
}
