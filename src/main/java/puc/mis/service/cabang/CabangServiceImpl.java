package puc.mis.service.cabang;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import puc.mis.dto.cabang.FormCabangDTO;
import puc.mis.model.*;
import puc.mis.repository.CabangDb;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.log.LogService;
import puc.mis.service.notifikasi.NotifikasiService;
import puc.mis.service.role.RoleService;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class CabangServiceImpl implements CabangService {

    @Autowired
    private CabangDb cabangDb;

    @Autowired
    private NotifikasiService notifikasiService;

    @Autowired
    private KaryawanService karyawanService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private LogService logService;

    @Override
    public List<CabangModel> getAll() {
//        if (karyawanService.getUserLogin().getRole().getNama().equalsIgnoreCase("Admin")) {
        return cabangDb.findAll();
//        } else {
//            throw new ResponseStatusException(
//                    HttpStatus.FORBIDDEN, "Anda tidak memiliki akses.");
//        }
    }

    @Override
    public CabangModel getById(Integer id) {
        Optional<CabangModel> cabang = cabangDb.findById(id);

        if (cabang.isPresent()) {
            return cabang.get();
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public CabangModel getByNama(String nama) {
        Optional<CabangModel> cabang = cabangDb.findByNama(nama);

        if (cabang.isPresent()) {
            return cabang.get();
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public List<CabangModel> getDaftarCabang() {
        return cabangDb.findAll();
    }

    @Override
    public CabangModel addCabang(FormCabangDTO cabang) {
        CabangModel newcabang = new CabangModel();

        newcabang.setNama(cabang.getNama());
        newcabang.setAlamat(cabang.getAlamat());

        newcabang = cabangDb.save(newcabang);

        if (cabang.getKoordinator() != null && !cabang.getKoordinator().equals("")) {
            cabang.setId(newcabang.getId());
            changeKoordinator(cabang);
        }

        //notifikasi
        String isi = "Cabang baru dengan nama " + newcabang.getNama() + " telah ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        String src = "/cabang/profil/" + newcabang.getId();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Cabang Baru", isi, src, "Admin,Manajemen");
        //

        //logs
        String judul = "Cabang dibuat";
        String deskripsi = "Cabang baru dibuat oleh " + karyawanService.getUserLogin().getNama();
        logService.add(judul, deskripsi, newcabang.getId().toString(), "Cabang");
        //

        return newcabang;
    }

    @Override
    public CabangModel changeCabang(FormCabangDTO cabang) {
        Optional<CabangModel> newCabang = cabangDb.findById(cabang.getId());

        if (newCabang.isPresent()) {
            newCabang.get().setNama(cabang.getNama());
            newCabang.get().setAlamat(cabang.getAlamat());
        } else {
            throw new NoSuchElementException();
        }

        //notifikasi
        String isi = "Cabang dengan nama " + cabang.getNama() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perubahan Cabang", isi, "/", "Admin");
        //

        //logs
        String judul = "Cabang diubah";
        String deskripsi = "Cabang diubah oleh " + karyawanService.getUserLogin().getNama();
        logService.add(judul, deskripsi, newCabang.get().getId().toString(), "Cabang");
        //

        return cabangDb.save(newCabang.get());
    }

    @Override
    public void deleteCabang(Integer id) {
        Optional<CabangModel> targetCabang = cabangDb.findById(id);

        if (targetCabang.isPresent()) {
            String namaCabang = targetCabang.get().getNama();

            if (!conditionChecker(targetCabang.get(), "delete")) {
                throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Tidak dapat menghapus cabang");
            }

            cabangDb.delete(targetCabang.get());

            //notifikasi
            String isi = "Cabang dengan nama " + namaCabang + " telah dihapus oleh " + karyawanService.getUserLogin().getNama();
            NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penghapusan Cabang", isi, "/", "Admin");
            //

            //logs
            logService.deleteAll(id.toString(), "Cabang");
            //
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public CabangModel changeKoordinator(FormCabangDTO cabang) {
        Optional<CabangModel> targetCabang = cabangDb.findById(cabang.getId());
        KaryawanModel targetKaryawan = karyawanService.getKaryawanById(cabang.getKoordinator());

        if (targetCabang.isPresent()) {
            if (targetCabang.get().getKoordinator() != null) {
                targetCabang.get().getKoordinator().setKoorCabang(null);
            }
            targetKaryawan.setKoorCabang(targetCabang.get());
            targetKaryawan.setCabang(targetCabang.get());
            targetKaryawan.setRole(roleService.getByNama("Koordinator Maintenance"));
            targetCabang.get().setKoordinator(targetKaryawan);
        } else {
            throw new NoSuchElementException();
        }

        //notifikasi
        String isi = "Karyawan dengan nama " + targetKaryawan.getNama() + " telah ditunjuk menjadi koordinator Cabang " + targetCabang.get().getNama() + " oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penunjukan Koordinator", isi, "/", "Admin");
        //

        //logs
        String judul = "Pergantian Koordinator";
        String deskripsi = "Cabang memiliki koordinator baru yaitu " + targetKaryawan.getNama();
        logService.add(judul, deskripsi, targetCabang.get().getId().toString(), "Cabang");

        judul = "Perubahan role";
        deskripsi = "Karyawan ditunjuk untuk menjadi koordinator " + targetCabang.get().getNama();
        logService.add(judul, deskripsi, targetKaryawan.getUuid(), "Karyawan");
        //

        return cabangDb.save(targetCabang.get());
    }

    private Boolean conditionChecker(CabangModel cabang, String source) {

        boolean con = true;

        if (source.contains("delete")) {

            boolean con1 = true;
            for (CustomerModel customer : cabang.getDaftarCustomer()) {

                if (!con1) {
                    break;
                }

                for (KontrakServisModel kontrak : customer.getDaftarKontrakServis()) {
                    if (kontrak.getStatus().getStatus().equals("Berjalan")) {
                        con1 = false;
                        break;
                    }
                }
            }

            List<KaryawanModel> daftarKaryawan = cabang.getDaftarKaryawan();

            con &= con1 && (daftarKaryawan == null || daftarKaryawan.isEmpty());

        }

        return con;
    }

    @Override
    public List<PurchaseOrderModel> getDaftarPo(Integer idCabang) {
        Set<PurchaseOrderModel> daftarPo = new HashSet<>();
        CabangModel cabang = getById(idCabang);

        for (CustomerModel customer : cabang.getDaftarCustomer()) {
            daftarPo.addAll(customer.getDaftarPo());
        }

        return new ArrayList<>(daftarPo);
    }

    @Override
    public List<KontrakServisModel> getDaftarKontrak(Integer idCabang) {
        Set<KontrakServisModel> daftarKontrak = new HashSet<>();
        CabangModel cabang = getById(idCabang);

        for (CustomerModel customer : cabang.getDaftarCustomer()) {
            daftarKontrak.addAll(customer.getDaftarKontrakServis());
        }

        return new ArrayList<>(daftarKontrak);
    }
}