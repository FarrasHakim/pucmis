package puc.mis.service.cabang;

import puc.mis.dto.cabang.FormCabangDTO;
import puc.mis.model.CabangModel;
import puc.mis.model.KontrakServisModel;
import puc.mis.model.PurchaseOrderModel;

import java.util.List;


public interface CabangService {
    List<CabangModel> getAll();

    CabangModel getById(Integer id);

    CabangModel getByNama(String nama);

    List<CabangModel> getDaftarCabang();

    CabangModel addCabang(FormCabangDTO cabang);

    CabangModel changeCabang(FormCabangDTO cabang);

    void deleteCabang(Integer id);

    CabangModel changeKoordinator(FormCabangDTO cabang);

    List<PurchaseOrderModel> getDaftarPo(Integer idCabang);

    List<KontrakServisModel> getDaftarKontrak(Integer idCabang);
}