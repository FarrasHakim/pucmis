package puc.mis.service.notifikasi;

import puc.mis.model.NotifikasiModel;

public interface NotifikasiService {
    NotifikasiModel addNotifikasiForRole(String judul, String deskripsi, String source, String roles);

    NotifikasiModel addNotifikasiForRoleInCabang(String judul, String deskripsi, String source, String roles, Integer idCabang);

    NotifikasiModel addNotifikasiForKaryawan(String judul, String deskripsi, String source, String uuid);

    String deleteNotifikasi(Integer idNotifikasi);

    NotifikasiModel save(NotifikasiModel notifikasiModel);
}
