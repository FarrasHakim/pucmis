package puc.mis.service.notifikasi;

import puc.mis.model.BacaNotifikasiModel;
import puc.mis.model.KaryawanModel;
import puc.mis.model.NotifikasiModel;

import java.util.List;

public interface BacaNotifikasiService {
    BacaNotifikasiModel addBacaNotif(BacaNotifikasiModel bacaNotif);

    void deleteBacaNotif(Integer idBacaNotif);

    NotifikasiModel lihatNotifikasi(Integer idBacaNotif);

    List<BacaNotifikasiModel> getDaftarBacaNotif(KaryawanModel karyawanModel);
}
