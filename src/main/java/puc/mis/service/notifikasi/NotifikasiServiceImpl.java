package puc.mis.service.notifikasi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import puc.mis.model.BacaNotifikasiModel;
import puc.mis.model.CabangModel;
import puc.mis.model.KaryawanModel;
import puc.mis.model.NotifikasiModel;
import puc.mis.repository.NotifikasiDb;
import puc.mis.service.cabang.CabangService;
import puc.mis.service.karyawan.KaryawanService;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class NotifikasiServiceImpl implements NotifikasiService {

    @Autowired
    private NotifikasiDb notifikasiDb;

    @Autowired
    private KaryawanService karyawanService;

    @Autowired
    private BacaNotifikasiService bacaNotifikasiService;

    @Autowired
    private CabangService cabangService;

    @Override
    public NotifikasiModel save(NotifikasiModel notifikasiModel) {
        return notifikasiDb.save(notifikasiModel);
    }

    @Override
    public NotifikasiModel addNotifikasiForRole(String judul, String deskripsi, String source, String daftarRolePenerima) {

        NotifikasiModel notifikasiBaru = initiateNotifikasi(judul, deskripsi, source);

        String[] penerimas = daftarRolePenerima.split(",");
        for (String penerima : penerimas) {
            List<KaryawanModel> daftarPenerima = karyawanService.getByRole(penerima);
            sendToKaryawan(daftarPenerima, notifikasiBaru);
        }

        return notifikasiBaru;
    }

    @Override
    public NotifikasiModel addNotifikasiForRoleInCabang(String judul, String deskripsi, String source, String roles, Integer idCabang) {

        NotifikasiModel notifikasiBaru = initiateNotifikasi(judul, deskripsi, source);
        CabangModel cabang = cabangService.getById(idCabang);

        String[] penerimas = roles.split(",");
        for (String penerima : penerimas) {
            List<KaryawanModel> daftarPenerima;
            if (penerima.equals("Manajemen")) {
                daftarPenerima = karyawanService.getByRole(penerima);
            } else {
                daftarPenerima = karyawanService.getByRoleAndCabang(penerima, cabang);
            }
            sendToKaryawan(daftarPenerima, notifikasiBaru);
        }

        return notifikasiBaru;
    }

    @Override
    public NotifikasiModel addNotifikasiForKaryawan(String judul, String deskripsi, String source, String uuid) {

        NotifikasiModel notifikasiBaru = initiateNotifikasi(judul, deskripsi, source);

        KaryawanModel karyawan = karyawanService.getKaryawanById(uuid);
        BacaNotifikasiModel notifUntukKaryawan = new BacaNotifikasiModel();
        notifUntukKaryawan.setNotifikasi(notifikasiBaru);
        notifUntukKaryawan.setKaryawan(karyawan);
        notifUntukKaryawan.setRead(false);
        bacaNotifikasiService.addBacaNotif(notifUntukKaryawan);

        return notifikasiBaru;
    }

    @Override
    public String deleteNotifikasi(Integer idNotifikasi) {
        NotifikasiModel notifikasi = notifikasiDb.findById(idNotifikasi).get();
        if (notifikasi.getDaftarPenerima().isEmpty()) {
            notifikasiDb.delete(notifikasi);
            return "success";
        } else {
            return "error";
        }
    }

    private NotifikasiModel initiateNotifikasi(String judul, String deskripsi, String source) {
        NotifikasiModel notifikasi = new NotifikasiModel();
        notifikasi.setJudul(judul);
        notifikasi.setDeskripsi(deskripsi);
        notifikasi.setTanggal(LocalDateTime.now());
        notifikasi.setSource(source);
        return notifikasiDb.save(notifikasi);
    }

    private void sendToKaryawan(List<KaryawanModel> daftarPenerima, NotifikasiModel notifikasi) {
        for (KaryawanModel karyawan : daftarPenerima) {
            BacaNotifikasiModel notifUntukKaryawan = new BacaNotifikasiModel();
            notifUntukKaryawan.setKaryawan(karyawan);
            notifUntukKaryawan.setNotifikasi(notifikasi);
            notifUntukKaryawan.setRead(false);
            bacaNotifikasiService.addBacaNotif(notifUntukKaryawan);
        }
    }

}
