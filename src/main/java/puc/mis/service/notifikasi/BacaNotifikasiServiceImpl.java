package puc.mis.service.notifikasi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import puc.mis.model.BacaNotifikasiModel;
import puc.mis.model.KaryawanModel;
import puc.mis.model.NotifikasiModel;
import puc.mis.repository.BacaNotifikasiDb;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class BacaNotifikasiServiceImpl implements BacaNotifikasiService {

    @Autowired
    private BacaNotifikasiDb bacaNotifikasiDb;

    @Autowired
    private NotifikasiService notifikasiService;

    @Override
    public BacaNotifikasiModel addBacaNotif(BacaNotifikasiModel bacaNotif) {
        return bacaNotifikasiDb.save(bacaNotif);
    }

    @Override
    public void deleteBacaNotif(Integer idBacaNotif) {
        Optional<BacaNotifikasiModel> bacaNotif = bacaNotifikasiDb.findById(idBacaNotif);
        if (!bacaNotif.isPresent()) {
            throw new NoSuchElementException();
        }
        bacaNotifikasiDb.delete(bacaNotif.get());
        //Integer idNotifikasi = bacaNotif.getNotifikasi().getId();
        //notifikasiRestService.deleteNotifikasi(idNotifikasi);
    }

    @Override
    public NotifikasiModel lihatNotifikasi(Integer idBacaNotif) {
        BacaNotifikasiModel bacaNotifModel = bacaNotifikasiDb.findById(idBacaNotif).get();
        try {
            bacaNotifModel.setRead(true);
            bacaNotifikasiDb.save(bacaNotifModel);
            return bacaNotifModel.getNotifikasi();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public List<BacaNotifikasiModel> getDaftarBacaNotif(KaryawanModel karyawanModel) {
        return bacaNotifikasiDb.findAllByKaryawanOrderByNotifikasiDesc(karyawanModel);
    }
}
