package puc.mis.service.invoice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import puc.mis.dto.invoice.FormInvoiceDTO;
import puc.mis.model.InvoiceModel;
import puc.mis.model.NotifikasiModel;
import puc.mis.repository.InvoiceDb;
import puc.mis.service.beritaAcara.BeritaAcaraPOService;
import puc.mis.service.beritaAcara.BeritaAcaraServisService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.notifikasi.NotifikasiService;
import puc.mis.service.status.StatusInvoiceService;
import puc.mis.service.status.StatusPOService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class InvoiceServiceImpl implements InvoiceService {

    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    @Autowired
    StatusInvoiceService statusInvoiceService;
    @Autowired
    private InvoiceDb invoiceDb;
    @Autowired
    private KaryawanService karyawanService;
    @Autowired
    private NotifikasiService notifikasiService;
    @Autowired
    private BeritaAcaraPOService beritaAcaraPOService;
    @Autowired
    private BeritaAcaraServisService beritaAcaraServisService;
    @Autowired
    private StatusPOService statusPOService;

    @Override
    public InvoiceModel getById(Integer id) {
        Optional<InvoiceModel> invoice = invoiceDb.findById(id);
        if (invoice.isPresent()) {
            return invoice.get();
        } else {
            throw new NoSuchElementException("Invoice with id " + id + " doesn't exist");
        }
    }

    @Override
    public List<InvoiceModel> getAll() {
        if (karyawanService.getUserLogin().getRole().getNama().equalsIgnoreCase("Admin")) {
            return invoiceDb.findAll();
        } else {
            throw new ResponseStatusException(
                    HttpStatus.FORBIDDEN, "Anda tidak memiliki akses.");
        }
    }

    @Override
    public void deleteById(Integer id) {
        Optional<InvoiceModel> invoice = invoiceDb.findById(id);
        if (invoice.isPresent()) {
            String nomor = invoice.get().getNomor();

            if (!conditionChecker(invoice.get(), "delete")) {
                throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Tidak dapat menghapus invoice");
            }

            invoiceDb.delete(invoice.get());

            //notifikasi
            String isi = "Invoice dengan nomor " + nomor + " telah dihapus oleh " + karyawanService.getUserLogin().getNama();
            NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penghapusan Area", isi, "/", "Admin");
            //
        } else {
            throw new NoSuchElementException("Invoice with id " + id + " doesn't exist");
        }
    }

    @Override
    public InvoiceModel save(FormInvoiceDTO invoice) {
        InvoiceModel newInvoice = new InvoiceModel();
        newInvoice.setJenis(invoice.getJenis());
        newInvoice.setBeritaAcaraPo(beritaAcaraPOService.getById(invoice.getBeritaAcaraPoId()));
        newInvoice.setBeritaAcaraServis(beritaAcaraServisService.getById(invoice.getBeritaAcaraServisId()));
        newInvoice.setPenerimaBa(karyawanService.getKaryawanById(invoice.getPenerimaBaId()));
        newInvoice.setNomor(invoice.getNomor());

        String date = invoice.getTanggalTerbit();
        LocalDate localDate = LocalDate.parse(date, dateFormat);
        newInvoice.setTanggalTerbit(localDate);

        date = invoice.getTanggalKirim();
        localDate = LocalDate.parse(date, dateFormat);
        newInvoice.setTanggalKirim(localDate);

        date = invoice.getTanggalKembali();
        localDate = LocalDate.parse(date, dateFormat);
        newInvoice.setTanggalKembali(localDate);

        date = invoice.getTanggalJatuhTempo();
        localDate = LocalDate.parse(date, dateFormat);
        newInvoice.setTanggalJatuhTempo(localDate);

        newInvoice.setJumlahTagihan(invoice.getJumlahTagihan());
        newInvoice.setJumlahPembayaran(invoice.getJumlahPembayaran());
        newInvoice.setStatus(statusInvoiceService.getById(invoice.getStatusInvoiceId()));

        String isi = "Invoice dengan nomor " + invoice.getNomor() + " berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penambahan Invoice ", isi, "/", "Admin");

        return invoiceDb.save(newInvoice);
    }

    @Override
    public InvoiceModel update(FormInvoiceDTO invoice) {
        Optional<InvoiceModel> newInvoice = invoiceDb.findById(invoice.getId());
        if (newInvoice.isPresent()) {
            if (!conditionChecker(newInvoice.get(), "update")) {
                throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Tidak dapat mengubah invoice");
            }

            newInvoice.get().setJenis(invoice.getJenis());


            if (invoice.getBeritaAcaraPoId() != null) {
                newInvoice.get().setBeritaAcaraPo(beritaAcaraPOService.getById(invoice.getBeritaAcaraPoId()));
                if (invoice.getStatusInvoiceId() == 3) {
                    beritaAcaraPOService.getById(invoice.getBeritaAcaraPoId()).getPurchaseOrder().setStatusPo(statusPOService.getById(4));
                }
                if (invoice.getStatusInvoiceId() != 3) {
                    beritaAcaraPOService.getById(invoice.getBeritaAcaraPoId()).getPurchaseOrder().setStatusPo(statusPOService.getById(2));
                }
            } else if (invoice.getBeritaAcaraServisId() != null) {
                newInvoice.get().setBeritaAcaraServis(beritaAcaraServisService.getById(invoice.getBeritaAcaraServisId()));
            }


            newInvoice.get().setPenerimaBa(karyawanService.getKaryawanById(invoice.getPenerimaBaId()));


            newInvoice.get().setNomor(invoice.getNomor());


            String date = invoice.getTanggalTerbit();
            LocalDate localDate = LocalDate.parse(date, dateFormat);
            newInvoice.get().setTanggalTerbit(localDate);


            date = invoice.getTanggalKirim();
            localDate = LocalDate.parse(date, dateFormat);
            newInvoice.get().setTanggalKirim(localDate);


            date = invoice.getTanggalKembali();
            localDate = LocalDate.parse(date, dateFormat);
            newInvoice.get().setTanggalKembali(localDate);


            date = invoice.getTanggalJatuhTempo();
            localDate = LocalDate.parse(date, dateFormat);
            newInvoice.get().setTanggalJatuhTempo(localDate);


            newInvoice.get().setJumlahTagihan(invoice.getJumlahTagihan());
            newInvoice.get().setJumlahPembayaran(invoice.getJumlahPembayaran());
            newInvoice.get().setStatus(statusInvoiceService.getById(invoice.getStatusInvoiceId()));


        } else {
            throw new NoSuchElementException("Invoice with id " + invoice.getId() + " doesn't exist");
        }


        //notifikasi
        String isi = "Invoice dengan nomor " + invoice.getNomor() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perubahan Invoice", isi, "/", "Admin");
        //


        return invoiceDb.save(newInvoice.get());
    }

    private Boolean conditionChecker(InvoiceModel invoice, String source) {

        boolean con = true;

        if (source.contains("delete") || source.contains("update")) {

            con &= invoice.getStatus().getStatus().equals("Terbit");

        }

        return con;
    }

    @Override
    public InvoiceModel saveInvoiceServis(FormInvoiceDTO invoice) {
        InvoiceModel newInvoice = new InvoiceModel();
        newInvoice.setJenis(invoice.getJenis());
        newInvoice.setBeritaAcaraServis(beritaAcaraServisService.getById(invoice.getBeritaAcaraServisId()));
        newInvoice.setPenerimaBa(karyawanService.getKaryawanById(invoice.getPenerimaBaId()));
        newInvoice.setNomor(invoice.getNomor());

        String date = invoice.getTanggalTerbit();
        LocalDate localDate = LocalDate.parse(date, dateFormat);
        newInvoice.setTanggalTerbit(localDate);

        date = invoice.getTanggalKirim();
        localDate = LocalDate.parse(date, dateFormat);
        newInvoice.setTanggalKirim(localDate);

        date = invoice.getTanggalKembali();
        localDate = LocalDate.parse(date, dateFormat);
        newInvoice.setTanggalKembali(localDate);

        date = invoice.getTanggalJatuhTempo();
        localDate = LocalDate.parse(date, dateFormat);
        newInvoice.setTanggalJatuhTempo(localDate);

        newInvoice.setJumlahTagihan(invoice.getJumlahTagihan());
        newInvoice.setJumlahPembayaran(invoice.getJumlahPembayaran());
        newInvoice.setStatus(statusInvoiceService.getById(invoice.getStatusInvoiceId()));

        String isi = "Invoice dengan nomor " + invoice.getNomor() + " berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penambahan Invoice ", isi, "/", "Admin");

        return invoiceDb.save(newInvoice);
    }

    @Override
    public InvoiceModel saveInvoicePo(FormInvoiceDTO invoice) {
        InvoiceModel newInvoice = new InvoiceModel();
        newInvoice.setJenis(invoice.getJenis());
        newInvoice.setBeritaAcaraPo(beritaAcaraPOService.getById(invoice.getBeritaAcaraPoId()));
        newInvoice.setPenerimaBa(karyawanService.getKaryawanById(invoice.getPenerimaBaId()));
        newInvoice.setNomor(invoice.getNomor());

        String date = invoice.getTanggalTerbit();
        LocalDate localDate = LocalDate.parse(date, dateFormat);
        newInvoice.setTanggalTerbit(localDate);

        date = invoice.getTanggalKirim();
        localDate = LocalDate.parse(date, dateFormat);
        newInvoice.setTanggalKirim(localDate);

        date = invoice.getTanggalKembali();
        localDate = LocalDate.parse(date, dateFormat);
        newInvoice.setTanggalKembali(localDate);

        date = invoice.getTanggalJatuhTempo();
        localDate = LocalDate.parse(date, dateFormat);
        newInvoice.setTanggalJatuhTempo(localDate);

        newInvoice.setJumlahTagihan(invoice.getJumlahTagihan());
        newInvoice.setJumlahPembayaran(invoice.getJumlahPembayaran());
        newInvoice.setStatus(statusInvoiceService.getById(invoice.getStatusInvoiceId()));


        if (invoice.getStatusInvoiceId() == 3) {
            beritaAcaraPOService.getById(invoice.getBeritaAcaraPoId()).getPurchaseOrder().setStatusPo(statusPOService.getById(4));
        }

        if (invoice.getStatusInvoiceId() != 3) {
            beritaAcaraPOService.getById(invoice.getBeritaAcaraPoId()).getPurchaseOrder().setStatusPo(statusPOService.getById(2));
        }

        String isi = "Invoice dengan nomor " + invoice.getNomor() + " berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penambahan Invoice ", isi, "/", "Admin");

        return invoiceDb.save(newInvoice);
    }

    @Override
    public InvoiceModel updateStatus(FormInvoiceDTO invoice) {
        Optional<InvoiceModel> newInvoice = invoiceDb.findById(invoice.getId());
        if (newInvoice.isPresent()) {
            if (!conditionChecker(newInvoice.get(), "update")) {
                throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Tidak dapat mengubah invoice");
            }

            newInvoice.get().setStatus(statusInvoiceService.getById(invoice.getStatusInvoiceId()));

        } else {
            throw new NoSuchElementException("Invoice with id " + invoice.getId() + " doesn't exist");
        }

        //notifikasi
        String isi = "Status invoice dengan nomor " + invoice.getNomor() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        String src = "/invoice/profil/" + newInvoice.get().getId();
        String roles = "Admin,Finance PO,Finance Servis,Direktur Keuangan,Koordinator Maintenance,Marketing,Manajemen";
        if (!statusInvoiceService.getById(invoice.getStatusInvoiceId()).getStatus().equals("Terkirim - Belum Lunas")) {
            roles += ",Bagian PO,Bagian Servis";
        }
        NotifikasiModel notif = notifikasiService.addNotifikasiForRoleInCabang("Perubahan Invoice", isi, src, roles, newInvoice.get().getPenerimaBa().getCabang().getId());
        //
        return invoiceDb.save(newInvoice.get());
    }
}
