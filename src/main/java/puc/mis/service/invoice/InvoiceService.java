package puc.mis.service.invoice;

import puc.mis.dto.invoice.FormInvoiceDTO;
import puc.mis.model.InvoiceModel;

import java.util.List;

public interface InvoiceService {
    InvoiceModel getById(Integer id);

    List<InvoiceModel> getAll();

    void deleteById(Integer id);

    InvoiceModel save(FormInvoiceDTO invoice);

    InvoiceModel update(FormInvoiceDTO invoice);

    InvoiceModel saveInvoiceServis(FormInvoiceDTO invoice);

    InvoiceModel saveInvoicePo(FormInvoiceDTO invoice);

    InvoiceModel updateStatus(FormInvoiceDTO invoice);
}
