package puc.mis.service.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import puc.mis.model.StatusKontrakServisModel;
import puc.mis.repository.StatusKontrakServisDb;
import puc.mis.service.karyawan.KaryawanService;

import javax.transaction.Transactional;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class StatusKontrakServisServiceImpl implements StatusKontrakServisService {


    @Autowired
    StatusKontrakServisDb statusKontrakServisDb;

    @Autowired
    KaryawanService karyawanService;

    @Override
    public StatusKontrakServisModel getById(Integer id) {
        Optional<StatusKontrakServisModel> StatusKontrakServis = statusKontrakServisDb.findById(id);
        if (StatusKontrakServis.isPresent()) {
            return StatusKontrakServis.get();
        } else {
            throw new NoSuchElementException("Status Kontrak Servis with id " + id + " doesn't exist");
        }
    }
}
