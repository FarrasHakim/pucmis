package puc.mis.service.status;

import puc.mis.model.StatusKontrakServisModel;

public interface StatusKontrakServisService {
    StatusKontrakServisModel getById(Integer id);
}
