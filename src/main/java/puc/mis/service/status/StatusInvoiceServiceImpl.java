package puc.mis.service.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import puc.mis.model.StatusInvoiceModel;
import puc.mis.repository.StatusInvoiceDb;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.notifikasi.NotifikasiService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class StatusInvoiceServiceImpl implements StatusInvoiceService {

    @Autowired
    private StatusInvoiceDb statusInvoiceDb;

    @Autowired
    private KaryawanService karyawanService;

    @Autowired
    private NotifikasiService notifikasiService;

    @Override
    public StatusInvoiceModel getById(Integer id) {
        Optional<StatusInvoiceModel> statusInvoice = statusInvoiceDb.findById(id);
        if (statusInvoice.isPresent()) {
            return statusInvoice.get();
        } else {
            throw new NoSuchElementException("StatusInvoice with id " + id + " doesn't exist");
        }
    }

    @Override
    public List<StatusInvoiceModel> getAll() {
        return statusInvoiceDb.findAll();
    }
}
