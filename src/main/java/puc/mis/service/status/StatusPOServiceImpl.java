package puc.mis.service.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import puc.mis.model.StatusPOModel;
import puc.mis.repository.StatusPODb;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class StatusPOServiceImpl implements StatusPOService {

    @Autowired
    StatusPODb statusPODb;

    public StatusPOModel getById(Integer id) {
        Optional<StatusPOModel> statusPO = statusPODb.findById(id);
        if (statusPO.isPresent()) {
            return statusPO.get();
        } else {
            throw new NoSuchElementException("StatusPO with id " + id + " doesn't exist");
        }
    }

    public StatusPOModel getByStatus(String status) {
        Optional<StatusPOModel> statusPO = statusPODb.findByStatus(status);
        if (statusPO.isPresent()) {
            return statusPO.get();
        } else {
            throw new NoSuchElementException("StatusPO with status " + status + " doesn't exist");
        }
    }

    @Override
    public List<StatusPOModel> getAll() {
        return statusPODb.findAll();
    }

}
