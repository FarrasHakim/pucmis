package puc.mis.service.status;

import puc.mis.model.StatusSparepartModel;

import java.util.List;

public interface StatusSparepartService {
    StatusSparepartModel getById(Integer id);

    List<StatusSparepartModel> getAll();
}
