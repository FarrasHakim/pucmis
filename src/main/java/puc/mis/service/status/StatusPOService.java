package puc.mis.service.status;

import puc.mis.model.StatusPOModel;

import java.util.List;

public interface StatusPOService {
    StatusPOModel getById(Integer id);

    StatusPOModel getByStatus(String status);

    List<StatusPOModel> getAll();
}
