package puc.mis.service.status;

import puc.mis.model.StatusKaryawanModel;

import java.util.List;

public interface StatusKaryawanService {
    List<StatusKaryawanModel> getAll();

    StatusKaryawanModel getStatusKaryawanByStatus(String status);
}
