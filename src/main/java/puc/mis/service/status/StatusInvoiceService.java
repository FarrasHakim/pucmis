package puc.mis.service.status;

import puc.mis.model.StatusInvoiceModel;

import java.util.List;

public interface StatusInvoiceService {
    StatusInvoiceModel getById(Integer id);

    List<StatusInvoiceModel> getAll();
}
