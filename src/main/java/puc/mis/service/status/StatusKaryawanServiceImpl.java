package puc.mis.service.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import puc.mis.model.StatusKaryawanModel;
import puc.mis.repository.StatusKaryawanDb;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class StatusKaryawanServiceImpl implements StatusKaryawanService {

    @Autowired
    private StatusKaryawanDb statusKaryawanDb;

    @Override
    public List<StatusKaryawanModel> getAll() {
        return statusKaryawanDb.findAll();
    }

    @Override
    public StatusKaryawanModel getStatusKaryawanByStatus(String status) {
        Optional<StatusKaryawanModel> statusModel = statusKaryawanDb.findByStatus(status);
        if (statusModel.isPresent()) {
            return statusModel.get();
        } else {
            throw new NoSuchElementException();
        }
    }
}
