package puc.mis.service.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import puc.mis.model.StatusJadwalServisModel;
import puc.mis.repository.StatusJadwalServisDb;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.notifikasi.NotifikasiService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class StatusJadwalServisServiceImpl implements StatusJadwalServisService {
    @Autowired
    StatusJadwalServisDb statusJadwalServisDb;
    @Autowired
    KaryawanService karyawanService;
    @Autowired
    NotifikasiService notifikasiService;

    @Override
    public StatusJadwalServisModel getById(Integer id) {
        Optional<StatusJadwalServisModel> statusJadwalServis = statusJadwalServisDb.findById(id);
        if (statusJadwalServis.isPresent()) {
            return statusJadwalServis.get();
        } else {
            throw new NoSuchElementException("StatusJadwalServis with id " + id + " doesn't exist");
        }
    }

    @Override
    public List<StatusJadwalServisModel> getAll() {
        if (karyawanService.getUserLogin().getRole().getNama().equalsIgnoreCase("Admin")) {
            return statusJadwalServisDb.findAll();
        } else {
            throw new ResponseStatusException(
                    HttpStatus.FORBIDDEN, "Anda tidak memiliki akses.");
        }
    }
}
