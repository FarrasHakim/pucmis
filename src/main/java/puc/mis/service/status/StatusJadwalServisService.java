package puc.mis.service.status;

import puc.mis.model.StatusJadwalServisModel;

import java.util.List;

public interface StatusJadwalServisService {
    StatusJadwalServisModel getById(Integer id);

    List<StatusJadwalServisModel> getAll();
}
