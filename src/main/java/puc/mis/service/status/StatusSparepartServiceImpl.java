package puc.mis.service.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import puc.mis.model.StatusSparepartModel;
import puc.mis.repository.StatusSparepartDb;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class StatusSparepartServiceImpl implements StatusSparepartService {

    @Autowired
    StatusSparepartDb statusSparepartDb;

    @Override
    public StatusSparepartModel getById(Integer id) {
        Optional<StatusSparepartModel> statusSparepart = statusSparepartDb.findById(id);
        if (statusSparepart.isPresent()) {
            return statusSparepart.get();
        } else {
            throw new NoSuchElementException("StatusSparepart with id " + id + " doesn't exist");
        }
    }

    @Override
    public List<StatusSparepartModel> getAll() {
        return statusSparepartDb.findAll();
    }

}
