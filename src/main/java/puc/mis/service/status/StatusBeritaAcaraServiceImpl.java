package puc.mis.service.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import puc.mis.model.StatusBeritaAcaraModel;
import puc.mis.repository.StatusBeritaAcaraDb;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class StatusBeritaAcaraServiceImpl implements StatusBeritaAcaraService {

    @Autowired
    StatusBeritaAcaraDb statusBeritaAcaraDb;

    @Override
    public StatusBeritaAcaraModel getById(Integer id) {
        Optional<StatusBeritaAcaraModel> statusBeritaAcara = statusBeritaAcaraDb.findById(id);
        if (statusBeritaAcara.isPresent()) {
            return statusBeritaAcara.get();
        } else {
            throw new NoSuchElementException("Status Berita Acara  with id " + id + " doesn't exist");
        }
    }

    @Override
    public List<StatusBeritaAcaraModel> getAll() {
        return statusBeritaAcaraDb.findAll();
    }

}
