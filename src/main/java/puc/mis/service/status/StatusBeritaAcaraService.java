package puc.mis.service.status;

import puc.mis.model.StatusBeritaAcaraModel;

import java.util.List;

public interface StatusBeritaAcaraService {
    StatusBeritaAcaraModel getById(Integer id);

    List<StatusBeritaAcaraModel> getAll();
}
