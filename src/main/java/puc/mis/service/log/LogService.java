package puc.mis.service.log;

import puc.mis.model.LogModel;

import java.util.List;

public interface LogService {
    LogModel add(String judul, String deskripsi, String idSumber, String sumber);

    void delete(Integer idLog);

    void deleteAll(String idSumber, String sumber);

    LogModel save(LogModel log);

    List<LogModel> get(String idSumber, String sumber);
}
