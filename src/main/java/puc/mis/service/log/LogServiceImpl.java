package puc.mis.service.log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import puc.mis.model.LogModel;
import puc.mis.repository.LogDb;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class LogServiceImpl implements LogService {

    @Autowired
    private LogDb logDb;

    @Override
    public LogModel add(String judul, String deskripsi, String idSumber, String sumber) {
        LogModel newLog = new LogModel();

        newLog.setJudul(judul);
        newLog.setDeskripsi(deskripsi);
        newLog.setIdSumber(idSumber);
        newLog.setSumber(sumber);
        newLog.setTanggal(LocalDateTime.now());

        return logDb.save(newLog);
    }

    @Override
    public void delete(Integer idLog) {
        Optional<LogModel> target = logDb.findById(idLog);

        if (target.isPresent()) {
            logDb.delete(target.get());
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void deleteAll(String idSumber, String sumber) {
        logDb.deleteAll(get(idSumber, sumber));
    }

    @Override
    public LogModel save(LogModel log) {
        return logDb.save(log);
    }

    @Override
    public List<LogModel> get(String idSumber, String sumber) {
        return logDb.findAllByIdSumberAndSumber(idSumber, sumber);
    }
}
