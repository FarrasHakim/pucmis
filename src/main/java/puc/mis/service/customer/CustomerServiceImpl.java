package puc.mis.service.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import puc.mis.dto.customer.FormCustomerDTO;
import puc.mis.model.*;
import puc.mis.repository.CustomerDb;
import puc.mis.service.area.AreaService;
import puc.mis.service.cabang.CabangService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.log.LogService;
import puc.mis.service.notifikasi.NotifikasiService;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDb customerDb;

    @Autowired
    private KaryawanService karyawanService;

    @Autowired
    private NotifikasiService notifikasiService;

    @Autowired
    private CabangService cabangService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private LogService logService;

    @Override
    public CustomerModel getById(Integer id) {
        Optional<CustomerModel> customer = customerDb.findById(id);
        if (customer.isPresent()) {
            return customer.get();
        } else {
            throw new NoSuchElementException("Customer with id " + id + " doesn't exist");
        }
    }

    @Override
    public List<CustomerModel> getAll() {
        return customerDb.findAll();
    }

    @Override
    public List<CustomerModel> getCustomerInCabang(Integer idCabang) {
        CabangModel cabang = cabangService.getById(idCabang);

        return customerDb.findAllByCabang(cabang);
    }

    @Override
    public void deleteById(Integer id) {
        Optional<CustomerModel> customer = customerDb.findById(id);
        if (customer.isPresent()) {
            String nama = customer.get().getNama();

            if (!conditionChecker(customer.get(), "delete")) {
                throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Tidak dapat menghapus Customer");
            }

            customerDb.delete(customer.get());

            //notifikasi
            String isi = "Customer dengan nama " + nama + " telah dihapus oleh " + karyawanService.getUserLogin().getNama();
            NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penghapusan Customer", isi, "/", "Admin");
            //

            //logs
            logService.deleteAll(id.toString(), "Customer");
            //
        } else {
            throw new NoSuchElementException("Customer with id " + id + " doesn't exist");
        }
    }

    @Override
    public CustomerModel save(FormCustomerDTO customer) {
        CustomerModel newCustomer = new CustomerModel();
        newCustomer.setNama(customer.getNama());
        newCustomer.setAlamat(customer.getAlamat());
        newCustomer.setNomorTelepon(customer.getNomorTelepon());
        newCustomer.setNomorFax(customer.getNomorFax());
        newCustomer.setEmail(customer.getEmail());
        newCustomer.setCabang(cabangService.getById(customer.getCabangId()));
        if (customer.getAreaId() != null) {
            newCustomer.setArea(areaService.getById(customer.getAreaId()));
        }
        newCustomer.setDaftarPo(new ArrayList<PurchaseOrderModel>());
        newCustomer.setDaftarKontrakServis(new ArrayList<KontrakServisModel>());

        newCustomer = customerDb.save(newCustomer);

        //notifikasi
        String isi = "Customer dengan nama " + newCustomer.getNama() + " berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        String src = "/customer/profil/" + newCustomer.getId();
        String roles = "Admin,Bagian PO,Bagian Servis,Kepala Area,Finance PO,Finance Servis,Direktur Keuangan,Koordinator Maintenance,Marketing,Manajemen";
        NotifikasiModel notif = notifikasiService.addNotifikasiForRoleInCabang("Penambahan Purchase Order", isi, src, roles, customer.getCabangId());
        //

        //logs
        String judul = "Customer dibuat";
        String deskripsi = "Customer baru dibuat oleh " + karyawanService.getUserLogin().getNama();
        logService.add(judul, deskripsi, newCustomer.getId().toString(), "Customer");
        //

        return newCustomer;
    }

    @Override
    public CustomerModel update(FormCustomerDTO customer) {
        Optional<CustomerModel> newCustomer = customerDb.findById(customer.getId());
        if (newCustomer.isPresent()) {
            newCustomer.get().setNama(customer.getNama());
            newCustomer.get().setAlamat(customer.getAlamat());
            newCustomer.get().setNomorTelepon(customer.getNomorTelepon());
            newCustomer.get().setNomorFax(customer.getNomorFax());
            newCustomer.get().setEmail(customer.getEmail());
            if (customer.getCabangId() != null) {
                newCustomer.get().setCabang(cabangService.getById(customer.getCabangId()));
            }
            if (customer.getAreaId() != null) {
                newCustomer.get().setArea(areaService.getById(customer.getAreaId()));
            }
        } else {
            throw new NoSuchElementException("Customer with id " + customer.getId() + " doesn't exist");
        }

        //notifikasi
        String isi = "Customer dengan nama " + customer.getNama() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perubahan Customer", isi, "/", "Admin");
        //

        //logs
        String judul = "Customer diubah";
        String deskripsi = "Customer diubah oleh " + karyawanService.getUserLogin().getNama();
        logService.add(judul, deskripsi, newCustomer.get().getId().toString(), "Customer");
        //

        return customerDb.save(newCustomer.get());
    }

    @Override
    public CustomerModel masukArea(Integer idCustomer, Integer idArea) {
        CustomerModel customer = customerDb.findById(idCustomer).get();
        AreaModel area = (idArea == null || idArea == 0) ? null : areaService.getById(idArea);

        customer.setArea(area);
        if (area != null) {
            customer.setCabang(area.getCabang());
        }

        return customerDb.save(customer);
    }

    @Override
    public CustomerModel masukCabang(Integer idCustomer, Integer idCabang) {
        CustomerModel customer = customerDb.findById(idCustomer).get();
        CabangModel cabang = (idCabang == null || idCabang == 0) ? null : cabangService.getById(idCabang);

        if (cabang != null) {
            customer.setArea(null);
            customer.setCabang(cabang);
        }

        return customerDb.save(customer);
    }

    @Override
    public List<InvoiceModel> getInvoices(Integer idCustomer) {
        Set<InvoiceModel> daftarInvoice = new HashSet<>();
        CustomerModel customer = getById(idCustomer);

        for (PurchaseOrderModel po : customer.getDaftarPo()) {
            for (BeritaAcaraPOModel bapo : po.getDaftarBeritaAcara()) {
                daftarInvoice.add(bapo.getInvoice());
            }
        }

        for (KontrakServisModel ks : customer.getDaftarKontrakServis()) {
            for (BeritaAcaraServisModel baks : ks.getDaftarBeritaAcara()) {
                daftarInvoice.add(baks.getInvoice());
            }
        }

        return new ArrayList<>(daftarInvoice);
    }

    private Boolean conditionChecker(CustomerModel customer, String source) {

        boolean con = true;

        if (source.contains("delete")) {

            List<KontrakServisModel> daftarKontrak = customer.getDaftarKontrakServis();
            List<PurchaseOrderModel> daftarPO = customer.getDaftarPo();

            con &= (daftarKontrak == null || daftarKontrak.isEmpty()) && (daftarPO == null || daftarPO.isEmpty());

        }

        return con;
    }
}
