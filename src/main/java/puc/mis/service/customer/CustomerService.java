package puc.mis.service.customer;

import puc.mis.dto.customer.FormCustomerDTO;
import puc.mis.model.CustomerModel;
import puc.mis.model.InvoiceModel;

import java.util.List;

public interface CustomerService {
    CustomerModel getById(Integer id);

    List<CustomerModel> getAll();

    List<CustomerModel> getCustomerInCabang(Integer idCabang);

    void deleteById(Integer id);

    CustomerModel save(FormCustomerDTO customer);

    CustomerModel update(FormCustomerDTO customer);

    CustomerModel masukArea(Integer idCustomer, Integer idArea);

    CustomerModel masukCabang(Integer idCustomer, Integer idCabang);

    List<InvoiceModel> getInvoices(Integer idCustomer);
}
