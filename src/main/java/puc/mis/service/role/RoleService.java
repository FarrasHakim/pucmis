package puc.mis.service.role;

import puc.mis.model.RoleModel;

import java.util.List;


public interface RoleService {
    List<RoleModel> findAll();

    RoleModel getById(Integer id);

    RoleModel getByNama(String nama);
}