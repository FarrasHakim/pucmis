package puc.mis.service.role;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import puc.mis.model.RoleModel;
import puc.mis.repository.RoleDb;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleDb roleDb;

    @Override
    public List<RoleModel> findAll() {
        return roleDb.findAll();
    }

    @Override
    public RoleModel getById(Integer id) {
        Optional<RoleModel> role = roleDb.findById(id);

        if (role.isPresent()) {
            return role.get();
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public RoleModel getByNama(String nama) {
        Optional<RoleModel> role = roleDb.findByNama(nama);

        if (role.isPresent()) {
            return role.get();
        } else {
            throw new NoSuchElementException();
        }
    }
}