package puc.mis.service.servis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import puc.mis.dto.beritaAcara.FormBeritaAcaraServisDTO;
import puc.mis.dto.servis.FormJadwalServisDTO;
import puc.mis.model.JadwalServisModel;
import puc.mis.model.KaryawanModel;
import puc.mis.model.KontrakServisModel;
import puc.mis.model.NotifikasiModel;
import puc.mis.repository.JadwalServisDb;
import puc.mis.service.beritaAcara.BeritaAcaraServisService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.notifikasi.NotifikasiService;
import puc.mis.service.status.StatusJadwalServisService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class JadwalServisServiceImpl implements JadwalServisService {

    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    @Autowired
    JadwalServisDb jadwalServisDb;
    @Autowired
    KaryawanService karyawanService;
    @Autowired
    NotifikasiService notifikasiService;
    @Autowired
    KontrakServisService kontrakServisService;
    @Autowired
    StatusJadwalServisService statusJadwalServisService;
    @Autowired
    BeritaAcaraServisService beritaAcaraServisService;

    @Override
    public JadwalServisModel getById(Integer id) {
        Optional<JadwalServisModel> jadwalServis = jadwalServisDb.findById(id);
        if (jadwalServis.isPresent()) {
            return jadwalServis.get();
        } else {
            throw new NoSuchElementException("JadwalServis with id " + id + " doesn't exist");
        }
    }

    @Override
    public List<JadwalServisModel> getAll() {
        return jadwalServisDb.findAll();
    }

    @Override
    public void deleteById(Integer id) {
        Optional<JadwalServisModel> jadwalServis = jadwalServisDb.findById(id);
        if (jadwalServis.isPresent()) {
            Integer idJadwalServis = jadwalServis.get().getId();

            if (!conditionChecker(jadwalServis.get(), "delete")) {
                throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Tidak dapat menghapus jadwal servis");
            }

            jadwalServisDb.delete(jadwalServis.get());

            //notifikasi
            String isi = "JadwalServis dengan id " + idJadwalServis + " telah dihapus oleh " + karyawanService.getUserLogin().getNama();
            NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penghapusan JadwalServis", isi, "/", "Admin");
            //
        } else {
            throw new NoSuchElementException("JadwalServis with id " + id + " doesn't exist");
        }
    }

    @Override
    public JadwalServisModel save(FormJadwalServisDTO jadwalServis) {
        JadwalServisModel newJadwalServis = new JadwalServisModel();
        newJadwalServis.setKontrakServis(kontrakServisService.getById(jadwalServis.getKontrakServisId()));

        String date = jadwalServis.getTanggalServis();
        LocalDate localDate = LocalDate.parse(date, dateFormat);
        newJadwalServis.setTanggalDeadlineServis(localDate);

        date = jadwalServis.getTanggalPelaksanaan();
        localDate = LocalDate.parse(date, dateFormat);
        newJadwalServis.setTanggalPelaksanaan(localDate);

        date = jadwalServis.getTanggalTerimaFormFisik();
        localDate = LocalDate.parse(date, dateFormat);
        newJadwalServis.setTanggalTerimaFormFisik(localDate);
        newJadwalServis.setStatus(statusJadwalServisService.getById(jadwalServis.getStatusJadwalServisId()));

        String isi = "JadwalServis baru berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penambahan JadwalServis ", isi, "/", "Admin");
        notifikasiService.addNotifikasiForRoleInCabang("Penambahan JadwalServis ", isi, "/", "Marketing", newJadwalServis.getKontrakServis().getCustomer().getCabang().getId());
        return jadwalServisDb.save(newJadwalServis);
    }

    @Override
    public JadwalServisModel update(FormJadwalServisDTO jadwalServis) {
        Optional<JadwalServisModel> newJadwalServis = jadwalServisDb.findById(jadwalServis.getId());
        if (newJadwalServis.isPresent()) {
            newJadwalServis.get().setKontrakServis(kontrakServisService.getById(jadwalServis.getKontrakServisId()));

            String date = jadwalServis.getTanggalServis();
            LocalDate localDate = LocalDate.parse(date, dateFormat);
            newJadwalServis.get().setTanggalDeadlineServis(localDate);

            date = jadwalServis.getTanggalPelaksanaan();
            localDate = LocalDate.parse(date, dateFormat);
            newJadwalServis.get().setTanggalPelaksanaan(localDate);

            date = jadwalServis.getTanggalTerimaFormFisik();
            localDate = LocalDate.parse(date, dateFormat);
            newJadwalServis.get().setTanggalTerimaFormFisik(localDate);
            newJadwalServis.get().setStatus(statusJadwalServisService.getById(jadwalServis.getStatusJadwalServisId()));

        } else {
            throw new NoSuchElementException("JadwalServis with id " + jadwalServis.getId() + " doesn't exist");
        }

        JadwalServisModel updatedJadwalServis = jadwalServisDb.save(newJadwalServis.get());

        //notifikasi
        String isi = "JadwalServis dengan id " + jadwalServis.getId() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perubahan JadwalServis", isi, "/", "Admin");
        notifikasiService.addNotifikasiForRoleInCabang("Perubahan JadwalServis ", isi, "/", "Marketing", newJadwalServis.get().getKontrakServis().getCustomer().getCabang().getId());
        //

        List<JadwalServisModel> jadwalServisDiKontrak = updatedJadwalServis.getKontrakServis().getDaftarJadwalServis();
        boolean isStatusAllServis = true;
        for (int i = 0; i < jadwalServisDiKontrak.size(); i++) {
            if (jadwalServisDiKontrak.get(i).getStatus().getId() != 2) {
                isStatusAllServis = false;
                KontrakServisModel kontrakServis = updatedJadwalServis.getKontrakServis();
                isi = "Jadwal servis dengan tanggal servis " + jadwalServisDiKontrak.get(i).getTanggalDeadlineServis().toString() + " belum di servis.";
                if (kontrakServis.getCustomer().getArea().getKepalaArea() != null) {
                    notif = notifikasiService.addNotifikasiForKaryawan("Info Jadwal Servis", isi, "/", kontrakServis.getCustomer().getArea().getKepalaArea().getUuid());
                } else {
                    notif = notifikasiService.addNotifikasiForRoleInCabang("Info Jadwal Servis", isi, "/", "Bagian Servis", kontrakServis.getCustomer().getArea().getCabang().getId());
                }
            }
        }
        if (isStatusAllServis) {
            FormBeritaAcaraServisDTO beritaAcaraServisDTO = new FormBeritaAcaraServisDTO();
            beritaAcaraServisDTO.setKontrakServisId(updatedJadwalServis.getKontrakServis().getId());
            beritaAcaraServisDTO.setStatusBeritaAcaraId(2);
            beritaAcaraServisService.init(beritaAcaraServisDTO);
        }

        return updatedJadwalServis;
    }

    @Override
    public JadwalServisModel init(FormJadwalServisDTO jadwalServis) {
        JadwalServisModel newJadwalServis = new JadwalServisModel();
        newJadwalServis.setKontrakServis(kontrakServisService.getById(jadwalServis.getKontrakServisId()));
        String date = jadwalServis.getTanggalServis();
        LocalDate localDate = LocalDate.parse(date, dateFormat);
        newJadwalServis.setTanggalDeadlineServis(localDate);
        date = "2011-11-11";
        localDate = LocalDate.parse(date, dateFormat);
        newJadwalServis.setTanggalPelaksanaan(localDate);
        localDate = LocalDate.parse(date, dateFormat);
        newJadwalServis.setTanggalTerimaFormFisik(localDate);
        newJadwalServis.setStatus(statusJadwalServisService.getById(jadwalServis.getStatusJadwalServisId()));
        String isi = "JadwalServis baru berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penambahan JadwalServis ", isi, "/", "Admin");
        return jadwalServisDb.save(newJadwalServis);
    }

    @Override
    public List<JadwalServisModel> getAllByKontrakServis(Integer kontrakServisId) {
        return jadwalServisDb.findAllByKontrakServis(kontrakServisService.getById(kontrakServisId));
    }

    private Boolean conditionChecker(JadwalServisModel jadwal, String source) {

        boolean con = true;

        if (source.contains("delete")) {

            KaryawanModel karyawan = karyawanService.getUserLogin();

            con &= Arrays.asList("Admin", "Marketing").contains(karyawan.getRole().getNama());
        }

        return con;
    }

}
