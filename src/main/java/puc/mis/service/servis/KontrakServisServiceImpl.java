package puc.mis.service.servis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import puc.mis.dto.servis.FormJadwalServisDTO;
import puc.mis.dto.servis.FormKontrakServisDTO;
import puc.mis.dto.servis.UnitServisDTO;
import puc.mis.model.*;
import puc.mis.repository.DataUnitKontrakServisDb;
import puc.mis.repository.KontrakServisDb;
import puc.mis.repository.MerkKontrakServisDb;
import puc.mis.repository.UnitKontrakServisDb;
import puc.mis.service.customer.CustomerService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.notifikasi.NotifikasiService;
import puc.mis.service.status.StatusKontrakServisService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
@Transactional
public class KontrakServisServiceImpl implements KontrakServisService {

    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    @Autowired
    KaryawanService karyawanService;
    @Autowired
    KontrakServisDb kontrakServisDb;
    @Autowired
    NotifikasiService notifikasiService;
    @Autowired
    CustomerService customerService;
    @Autowired
    StatusKontrakServisService statusKontrakServisService;
    @Autowired
    JadwalServisService jadwalServisService;
    @Autowired
    MerkKontrakServisDb merkKontrakServisDb;
    @Autowired
    UnitKontrakServisDb unitKontrakServisDb;
    @Autowired
    DataUnitKontrakServisDb dataUnitKontrakServisDb;

    @Override
    public List<KontrakServisModel> getAll() {
        if (karyawanService.getUserLogin().getRole().getNama().equalsIgnoreCase("Admin")) {
            return kontrakServisDb.findAll();
        } else {
            throw new ResponseStatusException(
                    HttpStatus.FORBIDDEN, "Anda tidak memiliki akses.");
        }
    }

    @Override
    public KontrakServisModel getById(Integer id) {
        Optional<KontrakServisModel> kontrakServis = kontrakServisDb.findById(id);
        if (kontrakServis.isPresent()) {
            return kontrakServis.get();
        } else {
            throw new NoSuchElementException("Kontrak Servis with id " + id + " doesn't exist");
        }
    }

    @Override
    public void deleteById(Integer id) {
        Optional<KontrakServisModel> kontrakServis = kontrakServisDb.findById(id);
        if (kontrakServis.isPresent()) {
            String nomorKontrak = kontrakServis.get().getNomorKontrak();

            if (!conditionChecker(kontrakServis.get(), "delete")) {
                throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Tidak dapat menghapus kontrak servis");
            }

            kontrakServisDb.delete(kontrakServis.get());

            //notifikasi
            String isi = "Kontrak Servis dengan nomor " + nomorKontrak + " telah dihapus oleh " + karyawanService.getUserLogin().getNama();
            NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penghapusan Kontrak Servis", isi, "/", "Admin");
            //
        } else {
            throw new NoSuchElementException("Kontrak Servis with id " + id + " doesn't exist");
        }
    }

    @Override
    public KontrakServisModel save(FormKontrakServisDTO kontrakServis) {
        KontrakServisModel newKontrakServis = new KontrakServisModel();
        newKontrakServis.setNomorKontrak(kontrakServis.getNomorKontrak());
        newKontrakServis.setCustomer(customerService.getById(kontrakServis.getCustomerId()));

        List<DataUnitKontrakServisModel> daftarDataUnit = new ArrayList<>();
        DataUnitKontrakServisModel dataUnit;
        UnitKontrakServisModel unitServis;
        MerkKontrakServisModel merkServis;

        String date = kontrakServis.getPeriodeAwal();
        LocalDate localDate = LocalDate.parse(date, dateFormat);
        newKontrakServis.setPeriodeAwal(localDate);
        date = kontrakServis.getPeriodeAkhir();
        localDate = LocalDate.parse(date, dateFormat);

        newKontrakServis.setPeriodeAkhir(localDate);
        newKontrakServis.setFrekuensiPerBulan(kontrakServis.getFrekuensiPerBulan());
        newKontrakServis.setDaftarJadwalServis(new ArrayList<JadwalServisModel>());
        newKontrakServis.setDaftarBeritaAcara(new ArrayList<BeritaAcaraServisModel>());
        newKontrakServis.setPenanggungJawab(karyawanService.getKaryawanById(kontrakServis.getPenanggungJawabId()));
        newKontrakServis.setStatus(statusKontrakServisService.getById(1));
        newKontrakServis.setPembuatKontrak(karyawanService.getUserLogin());
        newKontrakServis.setAlamatServis(kontrakServis.getAlamatServis());

        newKontrakServis = kontrakServisDb.save(newKontrakServis);
        for (UnitServisDTO unitServisDTO : kontrakServis.getDaftarUnitServis()) {
            dataUnit = new DataUnitKontrakServisModel();
            unitServis = new UnitKontrakServisModel();
            merkServis = new MerkKontrakServisModel();
            unitServis.setUnit(unitServisDTO.getUnit());
            unitKontrakServisDb.save(unitServis);
            merkServis.setMerk(unitServisDTO.getMerk());
            merkKontrakServisDb.save(merkServis);
            dataUnit.setKontrakServis(newKontrakServis);
            dataUnit.setUnitKontrakServis(unitServis);
            dataUnit.setMerkKontrakServis(merkServis);
            dataUnit.setQuantity(unitServisDTO.getQuantity());
            dataUnitKontrakServisDb.save(dataUnit);
            daftarDataUnit.add(dataUnit);
        }

        newKontrakServis.setDaftarUnit(daftarDataUnit);
        newKontrakServis = kontrakServisDb.save(newKontrakServis);

        long noOfDaysBetween = newKontrakServis.getPeriodeAwal().until(newKontrakServis.getPeriodeAkhir(), DAYS);
        long daysBeforeServis = noOfDaysBetween / (newKontrakServis.getFrekuensiPerBulan());

        String isi = "Kontrak Servis dengan nomor " + newKontrakServis.getNomorKontrak() + " berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();

        String src = "/kontrak-servis/profil/" + newKontrakServis.getId();
        String roles = "Admin,Bagian Servis,Kepala Area,Koordinator Maintenance,Manajemen,Marketing";
        NotifikasiModel notif = notifikasiService.addNotifikasiForRoleInCabang("Penambahan Kontrak Servis", isi, src, roles, newKontrakServis.getCustomer().getCabang().getId());
        LocalDate temp = newKontrakServis.getPeriodeAwal();
        FormJadwalServisDTO formJadwalServis = new FormJadwalServisDTO();
        for (int i = 0; i <= newKontrakServis.getFrekuensiPerBulan() && (temp.isBefore(newKontrakServis.getPeriodeAkhir())); i++) {
            temp = temp.plusDays(daysBeforeServis);
            // Create Jadwal servis
            formJadwalServis.setKontrakServisId(newKontrakServis.getId());
            formJadwalServis.setTanggalServis(temp.toString());
            formJadwalServis.setStatusJadwalServisId(1);

            jadwalServisService.init(formJadwalServis);
            isi = "Jadwal servis dengan tanggal servis " + temp.toString() + " telah ditambahkan.";
            if (newKontrakServis.getCustomer().getArea() != null && newKontrakServis.getCustomer().getArea().getKepalaArea() != null) {
                notif = notifikasiService.addNotifikasiForKaryawan("Penambahan Kontrak Servis", isi, src, newKontrakServis.getCustomer().getArea().getKepalaArea().getUuid());
            } else {
                notif = notifikasiService.addNotifikasiForRoleInCabang("Penambahan Kontrak Servis", isi, src, "Bagian Servis", newKontrakServis.getCustomer().getCabang().getId());
            }

        }
        return newKontrakServis;
    }

    @Override
    public KontrakServisModel update(FormKontrakServisDTO kontrakServis) {
        Optional<KontrakServisModel> newKontrakServis = kontrakServisDb.findById(kontrakServis.getId());
        if (newKontrakServis.isPresent()) {
            newKontrakServis.get().setNomorKontrak(kontrakServis.getNomorKontrak());
            newKontrakServis.get().setCustomer(customerService.getById(kontrakServis.getCustomerId()));

            List<DataUnitKontrakServisModel> daftarDataUnit = new ArrayList<DataUnitKontrakServisModel>();
//            List<Integer> idMerksToBeDeleted = new ArrayList<>();
//            List<Integer> idUnitsToBeDeleted = new ArrayList<>();

            for (DataUnitKontrakServisModel dataUnitKontrakServis: newKontrakServis.get().getDaftarUnit()) {
                merkKontrakServisDb.deleteById(dataUnitKontrakServis.getMerkKontrakServis().getId());
                unitKontrakServisDb.deleteById(dataUnitKontrakServis.getUnitKontrakServis().getId());
            }

            dataUnitKontrakServisDb.deleteAll(newKontrakServis.get().getDaftarUnit());

            DataUnitKontrakServisModel dataUnit;
            UnitKontrakServisModel unitServis;
            MerkKontrakServisModel merkServis;

            for (UnitServisDTO unitServisDTO : kontrakServis.getDaftarUnitServis()) {
                System.out.println(unitServisDTO.getMerk());
                dataUnit = new DataUnitKontrakServisModel();
                unitServis = new UnitKontrakServisModel();
                merkServis = new MerkKontrakServisModel();
                merkServis.setMerk(unitServisDTO.getMerk());
                merkKontrakServisDb.save(merkServis);
                unitServis.setUnit(unitServisDTO.getUnit());
                unitKontrakServisDb.save(unitServis);
                dataUnit.setKontrakServis(newKontrakServis.get());
                dataUnit.setMerkKontrakServis(merkServis);
                dataUnit.setUnitKontrakServis(unitServis);
                dataUnit.setQuantity(unitServisDTO.getQuantity());
                dataUnitKontrakServisDb.save(dataUnit);
                daftarDataUnit.add(dataUnit);
            }
            newKontrakServis.get().setDaftarUnit(daftarDataUnit);

            String date = kontrakServis.getPeriodeAwal();
            LocalDate localDate = LocalDate.parse(date, dateFormat);
            newKontrakServis.get().setPeriodeAwal(localDate);

            date = kontrakServis.getPeriodeAkhir();
            localDate = LocalDate.parse(date, dateFormat);
            newKontrakServis.get().setPeriodeAkhir(localDate);

            newKontrakServis.get().setFrekuensiPerBulan(kontrakServis.getFrekuensiPerBulan());
            newKontrakServis.get().setPenanggungJawab(karyawanService.getKaryawanById(kontrakServis.getPenanggungJawabId()));

            newKontrakServis.get().setAlamatServis(kontrakServis.getAlamatServis());
        } else {
            throw new NoSuchElementException("Kontrak Servis with id " + kontrakServis.getId() + "doesn't exist.");
        }

        //notifikasi
        String isi = "Kontrak Servis dengan nomor " + newKontrakServis.get().getNomorKontrak() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perubahan Kontrak Servis", isi, "/", "Admin");

        return kontrakServisDb.save(newKontrakServis.get());
    }

    private Boolean conditionChecker(KontrakServisModel kontrak, String source) {

        boolean con = true;

        if (source.contains("delete")) {

            boolean con1 = true;

            for (JadwalServisModel jadwal : kontrak.getDaftarJadwalServis()) {
                if (!jadwal.getStatus().getStatus().equals("Belum Diservis")) {
                    con1 = false;
                    break;
                }
            }

            con &= con1;
        }

        return con;
    }

}
