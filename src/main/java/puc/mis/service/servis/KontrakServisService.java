package puc.mis.service.servis;

import puc.mis.dto.servis.FormKontrakServisDTO;
import puc.mis.model.KontrakServisModel;

import java.util.List;

public interface KontrakServisService {
    List<KontrakServisModel> getAll();

    KontrakServisModel getById(Integer id);

    void deleteById(Integer id);

    KontrakServisModel save(FormKontrakServisDTO kontrakServis);

    KontrakServisModel update(FormKontrakServisDTO kontrakServis);
}
