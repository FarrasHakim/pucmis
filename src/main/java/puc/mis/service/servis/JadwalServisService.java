package puc.mis.service.servis;

import puc.mis.dto.servis.FormJadwalServisDTO;
import puc.mis.model.JadwalServisModel;

import java.util.List;

public interface JadwalServisService {
    JadwalServisModel getById(Integer id);

    List<JadwalServisModel> getAll();

    void deleteById(Integer id);

    JadwalServisModel save(FormJadwalServisDTO jadwalServis);

    JadwalServisModel update(FormJadwalServisDTO jadwalServis);

    JadwalServisModel init(FormJadwalServisDTO jadwalServis);

    List<JadwalServisModel> getAllByKontrakServis(Integer kontrakServisId);
}
