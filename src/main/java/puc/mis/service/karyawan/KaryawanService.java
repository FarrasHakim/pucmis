package puc.mis.service.karyawan;

import puc.mis.dto.karyawan.FormKaryawanDTO;
import puc.mis.model.CabangModel;
import puc.mis.model.KaryawanModel;

import java.util.List;


public interface KaryawanService {
    List<KaryawanModel> getAll();

    List<KaryawanModel> getActiveKaryawans();

    List<KaryawanModel> getActiveKaryawans(Integer idCabang);

    List<KaryawanModel> getActiveKaryawansKA();

    List<KaryawanModel> getActiveKaryawansKA(Integer idCabang);

    List<KaryawanModel> getActiveKaryawansKoor();

    List<KaryawanModel> getActiveKaryawansKoor(Integer idCabang);

    KaryawanModel addKaryawan(FormKaryawanDTO karyawan);

    KaryawanModel changeKaryawan(FormKaryawanDTO karyawan);

    void deleteKaryawan(String uuid);

    String encrypt(String password);

    KaryawanModel getKaryawanByUsername(String username);

    List<KaryawanModel> getByRole(String roleStr);

    List<KaryawanModel> getByRoleAndCabang(String roleStr, CabangModel cabang);

    KaryawanModel getUserLogin();

    KaryawanModel getKaryawanById(String id);

    List<KaryawanModel> getAllExceptAdmin();

    KaryawanModel changeCabang(String id, Integer idCabang);

    KaryawanModel changeRole(String id, Integer idRole);

    KaryawanModel changeStatus(String id, String status);
}
