package puc.mis.service.karyawan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import puc.mis.dto.karyawan.FormKaryawanDTO;
import puc.mis.model.*;
import puc.mis.repository.KaryawanDb;
import puc.mis.service.cabang.CabangService;
import puc.mis.service.log.LogService;
import puc.mis.service.notifikasi.NotifikasiService;
import puc.mis.service.role.RoleService;
import puc.mis.service.status.StatusKaryawanService;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class KaryawanServiceImpl implements KaryawanService {

    @Autowired
    private KaryawanDb karyawanDb;

    @Autowired
    private RoleService roleService;

    @Autowired
    private CabangService cabangService;

    @Autowired
    private NotifikasiService notifikasiService;

    @Autowired
    private StatusKaryawanService statusKaryawanService;

    @Autowired
    private LogService logService;

    @Override
    public List<KaryawanModel> getAll() {
        return karyawanDb.findAll();
    }

    @Override
    public List<KaryawanModel> getActiveKaryawans() {
        StatusKaryawanModel statusKaryawan = statusKaryawanService.getStatusKaryawanByStatus("Aktif");

        return karyawanDb.findAllByStatus(statusKaryawan);
    }

    @Override
    public List<KaryawanModel> getActiveKaryawans(Integer idCabang) {
        CabangModel cabang = cabangService.getById(idCabang);
        StatusKaryawanModel statusKaryawan = statusKaryawanService.getStatusKaryawanByStatus("Aktif");

        return karyawanDb.findAllByCabangAndStatus(cabang, statusKaryawan);
    }

    @Override
    public List<KaryawanModel> getActiveKaryawansKA() {
        StatusKaryawanModel statusKaryawan = statusKaryawanService.getStatusKaryawanByStatus("Aktif");
        List<KaryawanModel> daftarKaryawan = new ArrayList<>();

        daftarKaryawan.addAll(karyawanDb.findAllByCabangAndStatus(null, statusKaryawan));
        RoleModel admin = roleService.getByNama("Admin");
        daftarKaryawan.removeAll(karyawanDb.findAllByRole(admin));
        RoleModel koord = roleService.getByNama("Koordinator Maintenance");
        daftarKaryawan.removeAll(karyawanDb.findAllByRole(koord));

        return daftarKaryawan;
    }

    @Override
    public List<KaryawanModel> getActiveKaryawansKA(Integer idCabang) {
        CabangModel cabang = cabangService.getById(idCabang);
        List<KaryawanModel> daftarKaryawan = new ArrayList<>();
        StatusKaryawanModel statusKaryawan = statusKaryawanService.getStatusKaryawanByStatus("Aktif");

        daftarKaryawan.addAll(karyawanDb.findAllByCabangAndStatus(cabang, statusKaryawan));
        daftarKaryawan.addAll(karyawanDb.findAllByCabangAndStatus(null, statusKaryawan));
        RoleModel roleKaryawan = roleService.getByNama("Admin");
        daftarKaryawan.removeAll(karyawanDb.findAllByRole(roleKaryawan));
        roleKaryawan = roleService.getByNama("Koordinator Maintenance");
        daftarKaryawan.removeAll(karyawanDb.findAllByRole(roleKaryawan));

        return daftarKaryawan;
    }

    @Override
    public List<KaryawanModel> getActiveKaryawansKoor() {
        StatusKaryawanModel statusKaryawan = statusKaryawanService.getStatusKaryawanByStatus("Aktif");
        List<KaryawanModel> daftarKaryawan = new ArrayList<>();

        daftarKaryawan.addAll(karyawanDb.findAllByCabangAndStatus(null, statusKaryawan));
        RoleModel kepalaArea = roleService.getByNama("Kepala Area");
        daftarKaryawan.removeAll(karyawanDb.findAllByRole(kepalaArea));
        RoleModel admin = roleService.getByNama("Admin");
        daftarKaryawan.removeAll(karyawanDb.findAllByRole(admin));

        return daftarKaryawan;
    }

    @Override
    public List<KaryawanModel> getActiveKaryawansKoor(Integer idCabang) {
        CabangModel cabang = cabangService.getById(idCabang);
        StatusKaryawanModel statusKaryawan = statusKaryawanService.getStatusKaryawanByStatus("Aktif");
        List<KaryawanModel> daftarKaryawan = new ArrayList<>();

        daftarKaryawan.addAll(karyawanDb.findAllByCabangAndStatus(cabang, statusKaryawan));
        daftarKaryawan.addAll(karyawanDb.findAllByCabangAndStatus(null, statusKaryawan));
        RoleModel roleKaryawan = roleService.getByNama("Kepala Area");
        daftarKaryawan.removeAll(karyawanDb.findAllByRole(roleKaryawan));
        roleKaryawan = roleService.getByNama("Admin");
        daftarKaryawan.removeAll(karyawanDb.findAllByRole(roleKaryawan));

        return daftarKaryawan;
    }

    @Override
    public KaryawanModel getKaryawanByUsername(String username) {
        Optional<KaryawanModel> karyawan = karyawanDb.findByUsername(username);

        if (karyawan.isPresent()) {
            return karyawan.get();
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public List<KaryawanModel> getByRole(String roleStr) {
        RoleModel role = roleService.getByNama(roleStr);

        return karyawanDb.findAllByRole(role);
    }

    @Override
    public List<KaryawanModel> getByRoleAndCabang(String roleStr, CabangModel cabang) {
        RoleModel role = roleService.getByNama(roleStr);

        return karyawanDb.findAllByRoleAndCabang(role, cabang);
    }

    @Override
    public KaryawanModel addKaryawan(FormKaryawanDTO karyawan) {
        KaryawanModel newKaryawan = new KaryawanModel();

        if (!(karyawan.getPassword().matches("^.{8,}")) && (karyawan.getPassword().matches(".*\\d.*")) && (karyawan.getPassword().matches(".*[a-zA-Z].*"))) {
            throw new IllegalArgumentException();
        }

        newKaryawan.setNama(karyawan.getNama());
        newKaryawan.setAlamat(karyawan.getAlamat());
        newKaryawan.setNomorTelepon(karyawan.getNomorTelepon());
        newKaryawan.setEmail(karyawan.getEmail());
        newKaryawan.setUsername(karyawan.getUsername().toLowerCase());
        newKaryawan.setPassword(encrypt(karyawan.getPassword()));
        newKaryawan.setRole(roleService.getById(karyawan.getRole()));
        newKaryawan.setCabang(karyawan.getCabang() == null ? null : cabangService.getById(karyawan.getCabang()));
        newKaryawan.setStatus(statusKaryawanService.getStatusKaryawanByStatus("Aktif"));

        newKaryawan = karyawanDb.save(newKaryawan);

        //notifikasi
        String isi = "Karyawan baru dengan nama " + newKaryawan.getNama() + " telah ditambahkan oleh " + getUserLogin().getNama();
        String src = "/karyawan/profil/" + newKaryawan.getUsername();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Karyawan Baru", isi, src, "Admin,Manajemen");
        //

        //logs
        String judul = "Karyawan dibuat";
        String deskripsi = "Karyawan baru dibuat oleh " + getUserLogin().getNama();
        logService.add(judul, deskripsi, newKaryawan.getUuid(), "Karyawan");
        //

        return newKaryawan;
    }

    @Override
    public KaryawanModel changeKaryawan(FormKaryawanDTO karyawan) {
        Optional<KaryawanModel> newKaryawan = karyawanDb.findByUuid(karyawan.getUuid());

        if (karyawan.getPassword() != null && !karyawan.getPassword().isEmpty()) {
            if (!(karyawan.getPassword().matches("^.{8,}")) && (karyawan.getPassword().matches(".*\\d.*")) && (karyawan.getPassword().matches(".*[a-zA-Z].*"))) {
                throw new IllegalArgumentException();
            }
        }

        if (newKaryawan.isPresent()) {
            newKaryawan.get().setNama(karyawan.getNama());
            newKaryawan.get().setAlamat(karyawan.getAlamat());
            newKaryawan.get().setNomorTelepon(karyawan.getNomorTelepon());
            newKaryawan.get().setEmail(karyawan.getEmail());
            newKaryawan.get().setUsername(karyawan.getUsername().toLowerCase());
            if (karyawan.getPassword() != null && !karyawan.getPassword().isEmpty())
                newKaryawan.get().setPassword(encrypt(karyawan.getPassword()));
            if (karyawan.getRole() != null)
                newKaryawan.get().setRole(roleService.getById(karyawan.getRole()));
            if (karyawan.getCabang() != null)
                newKaryawan.get().setCabang(cabangService.getById(karyawan.getCabang()));
            if (karyawan.getStatus() != null)
                newKaryawan.get().setStatus(statusKaryawanService.getStatusKaryawanByStatus(karyawan.getStatus()));
        } else {
            throw new NoSuchElementException();
        }

        //notifikasi
        String isi = "Karyawan dengan nama " + karyawan.getNama() + " telah diubah oleh " + getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perubahan Karyawan", isi, "/", "Admin");
        //

        //logs
        String judul = "Karyawan diubah";
        String deskripsi = "Karyawan diubah oleh " + getUserLogin().getNama();
        logService.add(judul, deskripsi, newKaryawan.get().getUuid(), "Karyawan");
        //

        return karyawanDb.save(newKaryawan.get());
    }

    @Override
    public void deleteKaryawan(String uuid) {
        Optional<KaryawanModel> targetKaryawan = karyawanDb.findByUuid(uuid);

        if (targetKaryawan.isPresent()) {
            String namaKeryawan = targetKaryawan.get().getNama();

            if (!conditionChecker(targetKaryawan.get(), "delete")) {
                throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Tidak dapat menghapus Karyawan");
            }

            karyawanDb.delete(targetKaryawan.get());

            //notifikasi
            String isi = "Karyawan dengan nama " + namaKeryawan + " telah dihapus oleh " + getUserLogin().getNama();
            NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penghapusan Karyawan", isi, "/", "Admin");
            //

            //Logs
            logService.deleteAll(uuid, "Karyawan");
            //
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public String encrypt(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        return hashedPassword;
    }

    @Override
    public KaryawanModel getUserLogin() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        KaryawanModel karyawan = karyawanDb.findByUsername(auth.getName()).get();

        return karyawan;
    }

    @Override
    public KaryawanModel getKaryawanById(String uuid) {
        Optional<KaryawanModel> karyawan = karyawanDb.findByUuid(uuid);
        if (karyawan.isPresent()) {
            return karyawan.get();
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public List<KaryawanModel> getAllExceptAdmin() {
        RoleModel role = roleService.getByNama("Admin");
        return karyawanDb.findAllByRoleNotLike(role);
    }

    @Override
    public KaryawanModel changeCabang(String id, Integer idCabang) {
        KaryawanModel karyawan = getKaryawanById(id);
        CabangModel cabang = cabangService.getById(idCabang);

        karyawan.setCabang(cabang);

        //notifikasi
        String isi = "Karyawan dengan nama " + karyawan.getNama() + " telah dipindahkan ke cabang lain oleh " + getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perpindahan Cabang Karyawan", isi, "/", "Admin");
        //

        //logs
        String judul = "Karyawan pindah cabang";
        String deskripsi = "Karyawan dipindahkan cabang oleh " + getUserLogin().getNama();
        logService.add(judul, deskripsi, karyawan.getUuid(), "Karyawan");
        //

        return karyawanDb.save(karyawan);
    }

    @Override
    public KaryawanModel changeRole(String id, Integer idRole) {
        KaryawanModel karyawan = getKaryawanById(id);
        RoleModel role = roleService.getById(idRole);

        karyawan.setRole(role);
        if (!role.getNama().equals("Kepala Area") && karyawan.getArea() != null) {
            karyawan.getArea().setKepalaArea(null);
            karyawan.setArea(null);
        }
        if (!role.getNama().equals("Koordinator Maintenance") && karyawan.getKoorCabang() != null) {
            karyawan.getKoorCabang().setKoordinator(null);
            karyawan.setKoorCabang(null);
        }

        //notifikasi
        String isi = "Karyawan dengan nama " + karyawan.getNama() + " telah diganti jabatannya oleh " + getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Pergantian Jabatan Karyawan", isi, "/", "Admin");
        //

        //logs
        String judul = "Karyawan ganti jabatan";
        String deskripsi = "Karyawan diganti jabatannya oleh " + getUserLogin().getNama();
        logService.add(judul, deskripsi, karyawan.getUuid(), "Karyawan");
        //

        return karyawanDb.save(karyawan);
    }

    @Override
    public KaryawanModel changeStatus(String id, String statusStr) {
        KaryawanModel karyawan = getKaryawanById(id);
        StatusKaryawanModel status = statusKaryawanService.getStatusKaryawanByStatus(statusStr);

        karyawan.setStatus(status);

        return karyawanDb.save(karyawan);
    }

    private Boolean conditionChecker(KaryawanModel karyawan, String source) {

        String role = karyawan.getRole().getNama();

        boolean con = true;

        if (source.contains("delete")) {

            con &= karyawan.getKoorCabang() == null;

            if (Arrays.asList("Bagian PO", "Bagian Pemasangan", "Bagian Sparepart", "Finance PO").contains(role)) {
                List<PurchaseOrderModel> daftarPO = karyawan.getDaftarPo();

                con &= (daftarPO == null || daftarPO.isEmpty());
            }

            if (Arrays.asList("Marketing", "Kepala Area", "Bagian Servis", "Finance Servis").contains(role)) {
                List<KontrakServisModel> daftarKS = karyawan.getDaftarKontrakServis();
                List<KontrakServisModel> daftarKSDibuat = karyawan.getDaftarKontrakDibuat();

                Boolean con1 = daftarKS == null || daftarKS.isEmpty();
                Boolean con2 = daftarKSDibuat == null || daftarKSDibuat.isEmpty();

                con &= con1 && con2;
            }

            if (Arrays.asList("Finance PO", "Finance Servis").contains(role)) {
                List<InvoiceModel> daftarInvoice = karyawan.getDaftarInvoice();

                con &= daftarInvoice == null || daftarInvoice.isEmpty();
            }
        }

        return con;
    }

}

