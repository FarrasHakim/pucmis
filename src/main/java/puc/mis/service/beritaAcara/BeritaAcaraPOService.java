package puc.mis.service.beritaAcara;

import puc.mis.dto.beritaAcara.FormBeritaAcaraPODTO;
import puc.mis.model.BeritaAcaraPOModel;

import java.util.List;

public interface BeritaAcaraPOService {
    BeritaAcaraPOModel getById(Integer id);

    List<BeritaAcaraPOModel> getAll();

    void deleteById(Integer id);

    BeritaAcaraPOModel save(FormBeritaAcaraPODTO beritaAcaraPO);

    BeritaAcaraPOModel update(FormBeritaAcaraPODTO beritaAcaraPO);


    BeritaAcaraPOModel init(FormBeritaAcaraPODTO beritaAcaraPO);

    BeritaAcaraPOModel updateStatus(FormBeritaAcaraPODTO beritaAcaraPODTO);

    BeritaAcaraPOModel fill(FormBeritaAcaraPODTO beritaAcaraPODTO);
}
