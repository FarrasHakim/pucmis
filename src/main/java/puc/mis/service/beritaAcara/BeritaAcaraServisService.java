package puc.mis.service.beritaAcara;

import puc.mis.dto.beritaAcara.FormBeritaAcaraServisDTO;
import puc.mis.model.BeritaAcaraServisModel;

import java.util.List;

public interface BeritaAcaraServisService {
    BeritaAcaraServisModel getById(Integer id);

    List<BeritaAcaraServisModel> getAll();

    List<BeritaAcaraServisModel> getAllByKontrakServis(Integer id);

    void deleteById(Integer id);

    BeritaAcaraServisModel save(FormBeritaAcaraServisDTO beritaAcaraServis);

    BeritaAcaraServisModel update(FormBeritaAcaraServisDTO beritaAcaraServis);

    BeritaAcaraServisModel init(FormBeritaAcaraServisDTO beritaAcaraServis);

    BeritaAcaraServisModel fill(FormBeritaAcaraServisDTO beritaAcaraServis);
}
