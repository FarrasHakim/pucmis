package puc.mis.service.beritaAcara;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import puc.mis.dto.beritaAcara.FormBeritaAcaraPODTO;
import puc.mis.model.BeritaAcaraPOModel;
import puc.mis.model.NotifikasiModel;
import puc.mis.repository.BeritaAcaraPODb;
import puc.mis.service.invoice.InvoiceService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.notifikasi.NotifikasiService;
import puc.mis.service.purchaseOrder.PurchaseOrderService;
import puc.mis.service.status.StatusBeritaAcaraService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class BeritaAcaraPOServiceImpl implements BeritaAcaraPOService {

    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    @Autowired
    private BeritaAcaraPODb beritaAcaraPODb;
    @Autowired
    private KaryawanService karyawanService;
    @Autowired
    private NotifikasiService notifikasiService;
    @Autowired
    private PurchaseOrderService purchaseOrderService;
    @Autowired
    private StatusBeritaAcaraService statusBeritaAcaraService;
    @Autowired
    private InvoiceService invoiceService;

    @Override
    public BeritaAcaraPOModel getById(Integer id) {
        Optional<BeritaAcaraPOModel> beritaAcaraPO = beritaAcaraPODb.findById(id);
        if (beritaAcaraPO.isPresent()) {
            return beritaAcaraPO.get();
        } else {
            throw new NoSuchElementException("Berita Acara PO with id " + id + " doesn't exist");
        }
    }

    @Override
    public List<BeritaAcaraPOModel> getAll() {
        if (karyawanService.getUserLogin().getRole().getNama().equalsIgnoreCase("Admin")) {
            return beritaAcaraPODb.findAll();
        } else {
            throw new ResponseStatusException(
                    HttpStatus.FORBIDDEN, "Anda tidak memiliki akses.");
        }
    }

    @Override
    public void deleteById(Integer id) {
        Optional<BeritaAcaraPOModel> beritaAcaraPO = beritaAcaraPODb.findById(id);
        if (beritaAcaraPO.isPresent()) {
            String nomorBa = beritaAcaraPO.get().getNomorBa();
            beritaAcaraPODb.delete(beritaAcaraPO.get());

            //notifikasi
            String isi = "Berita acara PO dengan nomor " + nomorBa + " telah dihapus oleh " + karyawanService.getUserLogin().getNama();
            NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penghapusan Berita Acara PO", isi, "/", "Admin");
            //
        } else {
            throw new NoSuchElementException("Berita acara PO with id " + id + " doesn't exist");
        }
    }

    @Override
    public BeritaAcaraPOModel save(FormBeritaAcaraPODTO beritaAcaraPO) {
        BeritaAcaraPOModel newBeritaAcaraPO = new BeritaAcaraPOModel();
        newBeritaAcaraPO.setPurchaseOrder(purchaseOrderService.getById(beritaAcaraPO.getPurchaseOrderId()));
        newBeritaAcaraPO.setNomorBa(beritaAcaraPO.getNomorBa());
        newBeritaAcaraPO.setJenisPekerjaan(beritaAcaraPO.getJenisPekerjaan());
        newBeritaAcaraPO.setJenisBeritaAcara(beritaAcaraPO.getJenisBeritaAcara());

        String date = beritaAcaraPO.getTanggalTerbit();
        LocalDate localDate = LocalDate.parse(date, dateFormat);
        newBeritaAcaraPO.setTanggalTerbit(localDate);

        date = beritaAcaraPO.getTanggalKirim();
        localDate = LocalDate.parse(date, dateFormat);
        newBeritaAcaraPO.setTanggalKirim(localDate);

        date = beritaAcaraPO.getTanggalKembali();
        localDate = LocalDate.parse(date, dateFormat);
        newBeritaAcaraPO.setTanggalKembali(localDate);
        newBeritaAcaraPO.setStatus(statusBeritaAcaraService.getById(beritaAcaraPO.getStatusBeritaAcaraId()));
        newBeritaAcaraPO.setInvoice(invoiceService.getById(beritaAcaraPO.getInvoiceId()));

        date = beritaAcaraPO.getPeriodeBeritaAcara();
        localDate = LocalDate.parse(date, dateFormat);
        newBeritaAcaraPO.setPeriodeBeritaAcara(localDate);

        newBeritaAcaraPO.setApprove(beritaAcaraPO.isApprove());

        String isi = "Berita acara PO dengan nomor " + beritaAcaraPO.getNomorBa() + " berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penambahan Berita Acara PO", isi, "/", "Admin");

        return beritaAcaraPODb.save(newBeritaAcaraPO);
    }

    @Override
    public BeritaAcaraPOModel update(FormBeritaAcaraPODTO beritaAcaraPO) {
        Optional<BeritaAcaraPOModel> newBeritaAcaraPO = beritaAcaraPODb.findById(beritaAcaraPO.getId());
        if (newBeritaAcaraPO.isPresent()) {
            newBeritaAcaraPO.get().setPurchaseOrder(purchaseOrderService.getById(beritaAcaraPO.getPurchaseOrderId()));
            newBeritaAcaraPO.get().setNomorBa(beritaAcaraPO.getNomorBa());
            newBeritaAcaraPO.get().setJenisPekerjaan(beritaAcaraPO.getJenisPekerjaan());
            newBeritaAcaraPO.get().setJenisBeritaAcara(beritaAcaraPO.getJenisBeritaAcara());

            String date = beritaAcaraPO.getTanggalTerbit();
            LocalDate localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraPO.get().setTanggalTerbit(localDate);

            date = beritaAcaraPO.getTanggalKirim();
            localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraPO.get().setTanggalKirim(localDate);

            date = beritaAcaraPO.getTanggalKembali();
            localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraPO.get().setTanggalKembali(localDate);
            newBeritaAcaraPO.get().setStatus(statusBeritaAcaraService.getById(beritaAcaraPO.getStatusBeritaAcaraId()));
            newBeritaAcaraPO.get().setInvoice(invoiceService.getById(beritaAcaraPO.getInvoiceId()));

            date = beritaAcaraPO.getPeriodeBeritaAcara();
            localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraPO.get().setPeriodeBeritaAcara(localDate);

            newBeritaAcaraPO.get().setApprove(beritaAcaraPO.isApprove());

        } else {
            throw new NoSuchElementException("Berita acara PO with id " + beritaAcaraPO.getId() + " doesn't exist");
        }

        //notifikasi
        String isi = "Berita acara PO dengan nomor " + beritaAcaraPO.getNomorBa() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perubahan Berita Acara PO", isi, "/", "Admin");
        //

        isi = "Status Berita acara PO dengan nomor " + beritaAcaraPO.getNomorBa() + " telah diubah oleh " + karyawanService.getUserLogin().getNama() + " menjadi " + statusBeritaAcaraService.getById(beritaAcaraPO.getStatusBeritaAcaraId()).getStatus();
        if (beritaAcaraPO.getStatusBeritaAcaraId() == 1) {
            notifikasiService.addNotifikasiForRoleInCabang("Perubahan Status BA PO", isi, "/", "Finance PO, Bagian PO", newBeritaAcaraPO.get().getPurchaseOrder().getCustomer().getCabang().getId());
        } else if (beritaAcaraPO.getStatusBeritaAcaraId() == 3) {
            notifikasiService.addNotifikasiForRoleInCabang("Perubahan Status BA PO", isi, "/", "Bagian PO", newBeritaAcaraPO.get().getPurchaseOrder().getCustomer().getCabang().getId());
        }

        return beritaAcaraPODb.save(newBeritaAcaraPO.get());
    }

    @Override
    public BeritaAcaraPOModel init(FormBeritaAcaraPODTO beritaAcaraPO) {
        BeritaAcaraPOModel newBeritaAcaraPO = new BeritaAcaraPOModel();
        if (beritaAcaraPO.getStatusBeritaAcaraId() == 1) {
            newBeritaAcaraPO.setNomorBa("000000000000000000");
            newBeritaAcaraPO.setJenisBeritaAcara("PO");
            newBeritaAcaraPO.setApprove(true);
        }
        newBeritaAcaraPO.setPurchaseOrder(purchaseOrderService.getById(beritaAcaraPO.getPurchaseOrderId()));
        newBeritaAcaraPO.setJenisPekerjaan(beritaAcaraPO.getJenisPekerjaan());
        newBeritaAcaraPO.setStatus(statusBeritaAcaraService.getById(beritaAcaraPO.getStatusBeritaAcaraId()));
        newBeritaAcaraPO.setApprove(false);
        String isi = "Berita acara PO dengan nomor " + beritaAcaraPO.getNomorBa() + " berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penambahan Berita Acara PO", isi, "/", "Admin");

        return beritaAcaraPODb.save(newBeritaAcaraPO);
    }

    @Override
    public BeritaAcaraPOModel updateStatus(FormBeritaAcaraPODTO beritaAcaraPODTO) {
        Optional<BeritaAcaraPOModel> newBeritaAcaraPO = beritaAcaraPODb.findById(beritaAcaraPODTO.getId());
        if (newBeritaAcaraPO.isPresent()) {
            newBeritaAcaraPO.get().setStatus(statusBeritaAcaraService.getById(beritaAcaraPODTO.getStatusBeritaAcaraId()));

        } else {
            throw new NoSuchElementException("Berita acara PO with id " + beritaAcaraPODTO.getId() + " doesn't exist");
        }

        //notifikasi
        String isi = "Status Berita acara PO dengan nomor " + beritaAcaraPODTO.getNomorBa() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        String src = "/berita-acara-po/profil/" + newBeritaAcaraPO.get().getId();
        String status = newBeritaAcaraPO.get().getStatus().getStatus();
        String roles = (status.equals("Disetujui")) ? "Admin,Finance PO,Bagian PO, Finance Servis,Direktur Keuangan" : (status.equals("Menunggu Persetujuan")) ? "Admin,Koordinator Maintenance,Manajemen" : "Admin";
        NotifikasiModel notif = notifikasiService.addNotifikasiForRoleInCabang("Perubahan Berita Acara PO", isi, src, roles, newBeritaAcaraPO.get().getPurchaseOrder().getCustomer().getCabang().getId());
        //

        isi = "Status Berita acara PO dengan nomor " + beritaAcaraPODTO.getNomorBa() + " telah diubah oleh " + karyawanService.getUserLogin().getNama() + " menjadi " + statusBeritaAcaraService.getById(beritaAcaraPODTO.getStatusBeritaAcaraId()).getStatus();
        if (beritaAcaraPODTO.getStatusBeritaAcaraId() == 1) {
            notifikasiService.addNotifikasiForRoleInCabang("Perubahan Status BA PO", isi, "/", "Finance PO, Bagian PO", newBeritaAcaraPO.get().getPurchaseOrder().getCustomer().getCabang().getId());
        } else if (beritaAcaraPODTO.getStatusBeritaAcaraId() == 3) {
            notifikasiService.addNotifikasiForRoleInCabang("Perubahan Status BA PO", isi, "/", "Bagian PO", newBeritaAcaraPO.get().getPurchaseOrder().getCustomer().getCabang().getId());
        }

        return beritaAcaraPODb.save(newBeritaAcaraPO.get());
    }

    @Override
    public BeritaAcaraPOModel fill(FormBeritaAcaraPODTO beritaAcaraPO) {
        Optional<BeritaAcaraPOModel> newBeritaAcaraPO = beritaAcaraPODb.findById(beritaAcaraPO.getId());
        if (newBeritaAcaraPO.isPresent()) {
            newBeritaAcaraPO.get().setNomorBa(beritaAcaraPO.getNomorBa());
            newBeritaAcaraPO.get().setJenisPekerjaan(beritaAcaraPO.getJenisPekerjaan());
            newBeritaAcaraPO.get().setJenisBeritaAcara(beritaAcaraPO.getJenisBeritaAcara());

            String date = beritaAcaraPO.getTanggalTerbit();
            LocalDate localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraPO.get().setTanggalTerbit(localDate);

            date = beritaAcaraPO.getTanggalKirim();
            localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraPO.get().setTanggalKirim(localDate);

            date = beritaAcaraPO.getTanggalKembali();
            localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraPO.get().setTanggalKembali(localDate);

            date = beritaAcaraPO.getPeriodeBeritaAcara();
            localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraPO.get().setPeriodeBeritaAcara(localDate);

            newBeritaAcaraPO.get().setApprove(beritaAcaraPO.isApprove());


            newBeritaAcaraPO.get().setStatus(statusBeritaAcaraService.getById(beritaAcaraPO.getStatusBeritaAcaraId()));
        } else {
            throw new NoSuchElementException("Berita acara PO with id " + beritaAcaraPO.getId() + " doesn't exist");
        }
        //notifikasi
        String isi = "Berita acara PO dengan nomor " + beritaAcaraPO.getNomorBa() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perubahan Berita Acara PO", isi, "/", "Admin");
        //
        if (beritaAcaraPO.getStatusBeritaAcaraId() == 1) {
            isi = "Status Berita acara PO dengan nomor " + beritaAcaraPO.getNomorBa() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
            notifikasiService.addNotifikasiForRoleInCabang("Perubahan Berita Acara PO", isi, "/", "Finance PO", newBeritaAcaraPO.get().getPurchaseOrder().getCustomer().getCabang().getId());
        }
        isi = "Status Berita acara PO dengan nomor " + beritaAcaraPO.getNomorBa() + " telah diubah oleh " + karyawanService.getUserLogin().getNama() + " menjadi " + statusBeritaAcaraService.getById(beritaAcaraPO.getStatusBeritaAcaraId()).getStatus();
        if (beritaAcaraPO.getStatusBeritaAcaraId() == 1) {
            notifikasiService.addNotifikasiForRoleInCabang("Perubahan Status BA PO", isi, "/", "Finance PO, Bagian PO", newBeritaAcaraPO.get().getPurchaseOrder().getCustomer().getCabang().getId());
        } else if (beritaAcaraPO.getStatusBeritaAcaraId() == 3) {
            notifikasiService.addNotifikasiForRoleInCabang("Perubahan Status BA PO", isi, "/", "Bagian PO", newBeritaAcaraPO.get().getPurchaseOrder().getCustomer().getCabang().getId());
        }


        return beritaAcaraPODb.save(newBeritaAcaraPO.get());
    }
}
