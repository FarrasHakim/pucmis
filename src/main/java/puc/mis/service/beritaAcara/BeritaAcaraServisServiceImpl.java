package puc.mis.service.beritaAcara;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import puc.mis.dto.beritaAcara.FormBeritaAcaraServisDTO;
import puc.mis.model.BeritaAcaraServisModel;
import puc.mis.model.NotifikasiModel;
import puc.mis.repository.BeritaAcaraServisDb;
import puc.mis.service.invoice.InvoiceService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.notifikasi.NotifikasiService;
import puc.mis.service.servis.KontrakServisService;
import puc.mis.service.status.StatusBeritaAcaraService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class BeritaAcaraServisServiceImpl implements BeritaAcaraServisService {

    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    @Autowired
    InvoiceService invoiceService;
    @Autowired
    private BeritaAcaraServisDb beritaAcaraServisDb;
    @Autowired
    private KaryawanService karyawanService;
    @Autowired
    private NotifikasiService notifikasiService;
    @Autowired
    private KontrakServisService kontrakServisService;
    @Autowired
    private StatusBeritaAcaraService statusBeritaAcaraService;

    @Override
    public BeritaAcaraServisModel getById(Integer id) {
        Optional<BeritaAcaraServisModel> beritaAcaraServis = beritaAcaraServisDb.findById(id);
        if (beritaAcaraServis.isPresent()) {
            return beritaAcaraServis.get();
        } else {
            throw new NoSuchElementException("Berita Acara Servis with id " + id + " doesn't exist");
        }
    }

    @Override
    public List<BeritaAcaraServisModel> getAll() {
        return beritaAcaraServisDb.findAll();
    }

    @Override
    public List<BeritaAcaraServisModel> getAllByKontrakServis(Integer id) {
        return beritaAcaraServisDb.findAllByKontrakServis(kontrakServisService.getById(id));
    }

    @Override
    public void deleteById(Integer id) {
        Optional<BeritaAcaraServisModel> beritaAcaraServis = beritaAcaraServisDb.findById(id);
        if (beritaAcaraServis.isPresent()) {
            String nomorSpk = beritaAcaraServis.get().getNomorSpk();
            beritaAcaraServisDb.delete(beritaAcaraServis.get());

            //notifikasi
            String isi = "Berita acara servis dengan nomor spk " + nomorSpk + " telah dihapus oleh " + karyawanService.getUserLogin().getNama();
            NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penghapusan Berita Acara Servis", isi, "/", "Admin");
            //
        } else {
            throw new NoSuchElementException("BeritaAcaraServis with id " + id + " doesn't exist");
        }
    }

    @Override
    public BeritaAcaraServisModel save(FormBeritaAcaraServisDTO beritaAcaraServis) {
        BeritaAcaraServisModel newBeritaAcaraServis = new BeritaAcaraServisModel();
        newBeritaAcaraServis.setKontrakServis(kontrakServisService.getById(beritaAcaraServis.getKontrakServisId()));
        newBeritaAcaraServis.setNomorSpk(beritaAcaraServis.getNomorSpk());
        newBeritaAcaraServis.setPeriodeSpk(beritaAcaraServis.getPeriodeSpk());
        newBeritaAcaraServis.setStatus(statusBeritaAcaraService.getById(beritaAcaraServis.getStatusBeritaAcaraId()));
        newBeritaAcaraServis.setInvoice(invoiceService.getById(beritaAcaraServis.getInvoiceId()));

        String isi = "Berita acara servis dengan nomor spk " + beritaAcaraServis.getNomorSpk() + " berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penambahan Berita Acara Servis", isi, "/", "Admin");

        return beritaAcaraServisDb.save(newBeritaAcaraServis);
    }

    @Override
    public BeritaAcaraServisModel update(FormBeritaAcaraServisDTO beritaAcaraServis) {
        Optional<BeritaAcaraServisModel> newBeritaAcaraServis = beritaAcaraServisDb.findById(beritaAcaraServis.getId());
        if (newBeritaAcaraServis.isPresent()) {
            newBeritaAcaraServis.get().setKontrakServis(kontrakServisService.getById(beritaAcaraServis.getKontrakServisId()));

            newBeritaAcaraServis.get().setNomorSpk(beritaAcaraServis.getNomorSpk());

            newBeritaAcaraServis.get().setPeriodeSpk(beritaAcaraServis.getPeriodeSpk());

            newBeritaAcaraServis.get().setStatus(statusBeritaAcaraService.getById(beritaAcaraServis.getStatusBeritaAcaraId()));
            newBeritaAcaraServis.get().setInvoice(invoiceService.getById(beritaAcaraServis.getInvoiceId()));

            String date = beritaAcaraServis.getTanggalTerbit();
            LocalDate localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraServis.get().setTanggalTerbit(localDate);
            date = beritaAcaraServis.getTanggalKirim();
            localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraServis.get().setTanggalKirim(localDate);
            date = beritaAcaraServis.getTanggalKembali();
            localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraServis.get().setTanggalKembali(localDate);
            newBeritaAcaraServis.get().setStatus(statusBeritaAcaraService.getById(beritaAcaraServis.getStatusBeritaAcaraId()));
            date = beritaAcaraServis.getPeriodeBeritaAcara();
            localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraServis.get().setPeriodeBeritaAcara(localDate);
            newBeritaAcaraServis.get().setApprove(beritaAcaraServis.isApprove());
        } else {
            throw new NoSuchElementException("Berita acara servis with id " + beritaAcaraServis.getId() + " doesn't exist");
        }

        //notifikasi
        String isi = "Berita acara Servis dengan nomor spk " + beritaAcaraServis.getNomorSpk() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perubahan Berita Acara Servis", isi, "/", "Admin");
        notifikasiService.addNotifikasiForKaryawan("Penambahan Berita Acara Servis", isi, "/", newBeritaAcaraServis.get().getKontrakServis().getCustomer().getCabang().getKoordinator().getUuid());
        notifikasiService.addNotifikasiForRoleInCabang("Pengisian Berita Acara Servis", isi, "/", "Marketing", newBeritaAcaraServis.get().getKontrakServis().getCustomer().getCabang().getId());
        //

        if (newBeritaAcaraServis.get().getStatus().getId() == 1) {
            isi = "Status berita acara servis dengan nomor spk " + beritaAcaraServis.getNomorSpk() + " telah berubah menjadi disetujui oleh " + karyawanService.getUserLogin().getNama();
            notifikasiService.addNotifikasiForRole("Perubahan Status Berita Acara Servis", isi, "/", "Finance Servis, Finance PO, Direktur Keuangan, Bagian Servis");
        } else if (newBeritaAcaraServis.get().getStatus().getId() == 3) {
            isi = "Status berita acara servis dengan nomor spk " + beritaAcaraServis.getNomorSpk() + " telah berubah menjadi tidak disetujui oleh " + karyawanService.getUserLogin().getNama();
            notifikasiService.addNotifikasiForRole("Perubahan Status Berita Acara Servis", isi, "/", "Bagian Servis");

        }

        return beritaAcaraServisDb.save(newBeritaAcaraServis.get());
    }

    @Override
    public BeritaAcaraServisModel init(FormBeritaAcaraServisDTO beritaAcaraServis) {
        BeritaAcaraServisModel newBeritaAcaraServis = new BeritaAcaraServisModel();
        newBeritaAcaraServis.setKontrakServis(kontrakServisService.getById(beritaAcaraServis.getKontrakServisId()));
        newBeritaAcaraServis.setStatus(statusBeritaAcaraService.getById(beritaAcaraServis.getStatusBeritaAcaraId()));
        newBeritaAcaraServis.setApprove(false);
        String isi = "Berita acara servis dengan nomor kontrak servis " + newBeritaAcaraServis.getKontrakServis().getNomorKontrak() + " berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penambahan Berita Acara Servis", isi, "/", "Admin");
        notif = notifikasiService.addNotifikasiForKaryawan("Penambahan Berita Acara Servis", isi, "/", newBeritaAcaraServis.getKontrakServis().getCustomer().getCabang().getKoordinator().getUuid());

        return beritaAcaraServisDb.save(newBeritaAcaraServis);
    }

    @Override
    public BeritaAcaraServisModel fill(FormBeritaAcaraServisDTO beritaAcaraServis) {
        Optional<BeritaAcaraServisModel> newBeritaAcaraServis = beritaAcaraServisDb.findById(beritaAcaraServis.getId());
        if (newBeritaAcaraServis.isPresent()) {
            newBeritaAcaraServis.get().setNomorSpk(beritaAcaraServis.getNomorSpk());
            newBeritaAcaraServis.get().setPeriodeSpk(beritaAcaraServis.getPeriodeSpk());
            newBeritaAcaraServis.get().setStatus(statusBeritaAcaraService.getById(beritaAcaraServis.getStatusBeritaAcaraId()));

            String date = beritaAcaraServis.getTanggalTerbit();
            LocalDate localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraServis.get().setTanggalTerbit(localDate);

            date = beritaAcaraServis.getTanggalKirim();
            localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraServis.get().setTanggalKirim(localDate);
            date = beritaAcaraServis.getTanggalKembali();
            localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraServis.get().setTanggalKembali(localDate);
            date = beritaAcaraServis.getPeriodeBeritaAcara();
            localDate = LocalDate.parse(date, dateFormat);
            newBeritaAcaraServis.get().setPeriodeBeritaAcara(localDate);
            newBeritaAcaraServis.get().setApprove(beritaAcaraServis.isApprove());

        } else {
            throw new NoSuchElementException("Berita acara servis with id " + beritaAcaraServis.getId() + " doesn't exist");
        }

        //notifikasi
        String isi = "Berita acara Servis dengan nomor spk " + beritaAcaraServis.getNomorSpk() + " telah diisi/diubah oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Pengisian Berita Acara Servis", isi, "/", "Admin");
        notifikasiService.addNotifikasiForKaryawan("Pengisian Berita Acara Servis", isi, "/", newBeritaAcaraServis.get().getKontrakServis().getCustomer().getCabang().getKoordinator().getUuid());
        notifikasiService.addNotifikasiForRoleInCabang("Pengisian Berita Acara Servis", isi, "/", "Marketing", newBeritaAcaraServis.get().getKontrakServis().getCustomer().getCabang().getId());
        //

        if (newBeritaAcaraServis.get().getStatus().getId() == 1) {
            isi = "Status berita acara servis dengan nomor spk " + beritaAcaraServis.getNomorSpk() + " telah berubah menjadi disetujui oleh " + karyawanService.getUserLogin().getNama();
            notifikasiService.addNotifikasiForRole("Perubahan Status Berita Acara Servis", isi, "/", "Finance Servis, Finance PO, Direktur Keuangan, Bagian Servis");
        } else if (newBeritaAcaraServis.get().getStatus().getId() == 3) {
            isi = "Status berita acara servis dengan nomor spk " + beritaAcaraServis.getNomorSpk() + " telah berubah menjadi tidak disetujui oleh " + karyawanService.getUserLogin().getNama();
            notifikasiService.addNotifikasiForRole("Perubahan Status Berita Acara Servis", isi, "/", "Bagian Servis");

        }

        return beritaAcaraServisDb.save(newBeritaAcaraServis.get());
    }
}
