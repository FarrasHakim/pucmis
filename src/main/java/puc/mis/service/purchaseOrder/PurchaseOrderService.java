package puc.mis.service.purchaseOrder;

import puc.mis.dto.purchaseOrder.FormPurchaseOrderDTO;
import puc.mis.model.PurchaseOrderModel;

import java.util.List;

public interface PurchaseOrderService {
    List<PurchaseOrderModel> getAll();

    PurchaseOrderModel save(FormPurchaseOrderDTO purchaseOrder);

    PurchaseOrderModel getById(Integer id);

    PurchaseOrderModel update(FormPurchaseOrderDTO purchaseOrder);

    void deleteById(Integer id);

    PurchaseOrderModel updateStatusSparepart(FormPurchaseOrderDTO purchaseOrderDTO);

    PurchaseOrderModel cancelStatusPO(FormPurchaseOrderDTO purchaseOrderDTO);

    PurchaseOrderModel init(FormPurchaseOrderDTO purchaseOrder);
}
