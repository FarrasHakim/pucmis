package puc.mis.service.purchaseOrder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import puc.mis.dto.purchaseOrder.FormDataUnitPODTO;
import puc.mis.model.DataUnitPOModel;
import puc.mis.model.NotifikasiModel;
import puc.mis.repository.DataUnitPODb;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.notifikasi.NotifikasiService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class DataUnitPOServiceImpl implements DataUnitPOService {
    @Autowired
    DataUnitPODb dataUnitPODb;
    @Autowired
    KaryawanService karyawanService;
    @Autowired
    NotifikasiService notifikasiService;
    @Autowired
    PurchaseOrderService purchaseOrderService;
    @Autowired
    UnitPOService unitPOService;

    @Override
    public DataUnitPOModel getById(Integer id) {
        Optional<DataUnitPOModel> dataUnitPO = dataUnitPODb.findById(id);
        if (dataUnitPO.isPresent()) {
            return dataUnitPO.get();
        } else {
            throw new NoSuchElementException("DataUnitPO with id " + id + " doesn't exist");
        }
    }

    @Override
    public List<DataUnitPOModel> getAll(Integer id) {
        return dataUnitPODb.findAll();
    }

    @Override
    public void deleteById(Integer id) {
        Optional<DataUnitPOModel> dataUnitPO = dataUnitPODb.findById(id);
        if (dataUnitPO.isPresent()) {
            Integer dataUnitPOId = dataUnitPO.get().getId();
            dataUnitPODb.delete(dataUnitPO.get());

            //notifikasi
            String isi = "DataUnitPO dengan id " + dataUnitPOId + " telah dihapus oleh " + karyawanService.getUserLogin().getNama();
            NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penghapusan DataUnitPO", isi, "/", "Admin");
            //
        } else {
            throw new NoSuchElementException("DataUnitPO with id " + id + " doesn't exist");
        }
    }

    @Override
    public DataUnitPOModel save(FormDataUnitPODTO dataUnitPO) {
        DataUnitPOModel newDataUnitPO = new DataUnitPOModel();
        newDataUnitPO.setJumlahDipesan(dataUnitPO.getJumlahDipesan());
        newDataUnitPO.setPurchaseOrder(purchaseOrderService.getById(dataUnitPO.getPurchaseOrderId()));
        newDataUnitPO.setUnitPo(unitPOService.getById(dataUnitPO.getUnitPoId()));

        String isi = "DataUnitPO baru berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penambahan DataUnitPO", isi, "/", "Admin");
        return dataUnitPODb.save(newDataUnitPO);
    }

    @Override
    public DataUnitPOModel update(FormDataUnitPODTO dataUnitPO) {
        Optional<DataUnitPOModel> newDataUnitPO = dataUnitPODb.findById(dataUnitPO.getId());
        if (newDataUnitPO.isPresent()) {
            newDataUnitPO.get().setJumlahDipesan(dataUnitPO.getJumlahDipesan());
            newDataUnitPO.get().setPurchaseOrder(purchaseOrderService.getById(dataUnitPO.getPurchaseOrderId()));
            newDataUnitPO.get().setUnitPo(unitPOService.getById(dataUnitPO.getUnitPoId()));
        } else {
            throw new NoSuchElementException("DataUnitPO with id " + dataUnitPO.getId() + " doesn't exist");
        }

        //notifikasi
        String isi = "DataUnitPO dengan id " + dataUnitPO.getId() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perubahan DataUnitPO", isi, "/", "Admin");
        //

        return dataUnitPODb.save(newDataUnitPO.get());
    }
}
