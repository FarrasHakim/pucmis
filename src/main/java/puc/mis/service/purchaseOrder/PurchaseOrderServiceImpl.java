package puc.mis.service.purchaseOrder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import puc.mis.dto.beritaAcara.FormBeritaAcaraPODTO;
import puc.mis.dto.purchaseOrder.FormDataUnitPODTO;
import puc.mis.dto.purchaseOrder.FormPurchaseOrderDTO;
import puc.mis.dto.purchaseOrder.FormTambahUnitPO;
import puc.mis.model.BeritaAcaraPOModel;
import puc.mis.model.DataUnitPOModel;
import puc.mis.model.NotifikasiModel;
import puc.mis.model.PurchaseOrderModel;
import puc.mis.repository.PurchaseOrderDb;
import puc.mis.service.beritaAcara.BeritaAcaraPOService;
import puc.mis.service.customer.CustomerService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.notifikasi.NotifikasiService;
import puc.mis.service.status.StatusPOService;
import puc.mis.service.status.StatusSparepartService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;


@Service
@Transactional
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    @Autowired
    PurchaseOrderDb purchaseOrderDb;
    @Autowired
    CustomerService customerService;
    @Autowired
    KaryawanService karyawanService;
    @Autowired
    StatusPOService statusPOService;
    @Autowired
    StatusSparepartService statusSparepartService;
    @Autowired
    NotifikasiService notifikasiService;
    @Autowired
    BeritaAcaraPOService beritaAcaraPOService;
    @Autowired
    DataUnitPOService dataUnitPOService;
    @Autowired
    UnitPOService unitPOService;

    @Override
    public List<PurchaseOrderModel> getAll() {
        if (karyawanService.getUserLogin().getRole().getNama().equalsIgnoreCase("Admin")) {
            return purchaseOrderDb.findAll();
        } else {
            throw new ResponseStatusException(
                    HttpStatus.FORBIDDEN, "Anda tidak memiliki akses.");
        }
    }

    @Override
    public PurchaseOrderModel save(FormPurchaseOrderDTO purchaseOrder) {
        PurchaseOrderModel newPurchaseOrder = new PurchaseOrderModel();
        newPurchaseOrder.setNomor(purchaseOrder.getNomor());
        newPurchaseOrder.setCustomer(customerService.getById(purchaseOrder.getCustomerId()));
        newPurchaseOrder.setJenisPekerjaan(purchaseOrder.getJenisPekerjaan());
        newPurchaseOrder.setKeterangan(purchaseOrder.getKeterangan());

        String date = purchaseOrder.getTanggalTerima();
        LocalDate localDate = LocalDate.parse(date, dateFormat);

        newPurchaseOrder.setTanggalTerima(localDate);
        newPurchaseOrder.setNomorFaktur(purchaseOrder.getNomorFaktur());

        date = purchaseOrder.getTanggalPemasanganSparepart();
        localDate = LocalDate.parse(date, dateFormat);
        newPurchaseOrder.setTanggalPemasanganSparepart(localDate);

        date = purchaseOrder.getTanggalPengirimanSparepart();
        localDate = LocalDate.parse(date, dateFormat);
        newPurchaseOrder.setTanggalPengirimanSparepart(localDate);
        newPurchaseOrder.setPenanggungjawab(karyawanService.getKaryawanById(purchaseOrder.getPenanggungjawabId()));
        newPurchaseOrder.setDaftarUnit(new ArrayList<DataUnitPOModel>());
        newPurchaseOrder.setDaftarBeritaAcara(new ArrayList<BeritaAcaraPOModel>());
        newPurchaseOrder.setStatusPo(statusPOService.getById(1));
        newPurchaseOrder.setStatusSparepart(statusSparepartService.getById(1));
        newPurchaseOrder.setAlamatPemasangan(purchaseOrder.getAlamatPemasangan());

        newPurchaseOrder = purchaseOrderDb.save(newPurchaseOrder);

        for (FormTambahUnitPO dataUnitPODTO : purchaseOrder.getDaftarDataUnitPO()) {
            FormDataUnitPODTO formDataUnitPODTO = new FormDataUnitPODTO();
            formDataUnitPODTO.setJumlahDipesan(dataUnitPODTO.getJumlahDipesan());
            formDataUnitPODTO.setPurchaseOrderId(newPurchaseOrder.getId());
            formDataUnitPODTO.setUnitPoId(dataUnitPODTO.getUnitPOId());

            dataUnitPOService.save(formDataUnitPODTO);
        }

        String isi = "Purchase order dengan nomor " + newPurchaseOrder.getNomor() + " berhasil ditambahkan.";
        NotifikasiModel notif = notifikasiService.addNotifikasiForRoleInCabang("Penambahan Purchase Order", isi, "/", "Admin,Bagian Sparepart", newPurchaseOrder.getCustomer().getCabang().getId());

        return newPurchaseOrder;
    }

    @Override
    public PurchaseOrderModel getById(Integer id) {
        Optional<PurchaseOrderModel> purchaseOrder = purchaseOrderDb.findById(id);
        if (purchaseOrder.isPresent()) {
            return purchaseOrder.get();
        } else {
            throw new NoSuchElementException("PurchaseOrder with id " + id + " doesn't exist");
        }
    }

    @Override
    public PurchaseOrderModel update(FormPurchaseOrderDTO purchaseOrder) {

        Optional<PurchaseOrderModel> newPurchaseOrder = purchaseOrderDb.findById(purchaseOrder.getId());

        if (newPurchaseOrder.isPresent()) {

            newPurchaseOrder.get().setNomor(purchaseOrder.getNomor());

            newPurchaseOrder.get().setCustomer(customerService.getById(purchaseOrder.getCustomerId()));

            newPurchaseOrder.get().setJenisPekerjaan(purchaseOrder.getJenisPekerjaan());

            newPurchaseOrder.get().setKeterangan(purchaseOrder.getKeterangan());


            String date = purchaseOrder.getTanggalTerima();
            LocalDate localDate = LocalDate.parse(date, dateFormat);
            newPurchaseOrder.get().setTanggalTerima(localDate);


            newPurchaseOrder.get().setNomorFaktur(purchaseOrder.getNomorFaktur());


            date = purchaseOrder.getTanggalPemasanganSparepart();
            localDate = LocalDate.parse(date, dateFormat);
            newPurchaseOrder.get().setTanggalPemasanganSparepart(localDate);


            date = purchaseOrder.getTanggalPengirimanSparepart();
            localDate = LocalDate.parse(date, dateFormat);
            newPurchaseOrder.get().setTanggalPengirimanSparepart(localDate);

            newPurchaseOrder.get().setPenanggungjawab(karyawanService.getKaryawanById(purchaseOrder.getPenanggungjawabId()));
            newPurchaseOrder.get().setStatusSparepart(statusSparepartService.getById(purchaseOrder.getStatusSparepartId()));
            newPurchaseOrder.get().setStatusPo(statusPOService.getById(2));


            List<DataUnitPOModel> daftarDataUnitPO = new ArrayList<DataUnitPOModel>();
            DataUnitPOModel dataUnitPO;
            List<DataUnitPOModel> daftarDataUnitToBeDeleted = newPurchaseOrder.get().getDaftarUnit();

            for (FormTambahUnitPO dataUnitPODTO : purchaseOrder.getDaftarDataUnitPO()) {
                FormDataUnitPODTO formDataUnitPODTO = new FormDataUnitPODTO();
                formDataUnitPODTO.setJumlahDipesan(dataUnitPODTO.getJumlahDipesan());
                formDataUnitPODTO.setPurchaseOrderId(newPurchaseOrder.get().getId());
                formDataUnitPODTO.setUnitPoId(dataUnitPODTO.getUnitPOId());
                if (dataUnitPODTO.getId() != null) {
                    formDataUnitPODTO.setId(dataUnitPODTO.getId());
                    dataUnitPO = dataUnitPOService.update(formDataUnitPODTO);
                } else {
                    dataUnitPO = dataUnitPOService.save(formDataUnitPODTO);
                }
                daftarDataUnitPO.add(dataUnitPO);
            }
            newPurchaseOrder.get().setDaftarUnit(daftarDataUnitPO);
            daftarDataUnitToBeDeleted.removeAll(daftarDataUnitPO);
            for (DataUnitPOModel toBeDeleted : daftarDataUnitToBeDeleted) {
                dataUnitPOService.deleteById(toBeDeleted.getId());
            }


        } else {
            throw new NoSuchElementException("PurchaseOrder with id " + purchaseOrder.getId() + " doesn't exist");
        }

        //notifikasi
        String isi = "Purchase Order dengan nomor " + purchaseOrder.getNomor() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perubahan Purchase Order", isi, "/", "Admin");
        isi = "StatusSparepart PO dengan nomor " + purchaseOrder.getNomor() + " telah diubah oleh " + karyawanService.getUserLogin().getNama() + " menjadi " + statusSparepartService.getById(purchaseOrder.getStatusSparepartId()).getStatus();
        notifikasiService.addNotifikasiForRoleInCabang("Status Sparepart PO berubah", isi, "/", "Bagian PO,Bagian Sparepart", newPurchaseOrder.get().getCustomer().getCabang().getId());
        //

        FormBeritaAcaraPODTO beritaAcaraPODTO = new FormBeritaAcaraPODTO();
        if (newPurchaseOrder.get().getJenisPekerjaan().equalsIgnoreCase("pemasangan") && purchaseOrder.getStatusSparepartId() == 2) {
            notifikasiService.addNotifikasiForRoleInCabang("Perubahan Status Sparepart PO", isi, "/", "Bagian Pemasangan", newPurchaseOrder.get().getCustomer().getCabang().getId());
        }

        if ((newPurchaseOrder.get().getJenisPekerjaan().equalsIgnoreCase("pemasangan") && purchaseOrder.getStatusSparepartId() == 3)) {
            beritaAcaraPODTO.setJenisPekerjaan(newPurchaseOrder.get().getJenisPekerjaan());
            beritaAcaraPODTO.setPurchaseOrderId(purchaseOrder.getId());
            beritaAcaraPODTO.setStatusBeritaAcaraId(2);
            beritaAcaraPOService.init(beritaAcaraPODTO);
        }

        if ((!newPurchaseOrder.get().getJenisPekerjaan().equalsIgnoreCase("pemasangan") && purchaseOrder.getStatusSparepartId() == 2)) {
            beritaAcaraPODTO.setPurchaseOrderId(purchaseOrder.getId());
            beritaAcaraPODTO.setJenisBeritaAcara("PO");
            beritaAcaraPODTO.setJenisPekerjaan(newPurchaseOrder.get().getJenisPekerjaan());
            beritaAcaraPODTO.setStatusBeritaAcaraId(1);
            beritaAcaraPOService.init(beritaAcaraPODTO);
        }

        return purchaseOrderDb.save(newPurchaseOrder.get());
    }

    @Override
    public void deleteById(Integer id) {
        Optional<PurchaseOrderModel> purchaseOrder = purchaseOrderDb.findById(id);
        if (purchaseOrder.isPresent()) {
            String nomor = purchaseOrder.get().getNomor();
            purchaseOrderDb.delete(purchaseOrder.get());

            if (!conditionChecker(purchaseOrder.get(), "delete")) {
                throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Tidak dapat menghapus PO");
            }

            //notifikasi
            String isi = "Purchase Order dengan nomor " + nomor + " telah dihapus oleh " + karyawanService.getUserLogin().getNama();
            NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penghapusan Purchase Order", isi, "/", "Admin");
            //
        } else {
            throw new NoSuchElementException("PurchaseOrder with id " + id + " doesn't exist");
        }
    }

    @Override
    public PurchaseOrderModel updateStatusSparepart(FormPurchaseOrderDTO purchaseOrderDTO) {
        Optional<PurchaseOrderModel> newPurchaseOrder = purchaseOrderDb.findById(purchaseOrderDTO.getId());
        if (newPurchaseOrder.isPresent()) {
            newPurchaseOrder.get().setStatusSparepart(statusSparepartService.getById(purchaseOrderDTO.getStatusSparepartId()));
            newPurchaseOrder.get().setStatusPo(statusPOService.getById(2));
        } else {
            throw new NoSuchElementException("Purchase Order with id " + purchaseOrderDTO.getId() + " doesn't exist");
        }

        //notifikasi
        String isi = "Status Sparepart PO dengan nomor " + purchaseOrderDTO.getNomor() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perubahan Status Sparepart PO", isi, "/", "Admin");
        isi = "StatusSparepart PO dengan nomor " + purchaseOrderDTO.getNomor() + " telah diubah oleh " + karyawanService.getUserLogin().getNama() + " menjadi " + statusSparepartService.getById(purchaseOrderDTO.getStatusSparepartId()).getStatus();
        notifikasiService.addNotifikasiForRoleInCabang("Status Sparepart PO berubah", isi, "/", "Bagian PO,Bagian Sparepart", newPurchaseOrder.get().getCustomer().getCabang().getId());
        //

        FormBeritaAcaraPODTO beritaAcaraPODTO = new FormBeritaAcaraPODTO();
        if (newPurchaseOrder.get().getJenisPekerjaan().equalsIgnoreCase("pemasangan") && purchaseOrderDTO.getStatusSparepartId() == 2) {
            notifikasiService.addNotifikasiForRoleInCabang("Perubahan Status Sparepart PO", isi, "/", "Bagian Pemasangan", newPurchaseOrder.get().getCustomer().getCabang().getId());
        }
        if (!newPurchaseOrder.get().getJenisPekerjaan().equalsIgnoreCase("pemasangan") && (purchaseOrderDTO.getStatusSparepartId() == 2 || purchaseOrderDTO.getStatusSparepartId() == 3) || newPurchaseOrder.get().getJenisPekerjaan().equalsIgnoreCase("pemasangan") && purchaseOrderDTO.getStatusSparepartId() == 3) {
            beritaAcaraPODTO.setJenisPekerjaan(newPurchaseOrder.get().getJenisPekerjaan());
            beritaAcaraPODTO.setPurchaseOrderId(purchaseOrderDTO.getId());
            beritaAcaraPODTO.setStatusBeritaAcaraId(2);
            beritaAcaraPOService.init(beritaAcaraPODTO);
        }

        return purchaseOrderDb.save(newPurchaseOrder.get());
    }

    @Override
    public PurchaseOrderModel cancelStatusPO(FormPurchaseOrderDTO purchaseOrderDTO) {
        Optional<PurchaseOrderModel> newPurchaseOrder = purchaseOrderDb.findById(purchaseOrderDTO.getId());
        if (newPurchaseOrder.isPresent()) {
            if (newPurchaseOrder.get().getStatusSparepart().getId() != 2) {
                newPurchaseOrder.get().setStatusPo(statusPOService.getById(3));
            } else if (newPurchaseOrder.get().getStatusSparepart().getId() == 2) {
                throw new ResponseStatusException(
                        HttpStatus.NOT_ACCEPTABLE, "PO dengan Status Sparepart Tersedia-Belum Terpasang tidak bisa dibatalkan");
            }
        } else {
            throw new NoSuchElementException("Purchase Order with id " + purchaseOrderDTO.getId() + " doesn't exist");
        }

        //notifikasi
        String isi = "Status PO dengan nomor " + purchaseOrderDTO.getNomor() + " telah dibatalkan oleh " + karyawanService.getUserLogin().getNama();
        String src = "/purchase-order/profil/" + newPurchaseOrder.get().getId();
        String roles = "Admin,Bagian PO,Koordinator Maintenance,Manajemen";
        NotifikasiModel notif = notifikasiService.addNotifikasiForRoleInCabang("Perubahan Status PO", isi, src, roles, newPurchaseOrder.get().getCustomer().getCabang().getId());
        //

        return purchaseOrderDb.save(newPurchaseOrder.get());
    }

    @Override
    public PurchaseOrderModel init(FormPurchaseOrderDTO purchaseOrder) {
        PurchaseOrderModel newPurchaseOrder = new PurchaseOrderModel();
        newPurchaseOrder.setNomor(purchaseOrder.getNomor());
        newPurchaseOrder.setCustomer(customerService.getById(purchaseOrder.getCustomerId()));
        newPurchaseOrder.setJenisPekerjaan(purchaseOrder.getJenisPekerjaan());
        newPurchaseOrder.setKeterangan(purchaseOrder.getKeterangan());

        String date = purchaseOrder.getTanggalTerima();
        LocalDate localDate = LocalDate.parse(date, dateFormat);

        newPurchaseOrder.setTanggalTerima(localDate);
        newPurchaseOrder.setNomorFaktur(purchaseOrder.getNomorFaktur());

        date = purchaseOrder.getTanggalPengirimanSparepart();
        localDate = LocalDate.parse(date, dateFormat);
        newPurchaseOrder.setTanggalPengirimanSparepart(localDate);
        newPurchaseOrder.setPenanggungjawab(karyawanService.getKaryawanById(purchaseOrder.getPenanggungjawabId()));
        newPurchaseOrder.setDaftarUnit(new ArrayList<DataUnitPOModel>());
        newPurchaseOrder.setDaftarBeritaAcara(new ArrayList<BeritaAcaraPOModel>());
        newPurchaseOrder.setAlamatPemasangan(purchaseOrder.getAlamatPemasangan());
        newPurchaseOrder.setStatusPo(statusPOService.getById(1));
        newPurchaseOrder.setStatusSparepart(statusSparepartService.getById(1));

        newPurchaseOrder = purchaseOrderDb.save(newPurchaseOrder);

        for (FormTambahUnitPO dataUnitPODTO : purchaseOrder.getDaftarDataUnitPO()) {
            FormDataUnitPODTO formDataUnitPODTO = new FormDataUnitPODTO();
            formDataUnitPODTO.setJumlahDipesan(dataUnitPODTO.getJumlahDipesan());
            formDataUnitPODTO.setPurchaseOrderId(newPurchaseOrder.getId());
            formDataUnitPODTO.setUnitPoId(dataUnitPODTO.getUnitPOId());

            dataUnitPOService.save(formDataUnitPODTO);
        }

        String isi = "Purchase order dengan nomor " + newPurchaseOrder.getNomor() + " berhasil ditambahkan.";
        String src = "/purchase-order/profil/" + newPurchaseOrder.getId();
        String roles = "Admin,Bagian Pemasangan,Bagian Sparepart,Koordinator Maintenance,Manajemen";
        NotifikasiModel notif = notifikasiService.addNotifikasiForRoleInCabang("Penambahan Purchase Order", isi, src, roles, newPurchaseOrder.getCustomer().getCabang().getId());

        return newPurchaseOrder;
    }

    private Boolean conditionChecker(PurchaseOrderModel po, String source) {

        boolean con = true;

        if (source.contains("delete")) {

            String status = po.getStatusPo().getStatus();

            con &= status.equals("Dibuat");

        }

        if (source.contains("dibatalkan")) {
            String status = po.getStatusSparepart().getStatus();
            List<BeritaAcaraPOModel> daftarBA = po.getDaftarBeritaAcara();

            con &= !status.contains("Terpasang") && (daftarBA == null || daftarBA.isEmpty());
        }

        return con;
    }

}
