package puc.mis.service.purchaseOrder;

import puc.mis.dto.purchaseOrder.FormDataUnitPODTO;
import puc.mis.model.DataUnitPOModel;

import java.util.List;

public interface DataUnitPOService {
    DataUnitPOModel getById(Integer id);

    List<DataUnitPOModel> getAll(Integer id);

    void deleteById(Integer id);

    DataUnitPOModel save(FormDataUnitPODTO dataUnitPO);

    DataUnitPOModel update(FormDataUnitPODTO dataUnitPO);
}
