package puc.mis.service.purchaseOrder;

import puc.mis.dto.purchaseOrder.FormUnitPODTO;
import puc.mis.model.UnitPOModel;

import java.util.List;

public interface UnitPOService {
    UnitPOModel getById(Integer id);

    List<UnitPOModel> getAll();

    void deleteById(Integer id);

    UnitPOModel save(FormUnitPODTO unitPO);

    UnitPOModel update(FormUnitPODTO unitPO);
}
