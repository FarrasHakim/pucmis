package puc.mis.service.purchaseOrder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import puc.mis.dto.purchaseOrder.FormUnitPODTO;
import puc.mis.model.NotifikasiModel;
import puc.mis.model.UnitPOModel;
import puc.mis.repository.UnitPODb;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.notifikasi.NotifikasiService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class UnitPOServiceImpl implements UnitPOService {
    @Autowired
    private UnitPODb unitPODb;
    @Autowired
    private KaryawanService karyawanService;
    @Autowired
    private NotifikasiService notifikasiService;

    @Override
    public UnitPOModel getById(Integer id) {
        Optional<UnitPOModel> unitPO = unitPODb.findById(id);
        if (unitPO.isPresent()) {
            return unitPO.get();
        } else {
            throw new NoSuchElementException("UnitPO with id " + id + " doesn't exist");
        }
    }

    @Override
    public List<UnitPOModel> getAll() {
        return unitPODb.findAll();
    }

    @Override
    public void deleteById(Integer id) {
        Optional<UnitPOModel> unitPO = unitPODb.findById(id);
        if (unitPO.isPresent()) {
            Integer unitPOId = unitPO.get().getId();
            unitPODb.delete(unitPO.get());

            //notifikasi
            String isi = "UnitPO dengan id " + unitPOId + " telah dihapus oleh " + karyawanService.getUserLogin().getNama();
            NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penghapusan UnitPO", isi, "/", "Admin");
            //
        } else {
            throw new NoSuchElementException("UnitPO with id " + id + " doesn't exist");
        }
    }

    @Override
    public UnitPOModel save(FormUnitPODTO unitPO) {
        UnitPOModel newUnitPO = new UnitPOModel();
        newUnitPO.setNama(unitPO.getNama());
        newUnitPO.setDaftarUnit(unitPO.getDaftarUnit());

        String isi = "UnitPO dengan id " + newUnitPO.getId() + " berhasil ditambahkan oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Penambahan UnitPO", isi, "/", "Admin");

        return unitPODb.save(newUnitPO);
    }

    @Override
    public UnitPOModel update(FormUnitPODTO unitPO) {
        Optional<UnitPOModel> newUnitPO = unitPODb.findById(unitPO.getId());
        if (newUnitPO.isPresent()) {
            newUnitPO.get().setNama(unitPO.getNama());
            newUnitPO.get().setDaftarUnit(unitPO.getDaftarUnit());
        } else {
            throw new NoSuchElementException("UnitPO with id " + unitPO.getId() + " doesn't exist");
        }

        //notifikasi
        String isi = "UnitPO dengan id " + newUnitPO.get().getId() + " telah diubah oleh " + karyawanService.getUserLogin().getNama();
        NotifikasiModel notif = notifikasiService.addNotifikasiForRole("Perubahan UnitPO", isi, "/", "Admin");
        //

        return unitPODb.save(newUnitPO.get());
    }
}
