package puc.mis.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import puc.mis.dto.servis.FormJadwalServisDTO;
import puc.mis.dto.servis.FormKontrakServisDTO;
import puc.mis.dto.servis.UnitServisDTO;
import puc.mis.model.DataUnitKontrakServisModel;
import puc.mis.model.KontrakServisModel;
import puc.mis.model.Views;
import puc.mis.service.beritaAcara.BeritaAcaraServisService;
import puc.mis.service.customer.CustomerService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.servis.JadwalServisService;
import puc.mis.service.servis.KontrakServisService;
import puc.mis.service.status.StatusJadwalServisService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/kontrak-servis")
public class KontrakServisController {

    private final String location = "servis";
    @Autowired
    private KontrakServisService kontrakServisService;
    @Autowired
    private KaryawanService karyawanService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private JadwalServisService jadwalServisService;
    @Autowired
    private StatusJadwalServisService statusJadwalServisService;

    @Autowired
    private BeritaAcaraServisService beritaAcaraServisService;

    @JsonView(Views.KontrakServis.class)
    @RequestMapping(value = "", method = RequestMethod.GET)
    private String getAllKontrakServisPage(Model model) {
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            model.addAttribute("daftarKontrakServis", kontrakServisService.getAll());
            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian Servis"));
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("location", location);
            return "kontrakServis/get-all-kontrak-servis";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "kontrakServis/kontrak-servis-error";
        }
    }

    @JsonView(Views.KontrakServis.class)
    @RequestMapping(value = "/hapus", method = RequestMethod.POST)
    private String deleteKontrakServis(@RequestParam("id") String id, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            kontrakServisService.deleteById(Integer.parseInt(id));
            model.addAttribute("messageTitle", "Penghapusan Kontrak Servis Berhasil");
            model.addAttribute("messageText", "Kontrak Servis dengan id " + id + " berhasil dihapus");
            model.addAttribute("messageUrl", "/kontrak-servis/");
            model.addAttribute("messageUrlText", "Lihat Kontrak Servis");
            return "success";
        } catch (Exception e) {
            return "redirect:/";
        }
    }

    @JsonView(Views.KontrakServis.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST)
    private String add(@ModelAttribute FormKontrakServisDTO kontrakServis, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            System.out.println("AddKontrakServisControllerPost");
            kontrakServisService.save(kontrakServis);
            model.addAttribute("messageTitle", "Penambahan Kontrak Servis Berhasil");
            model.addAttribute("messageText", "Kontrak Servis dengan nomor " + kontrakServis.getNomorKontrak() + " berhasil ditambahkan");
            model.addAttribute("messageUrl", "/kontrak-servis/");
            model.addAttribute("messageUrlText", "Lihat Kontrak Servis");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "kontrakServis/kontrak-servis-error";
        }
    }

    @JsonView(Views.KontrakServis.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST, params = {"addUnit"})
    private String addPageAddUnit(@ModelAttribute FormKontrakServisDTO kontrakServis, Model model) {
        try {

            if (kontrakServis.getDaftarUnitServis() == null) {
                kontrakServis.setDaftarUnitServis(new ArrayList<UnitServisDTO>());
            }

            kontrakServis.getDaftarUnitServis().add(new UnitServisDTO());
            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian Servis"));
            model.addAttribute("kontrakServis", kontrakServis);
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            return "kontrakServis/add-kontrak-servis";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "kontrakServis/kontrak-servis-error";
        }
    }

    @JsonView(Views.KontrakServis.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST, params = {"removeUnit"})
    private String addPageRemoveUnit(@ModelAttribute FormKontrakServisDTO kontrakServis, HttpServletRequest req, Model model) {
        try {

            Integer rowId = Integer.valueOf(req.getParameter("removeUnit"));
            kontrakServis.getDaftarUnitServis().remove(rowId.intValue());

            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian Servis"));
            model.addAttribute("kontrakServis", kontrakServis);
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            return "kontrakServis/add-kontrak-servis";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "kontrakServis/kontrak-servis-error";
        }
    }

    @JsonView(Views.KontrakServis.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.GET)
    private String addPage(Model model) {
        try {
            FormKontrakServisDTO kontrakServis = new FormKontrakServisDTO();
            List<UnitServisDTO> daftarUnitServis = new ArrayList<>();
            UnitServisDTO unitServis = new UnitServisDTO();
            daftarUnitServis.add(unitServis);
            kontrakServis.setDaftarUnitServis(daftarUnitServis);

            model.addAttribute("kontrakServis", kontrakServis);
            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian Servis"));
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            return "kontrakServis/add-kontrak-servis";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "kontrakServis/kontrak-servis-error";
        }
    }

    @JsonView(Views.KontrakServis.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.GET)
    private String editPage(@RequestParam("id") Integer idKontrak, Model model) {
        try {
            KontrakServisModel realKontrakServis = kontrakServisService.getById(idKontrak);
            FormKontrakServisDTO kontrakServis = new FormKontrakServisDTO();
            List<UnitServisDTO> daftarUnitServis = new ArrayList<>();
            UnitServisDTO unitServis;

            for (DataUnitKontrakServisModel dataUnitKontrakServis: realKontrakServis.getDaftarUnit()) {
                unitServis = new UnitServisDTO();
                unitServis.setUnit(dataUnitKontrakServis.getUnitKontrakServis().getUnit());
                unitServis.setMerk(dataUnitKontrakServis.getMerkKontrakServis().getMerk());
                unitServis.setQuantity(dataUnitKontrakServis.getQuantity());
                daftarUnitServis.add(unitServis);
            }

            // Set data form kontrak dengan data asli
            kontrakServis.setId(realKontrakServis.getId());
            kontrakServis.setDaftarUnitServis(daftarUnitServis);
            kontrakServis.setAlamatServis(realKontrakServis.getAlamatServis());
            kontrakServis.setFrekuensiPerBulan(realKontrakServis.getFrekuensiPerBulan());
            kontrakServis.setNomorKontrak(realKontrakServis.getNomorKontrak());
            kontrakServis.setPeriodeAwal(realKontrakServis.getPeriodeAwal().toString());
            kontrakServis.setPeriodeAkhir(realKontrakServis.getPeriodeAkhir().toString());
            kontrakServis.setCustomerId(realKontrakServis.getCustomer().getId());
            kontrakServis.setPembuatKontrakId(realKontrakServis.getPembuatKontrak().getUuid());
            kontrakServis.setPenanggungJawabId(realKontrakServis.getPenanggungJawab().getUuid());
            kontrakServis.setStatusKontrakServisId(realKontrakServis.getStatus().getId());
            kontrakServis.setDaftarUnitServis(daftarUnitServis);

            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian Servis"));
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("kontrakServis", kontrakServis);
            return "kontrakServis/edit-kontrak-servis";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "kontrakServis/kontrak-servis-error";
        }
    }

    @JsonView(Views.KontrakServis.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST, params = {"addUnit"})
    private String editPageAddUnit(@ModelAttribute FormKontrakServisDTO kontrakServis, Model model) {
        try {

            if (kontrakServis.getDaftarUnitServis() == null) {
                kontrakServis.setDaftarUnitServis(new ArrayList<UnitServisDTO>());
            }

            kontrakServis.getDaftarUnitServis().add(new UnitServisDTO());
            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian Servis"));
            model.addAttribute("kontrakServis", kontrakServis);
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            return "kontrakServis/edit-kontrak-servis";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "kontrakServis/kontrak-servis-error";
        }
    }

    @JsonView(Views.KontrakServis.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST, params = {"removeUnit"})
    private String editPageRemoveUnit(@ModelAttribute FormKontrakServisDTO kontrakServis, HttpServletRequest req, Model model) {
        try {

            Integer rowId = Integer.valueOf(req.getParameter("removeUnit"));
            kontrakServis.getDaftarUnitServis().remove(rowId.intValue());

            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian Servis"));
            model.addAttribute("kontrakServis", kontrakServis);
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            return "kontrakServis/edit-kontrak-servis";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "kontrakServis/kontrak-servis-error";
        }
    }

    @JsonView(Views.KontrakServis.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST)
    private String editKontrakServis(@ModelAttribute FormKontrakServisDTO kontrakServis, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            kontrakServisService.update(kontrakServis);
            model.addAttribute("messageTitle", "Pengubahan Kontrak Servis Berhasil");
            model.addAttribute("messageText", "Kontrak Servis dengan id " + kontrakServis.getId() + " berhasil diubah");
            model.addAttribute("messageUrl", "/kontrak-servis/");
            model.addAttribute("messageUrlText", "Lihat Kontrak Servis");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "kontrakServis/kontrak-servis-error";
        }
    }

    @JsonView(Views.JadwalServis.class)
    @RequestMapping(value = "/update-jadwal", method = RequestMethod.GET)
    private String updateJadwalServisPage(@RequestParam("idJadwalServis") Integer idJadwal, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("jadwalServis", jadwalServisService.getById(idJadwal));
            model.addAttribute("daftarStatusJadwalServis", statusJadwalServisService.getAll());
            return "kontrakServis/edit-jadwal-servis";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "kontrakServis/kontrak-servis-error";
        }
    }

    @JsonView(Views.JadwalServis.class)
    @RequestMapping(value = "/update-jadwal", method = RequestMethod.POST)
    private String updateJadwalServis(@ModelAttribute FormJadwalServisDTO jadwalServis, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            jadwalServisService.update(jadwalServis);
            model.addAttribute("messageTitle", "Pengubahan Jadwal Servis Berhasil");
            model.addAttribute("messageText", "Jadwal Servis dengan id " + jadwalServis.getId() + " berhasil diubah");
            model.addAttribute("messageUrl", "/kontrak-servis/profil/" + jadwalServis.getKontrakServisId());
            model.addAttribute("messageUrlText", "Lihat Jadwal Servis");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "kontrakServis/kontrak-servis-error";
        }
    }

    @JsonView(Views.KontrakServis.class)
    @RequestMapping(value = "/profil/{idKontrak}", method = RequestMethod.GET)
    private String profilePage(@PathVariable Integer idKontrak, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        model.addAttribute("kontrakServis", kontrakServisService.getById(idKontrak));
        model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian Servis"));
        model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
        model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
        model.addAttribute("daftarJadwalServis", jadwalServisService.getAllByKontrakServis(idKontrak));
        model.addAttribute("daftarBeritaAcaraServis", beritaAcaraServisService.getAllByKontrakServis(idKontrak));
        return "kontrakServis/profile";
    }

    @JsonView(Views.JadwalServis.class)
    @RequestMapping(value = "/hapus-jadwal", method = RequestMethod.POST)
    private String deleteJadwalServis(@RequestParam("idJadwalServis") String id, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            jadwalServisService.deleteById(Integer.parseInt(id));
            model.addAttribute("messageTitle", "Penghapusan Jadwal Servis Berhasil");
            model.addAttribute("messageText", "Jadwal Servis dengan id " + id + "berhasil diubah");
            model.addAttribute("messageUrl", "/kontrak-servis");
            model.addAttribute("messageUrlText", "Lihat Kontrak Servis");
            return "success";
        } catch (Exception e) {
            return "redirect:/";
        }
    }
}
