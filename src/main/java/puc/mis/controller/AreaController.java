package puc.mis.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import puc.mis.dto.area.FormAreaDTO;
import puc.mis.model.AreaModel;
import puc.mis.model.Views;
import puc.mis.service.area.AreaService;
import puc.mis.service.cabang.CabangService;
import puc.mis.service.customer.CustomerService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.log.LogService;

import java.util.List;

@Controller
@RequestMapping("/area")
public class AreaController {

    @Autowired
    private AreaService areaService;

    @Autowired
    private CabangService cabangService;

    @Autowired
    private KaryawanService karyawanService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LogService logService;

    @JsonView(Views.Area.class)
    @RequestMapping(value = "", method = RequestMethod.GET)
    private String allArea(@RequestParam(required = false) String newArea,
                           @RequestParam(required = false) String succmsg, Model model) {
        List<AreaModel> daftarArea = areaService.getAll();
        model.addAttribute("succmsg", succmsg);
        model.addAttribute("newArea", newArea);
        model.addAttribute("daftarArea", daftarArea);
        model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
        return "area/areas";
    }

    @JsonView(Views.Area.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST)
    private ModelAndView addCabangSubmit(@ModelAttribute FormAreaDTO area, @RequestParam String source, ModelMap model) {
        try {
            areaService.save(area);
            model.addAttribute("newArea", area.getNama());
            return new ModelAndView("redirect:" + source, model);
        } catch (Exception e) {
            model.addAttribute("errmsg", e.toString());
            model.addAttribute("source", source);
            model.addAttribute("idCabang", area.getCabang());
            return new ModelAndView("redirect:/area/tambah", model);
        }
    }

    @JsonView(Views.Area.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.GET)
    private String addAreaPage(@RequestParam(required = false) Integer idCabang,
                               @RequestParam(required = false) String errmsg, @RequestParam String source, Model model) {
        model.addAttribute("errmsg", errmsg);
        model.addAttribute("idCabang", (idCabang != null) ? idCabang : 0);
        model.addAttribute("daftarKaryawan", (idCabang != null) ? karyawanService.getActiveKaryawansKA(idCabang) : null);
        model.addAttribute("cabang", (idCabang != null) ? cabangService.getById(idCabang) : null);
        model.addAttribute("daftarCabang", cabangService.getAll());
        model.addAttribute("source", source);
        return "area/add-area";
    }

    @JsonView(Views.Area.class)
    @RequestMapping(value = "/profil/{idArea}", method = RequestMethod.GET)
    private String profilePage(@PathVariable Integer idArea, @RequestParam(required = false) String newCustomer,
                               @RequestParam(required = false) String succmsg, @RequestParam(required = false) String errmsg, Model model) {
        model.addAttribute("succmsg", succmsg);
        model.addAttribute("errmsg", errmsg);
        model.addAttribute("newCustomer", newCustomer);
        model.addAttribute("area", areaService.getById(idArea));
        model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
        model.addAttribute("logs", logService.get(idArea.toString(), "Area"));
        return "area/profile";
    }

    @JsonView(Views.Area.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST)
    private ModelAndView editAreaSubmit(@ModelAttribute FormAreaDTO areaDTO, ModelMap model) {
        try {
            areaService.update(areaDTO);
            model.addAttribute("succmsg", "diubah");
            return new ModelAndView("redirect:/area/profil/" + areaDTO.getId(), model);
        } catch (Exception e) {
            model.addAttribute("errmsg", e.toString());
            model.addAttribute("id", areaDTO.getId());
            return new ModelAndView("redirect:/area/ubah", model);
        }
    }

    @JsonView(Views.Area.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.GET)
    public String editAreaPage(@RequestParam("id") Integer idArea, @RequestParam(required = false) String errmsg, Model model) {
        model.addAttribute("area", areaService.getById(idArea));
        model.addAttribute("daftarCabang", cabangService.getAll());
        model.addAttribute("errmsg", errmsg);
        return "area/edit-area";
    }

    @JsonView(Views.Area.class)
    @RequestMapping(value = "/pilih-kepala", method = RequestMethod.POST)
    private ModelAndView kepalaAreaSubmit(@ModelAttribute FormAreaDTO areaDTO, ModelMap model) {
        try {
            areaService.changeKepala(areaDTO);
            model.addAttribute("succmsg", "diganti kepalanya");
            return new ModelAndView("redirect:/area/profil/" + areaDTO.getId(), model);
        } catch (Exception e) {
            model.addAttribute("errmsg", e.toString());
            model.addAttribute("id", areaDTO.getId());
            return new ModelAndView("redirect:/area/pilih-kepala", model);
        }
    }

    @JsonView(Views.Area.class)
    @RequestMapping(value = "/pilih-kepala", method = RequestMethod.GET)
    public String kepalaAreaPage(@RequestParam("id") Integer idArea, Model model) {
        AreaModel area = areaService.getById(idArea);
        model.addAttribute("area", area);
        model.addAttribute("daftarKaryawan", karyawanService.getActiveKaryawansKA(area.getCabang().getId()));
        return "area/edit-kepala-area";
    }

    @JsonView(Views.Area.class)
    @RequestMapping(value = "/masukkan-customer", method = RequestMethod.POST)
    private ModelAndView masukCustomerSubmit(@RequestParam Integer idArea, @RequestParam Integer idCustomer, ModelMap model) {
        try {
            customerService.masukArea(idCustomer, idArea);
            model.addAttribute("succmsg", "memasukkan customer");
            return new ModelAndView("redirect:/area/profil/" + idArea, model);
        } catch (Exception e) {
            model.addAttribute("errmsg", e.toString());
            model.addAttribute("id", idArea);
            return new ModelAndView("redirect:/area/masukkan-customer", model);
        }
    }

    @JsonView(Views.Area.class)
    @RequestMapping(value = "/masukkan-customer", method = RequestMethod.GET)
    public String masukCustomerPage(@RequestParam("id") Integer idArea, @RequestParam(required = false) String errmsg, Model model) {
        AreaModel area = areaService.getById(idArea);
        model.addAttribute("errmsg", errmsg);
        model.addAttribute("area", area);
        model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(area.getCabang().getId()));
        return "area/masukkan-customer";
    }

    @JsonView(Views.Area.class)
    @RequestMapping(value = "/hapus", method = RequestMethod.POST)
    private ModelAndView deleteAreaSubmit(@RequestParam("id") Integer id, ModelMap model) {
        try {
            String namaArea = areaService.getById(id).getNama();
            areaService.deleteById(id);
            model.addAttribute("succmsg", "Area " + namaArea + " berhasil dihapus !");
            return new ModelAndView("redirect:/area", model);
        } catch (Exception e) {
            model.addAttribute("errmsg", e.toString());
            return new ModelAndView("redirect:/area/profil/" + id, model);
        }
    }

}
