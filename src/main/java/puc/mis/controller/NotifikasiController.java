package puc.mis.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import puc.mis.model.BacaNotifikasiModel;
import puc.mis.model.Views;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.notifikasi.BacaNotifikasiService;
import puc.mis.service.notifikasi.NotifikasiService;

import java.util.List;

@Controller
@RequestMapping("/notifikasi")
public class NotifikasiController {

    @Autowired
    BacaNotifikasiService bacaNotifikasiService;
    @Autowired
    KaryawanService karyawanService;
    @Autowired
    private NotifikasiService notifikasiService;

    @JsonView(Views.BacaNotifikasi.class)
    @RequestMapping(value = "", method = RequestMethod.GET)
    private String lihatDaftarNotifikasi(Model model) {
        List<BacaNotifikasiModel> daftarNotifikasi = bacaNotifikasiService.getDaftarBacaNotif(karyawanService.getUserLogin());
        model.addAttribute("daftarNotifikasi", daftarNotifikasi);
        return "notifikasi/notifications";
    }
}