package puc.mis.controller;


import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import puc.mis.dto.purchaseOrder.FormUnitPODTO;
import puc.mis.model.UnitPOModel;
import puc.mis.model.Views;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.purchaseOrder.UnitPOService;

import java.util.List;

@Controller
@RequestMapping("/unit-po")
public class UnitPOController {

    @Autowired
    private UnitPOService unitPOService;
    @Autowired
    private KaryawanService karyawanService;

    @JsonView(Views.UnitPO.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST)
    private String add(@ModelAttribute FormUnitPODTO unitPODTO, Model model) {
        try {
            unitPOService.save(unitPODTO);
            model.addAttribute("messageTitle", "Penambahan Unit PO Berhasil");
            model.addAttribute("messageText", "Unit PO dengan nama " + unitPODTO.getNama() + " berhasil ditambahkan");
            model.addAttribute("messageUrl", "/unit-po");
            model.addAttribute("messageUrlText", "Daftar Unit PO");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "unitPO/unit-po-error";
        }
    }

    @JsonView(Views.UnitPO.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.GET)
    private String addPage(Model model) {
        try {
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            return "unitPO/add-unit-po";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "unitPO/unit-po-error";
        }
    }

    @JsonView(Views.UnitPO.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST)
    private String edit(@ModelAttribute FormUnitPODTO unitPODTO, Model model) {
        try {
            unitPOService.update(unitPODTO);
            model.addAttribute("messageTitle", "Pengubahan Unit PO Berhasil");
            model.addAttribute("messageText", "Unit PO dengan nama " + unitPODTO.getNama() + " berhasil diubah");
            model.addAttribute("messageUrl", "/unit-po");
            model.addAttribute("messageUrlText", "Daftar Unit PO");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "unitPO/unit-po-error";
        }
    }

    @JsonView(Views.UnitPO.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.GET)
    private String editPage(@RequestParam("id") Integer id, Model model) {
        try {
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("unitPO", unitPOService.getById(id));
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            return "unitPO/edit-unit-po";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "unitPO/unit-po-error";
        }
    }

    @JsonView(Views.UnitPO.class)
    @RequestMapping(value = "", method = RequestMethod.GET)
    private String getAll(Model model) {
        try {
            List<UnitPOModel> daftarUnitPO = unitPOService.getAll();
            model.addAttribute("daftarUnitPO", daftarUnitPO);
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            return "unitPO/get-all-unit-po";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "unitPO/unit-po-error";
        }
    }

    @JsonView(Views.UnitPO.class)
    @RequestMapping(value = "/hapus", method = RequestMethod.POST)
    private String delete(@RequestParam("id") String id, Model model) {
        try {
            unitPOService.deleteById(Integer.parseInt(id));
            model.addAttribute("messageTitle", "Penghapusan Unit PO Berhasil");
            model.addAttribute("messageText", "Unit PO dengan id " + id + " berhasil dihapus");
            model.addAttribute("messageUrl", "/unit-po");
            model.addAttribute("messageUrlText", "Daftar Unit PO");
            return "success";
        } catch (Exception e) {
            return "redirect:/";
        }
    }

    @JsonView(Views.UnitPO.class)
    @RequestMapping(value = "/profil/{id}", method = RequestMethod.GET)
    private String profilePage(@PathVariable Integer id, Model model) {
        model.addAttribute("unitPO", unitPOService.getById(id));
        model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
        return "unitPO/profile";
    }
}
