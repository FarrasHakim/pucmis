package puc.mis.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import puc.mis.dto.beritaAcara.FormBeritaAcaraServisDTO;
import puc.mis.dto.invoice.FormInvoiceDTO;
import puc.mis.model.BeritaAcaraServisModel;
import puc.mis.model.Views;
import puc.mis.service.beritaAcara.BeritaAcaraServisService;
import puc.mis.service.invoice.InvoiceService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.status.StatusBeritaAcaraService;
import puc.mis.service.status.StatusInvoiceService;

import java.util.List;

@Controller
@RequestMapping("/berita-acara-servis")
public class BeritaAcaraServisController {
    @Autowired
    private BeritaAcaraServisService beritaAcaraServisService;
    @Autowired
    private KaryawanService karyawanService;
    @Autowired
    private StatusBeritaAcaraService statusBeritaAcaraService;
    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private StatusInvoiceService statusInvoiceService;

    @JsonView(Views.BeritaAcaraServis.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST)
    private String add(@ModelAttribute FormBeritaAcaraServisDTO beritaAcaraServis, Model model) {
        try {
            beritaAcaraServisService.save(beritaAcaraServis);
            model.addAttribute("messageTitle", "Penambahan BA Servis Berhasil");
            model.addAttribute("messageText", "BA Servis dengan nomor spk " + beritaAcaraServis.getNomorSpk() + " berhasil ditambahkan");
            model.addAttribute("messageUrl", "/berita-acara-servis");
            model.addAttribute("messageUrlText", "List BA Servis");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraServis/berita-acara-servis-error";
        }
    }

    @JsonView(Views.BeritaAcaraServis.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.GET)
    private String addPage(Model model) {
        try {
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            model.addAttribute("daftarStatusBAServis", statusBeritaAcaraService.getAll());
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            return "beritaAcaraServis/add-berita-acara-servis";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraServis/berita-acara-servis-error";
        }
    }

    @JsonView(Views.BeritaAcaraServis.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST)
    private String editPurchaseOrder(@ModelAttribute FormBeritaAcaraServisDTO beritaAcaraServis, Model model) {
        try {
            beritaAcaraServisService.update(beritaAcaraServis);
            model.addAttribute("messageTitle", "Pengubahan BA Servis Berhasil");
            model.addAttribute("messageText", "BA Servis dengan nomor spk " + beritaAcaraServis.getNomorSpk() + " berhasil diubah");
            model.addAttribute("messageUrl", "/berita-acara-servis");
            model.addAttribute("messageUrlText", "List BA Servis");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraServis/berita-acara-servis-error";
        }
    }

    @JsonView(Views.BeritaAcaraServis.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.GET)
    private String editPurchaseOrderPage(@RequestParam("id") Integer id, Model model) {
        try {
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("beritaAcaraServis", beritaAcaraServisService.getById(id));
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            model.addAttribute("daftarStatusBAServis", statusBeritaAcaraService.getAll());
            return "beritaAcaraServis/edit-berita-acara-servis";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraServis/berita-acara-servis-error";
        }
    }

    @JsonView(Views.BeritaAcaraServis.class)
    @RequestMapping(value = "", method = RequestMethod.GET)
    private String getAllPurchaseOrder(Model model) {
        try {
            List<BeritaAcaraServisModel> daftarBeritaAcaraServis = beritaAcaraServisService.getAll();
            model.addAttribute("daftarBeritaAcaraServis", daftarBeritaAcaraServis);
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            return "beritaAcaraServis/get-all-berita-acara-servis";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraServis/berita-acara-servis-error";
        }
    }

    @JsonView(Views.BeritaAcaraServis.class)
    @RequestMapping(value = "/hapus", method = RequestMethod.POST)
    private String delete(@RequestParam("id") String id, Model model) {
        try {
            beritaAcaraServisService.deleteById(Integer.parseInt(id));
            model.addAttribute("messageTitle", "Penghapusan BA Servis Berhasil");
            model.addAttribute("messageText", "BA Servis dengan id " + id + " berhasil dihapus");
            model.addAttribute("messageUrl", "/berita-acara-servis");
            model.addAttribute("messageUrlText", "List BA Servis");
            return "success";
        } catch (Exception e) {
            return "redirect:/";
        }
    }

    @JsonView(Views.BeritaAcaraServis.class)
    @RequestMapping(value = "/profil/{id}", method = RequestMethod.GET)
    private String profilePage(@PathVariable Integer id, Model model) {
        model.addAttribute("beritaAcara", beritaAcaraServisService.getById(id));
        model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
        return "beritaAcaraServis/profile";
    }

    @JsonView(Views.BeritaAcaraServis.class)
    @RequestMapping(value = "/isi", method = RequestMethod.POST)
    private String fill(@ModelAttribute FormBeritaAcaraServisDTO beritaAcaraServis, Model model) {
        try {
            beritaAcaraServisService.fill(beritaAcaraServis);
            model.addAttribute("messageTitle", "Pengisian BA Servis Berhasil");
            model.addAttribute("messageText", "BA Servis dengan nomor spk " + beritaAcaraServis.getNomorSpk() + " berhasil diisi datanya");
            model.addAttribute("messageUrl", "/berita-acara-servis");
            model.addAttribute("messageUrlText", "List BA Servis");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraServis/berita-acara-servis-error";
        }
    }

    @JsonView(Views.BeritaAcaraServis.class)
    @RequestMapping(value = "/isi", method = RequestMethod.GET)
    private String fillPage(@RequestParam("id") Integer id, Model model) {
        try {
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("beritaAcaraServis", beritaAcaraServisService.getById(id));
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            model.addAttribute("daftarStatusBAServis", statusBeritaAcaraService.getAll());
            return "beritaAcaraServis/isi-berita-acara-servis";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraServis/berita-acara-servis-error";
        }
    }

    @JsonView(Views.Invoice.class)
    @RequestMapping(value = "/invoice/tambah", method = RequestMethod.POST)
    private String addInvoicePerBA(@ModelAttribute FormInvoiceDTO invoiceDTO, Model model) {
        try {
            invoiceService.saveInvoiceServis(invoiceDTO);
            model.addAttribute("messageTitle", "Penambahan Invoice Berhasil");
            model.addAttribute("messageText", "Invoice dengan nomor  " + invoiceDTO.getNomor() + " berhasil diisi datanya");
            model.addAttribute("messageUrl", "/berita-acara-servis");
            model.addAttribute("messageUrlText", "List BA Servis");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraServis/berita-acara-servis-error";
        }
    }

    @JsonView(Views.Invoice.class)
    @RequestMapping(value = "/invoice/tambah", method = RequestMethod.GET)
    private String addInvoicePerBAPage(@RequestParam("id") Integer id, Model model) {
        try {
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("beritaAcaraServis", beritaAcaraServisService.getById(id));
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            model.addAttribute("daftarStatusBAServis", statusBeritaAcaraService.getAll());
            model.addAttribute("daftarStatusInvoice", statusInvoiceService.getAll());
            return "beritaAcaraServis/add-invoice-berita-acara-servis";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraServis/berita-acara-servis-error";
        }
    }

}
