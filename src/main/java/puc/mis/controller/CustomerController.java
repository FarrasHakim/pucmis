package puc.mis.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import puc.mis.dto.customer.FormCustomerDTO;
import puc.mis.dto.customer.FormCustomerDTOWrapper;
import puc.mis.model.CustomerModel;
import puc.mis.model.Views;
import puc.mis.service.area.AreaService;
import puc.mis.service.cabang.CabangService;
import puc.mis.service.customer.CustomerService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.log.LogService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CabangService cabangService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private KaryawanService karyawanService;

    @Autowired
    private LogService logService;

    @JsonView(Views.Customer.class)
    @RequestMapping(value = "", method = RequestMethod.GET)
    private String allCustomer(@RequestParam(required = false) String newCustomer,
                               @RequestParam(required = false) String succmsg, Model model) {
        List<CustomerModel> daftarCustomer = customerService.getAll();
        model.addAttribute("succmsg", succmsg);
        model.addAttribute("newCustomer", newCustomer);
        model.addAttribute("daftarCustomer", daftarCustomer);
        model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
        return "customer/customers";
    }

    @JsonView(Views.Customer.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST)
    private ModelAndView addCustomerSubmit(@ModelAttribute FormCustomerDTOWrapper customers,
                                           @RequestParam(required = false) Integer idCabang,
                                           @RequestParam(required = false) Integer idArea,
                                           @RequestParam String source, ModelMap model) {
        try {

            for (FormCustomerDTO customer : customers.getCustomerList()) {
                if (customers.getIdCabang() != null) {
                    customer.setCabangId(customers.getIdCabang());
                }
                if (customers.getIdArea() != null) {
                    customer.setAreaId(customers.getIdArea());
                }
                customerService.save(customer);
            }
            model.addAttribute("newCustomer", "ada");
            return new ModelAndView("redirect:" + source, model);
        } catch (IllegalArgumentException e) {
            model.addAttribute("customers", customers);
            model.addAttribute("errmsg", e.toString());
            model.addAttribute("source", source);
            model.addAttribute("area", (idArea != null) ? areaService.getById(idArea) : null);
            model.addAttribute("idCabang", idCabang);
            model.addAttribute("idArea", idArea);
            model.addAttribute("cabang", (idCabang != null) ? cabangService.getById(idCabang) : null);
            model.addAttribute("customers", customers);
            return new ModelAndView("redirect:/customer/tambah", model);
        }
    }

    @JsonView(Views.Customer.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST, params = {"addForm"})
    private String addCustomerForm(@ModelAttribute FormCustomerDTOWrapper customers,
                                   @RequestParam(required = false) Integer idCabang,
                                   @RequestParam(required = false) Integer idArea,
                                   @RequestParam String source, Model model) {

        if (idArea != null) {
            idCabang = areaService.getById(idArea).getCabang().getId();
        }

        if (customers.getCustomerList() == null) {
            customers.setCustomerList(new ArrayList<FormCustomerDTO>());
        }

        customers.getCustomerList().add(new FormCustomerDTO());
        model.addAttribute("source", source);
        model.addAttribute("customers", customers);
        model.addAttribute("cabang", (idCabang != null) ? cabangService.getById(idCabang) : null);
        model.addAttribute("idCabang", (idCabang != null) ? idCabang : "");
        model.addAttribute("area", (idArea != null) ? areaService.getById(idArea) : null);
        model.addAttribute("listCabang", cabangService.getAll());
        model.addAttribute("idArea", (idArea != null) ? idArea : "");
        model.addAttribute("listArea", (idCabang != null) ? areaService.getDaftarArea(idCabang) : new ArrayList<>());
        return "customer/add-customer";
    }

    @JsonView(Views.Customer.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST, params = {"removeForm"})
    private String removeCustomerForm(@ModelAttribute FormCustomerDTOWrapper customers,
                                      @RequestParam(required = false) Integer idCabang,
                                      @RequestParam(required = false) Integer idArea,
                                      @RequestParam String source,
                                      HttpServletRequest req, Model model) {

        if (idArea != null) {
            idCabang = areaService.getById(idArea).getCabang().getId();
        }

        Integer rowId = Integer.valueOf(req.getParameter("removeForm"));
        customers.getCustomerList().remove(rowId.intValue());

        model.addAttribute("source", source);
        model.addAttribute("customers", customers);
        model.addAttribute("listArea", (idCabang != null) ? areaService.getDaftarArea(idCabang) : new ArrayList<>());
        model.addAttribute("idArea", (idArea != null) ? idArea : "");
        model.addAttribute("listCabang", cabangService.getAll());
        model.addAttribute("idCabang", (idCabang != null) ? idCabang : "");
        model.addAttribute("area", (idArea != null) ? areaService.getById(idArea) : null);
        model.addAttribute("cabang", (idCabang != null) ? cabangService.getById(idCabang) : null);
        return "customer/add-customer";
    }

    @JsonView(Views.Customer.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.GET)
    public String addCustomerPage(@RequestParam(required = false) Integer idCabang,
                                  @RequestParam(required = false) Integer idArea,
                                  @RequestParam(required = false) String errmsg,
                                  @RequestParam String source, Model model) {
        if (idArea != null) {
            idCabang = areaService.getById(idArea).getCabang().getId();
        }

        FormCustomerDTO customer = new FormCustomerDTO();
        FormCustomerDTOWrapper customers = new FormCustomerDTOWrapper();
        List<FormCustomerDTO> listCustomer = new ArrayList<>();
        listCustomer.add(customer);
        customers.setCustomerList(listCustomer);

        model.addAttribute("errmsg", errmsg);
        model.addAttribute("source", source);
        model.addAttribute("customers", customers);
        model.addAttribute("listCabang", cabangService.getAll());
        model.addAttribute("cabang", (idCabang != null) ? cabangService.getById(idCabang) : null);
        model.addAttribute("listArea", (idCabang != null) ? areaService.getDaftarArea(idCabang) : new ArrayList<>());
        model.addAttribute("area", (idArea != null) ? areaService.getById(idArea) : null);
        model.addAttribute("idCabang", (idCabang != null) ? idCabang : "");
        model.addAttribute("idArea", (idArea != null) ? idArea : "");
        return "customer/add-customer";
    }

    @JsonView(Views.Customer.class)
    @RequestMapping(value = "/profil/{id}", method = RequestMethod.GET)
    public String profilePage(@PathVariable Integer id, @RequestParam(required = false) String succmsg,
                              @RequestParam(required = false) String errmsg, Model model) {
        try {
            model.addAttribute("succmsg", succmsg);
            model.addAttribute("errmsg", errmsg);
            model.addAttribute("customer", customerService.getById(id));
            model.addAttribute("daftarInvoice", customerService.getInvoices(id));
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("logs", logService.get(id.toString(), "Customer"));
            return "customer/profile";
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Customer tidak ditemukan");
        }
    }

    @JsonView(Views.Customer.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST)
    private ModelAndView editCustomerSubmit(@ModelAttribute FormCustomerDTO customer, ModelMap model) {
        try {
            customerService.update(customer);
            model.addAttribute("succmsg", "diubah");
            return new ModelAndView("redirect:/customer/profil/" + customer.getId(), model);
        } catch (IllegalArgumentException e) {
            model.addAttribute("errmsg", e.toString());
            model.addAttribute("id", customer.getId());
            return new ModelAndView("redirect:/customer/ubah", model);
        }
    }

    @JsonView(Views.Customer.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.GET)
    public String editCustomerPage(@RequestParam("id") Integer id, @RequestParam(required = false) String errmsg, Model model) {
        CustomerModel customer = customerService.getById(id);
        model.addAttribute("errmsg", errmsg);
        model.addAttribute("customer", customer);
        model.addAttribute("listCabang", cabangService.getAll());
        model.addAttribute("listArea", areaService.getDaftarArea(customer.getCabang().getId()));
        return "customer/edit-customer";
    }

    @JsonView(Views.Customer.class)
    @RequestMapping(value = "/pindahkan-customer", method = RequestMethod.POST)
    private ModelAndView pindahCustomerSubmit(@RequestParam Integer idCustomer, @RequestParam Integer idArea,
                                              @RequestParam Integer idCabang, ModelMap model) {
        try {
            if (idCabang != null && idCabang != 0) {
                customerService.masukCabang(idCustomer, idCabang);
            } else {
                customerService.masukArea(idCustomer, idArea);
            }
            model.addAttribute("succmsg", "dipindahkan");
            return new ModelAndView("redirect:/customer/profil/" + idCustomer, model);
        } catch (Exception e) {
            model.addAttribute("errmsg", e.toString());
            model.addAttribute("id", idCustomer);
            return new ModelAndView("redirect:/customer/pindahkan-customer", model);
        }
    }

    @JsonView(Views.Customer.class)
    @RequestMapping(value = "/pindahkan-customer", method = RequestMethod.GET)
    public String pindahCustomerPage(@RequestParam("id") Integer idCustomer, @RequestParam(required = false) String errmsg, Model model) {
        CustomerModel customer = customerService.getById(idCustomer);
        model.addAttribute("errmsg", errmsg);
        model.addAttribute("customer", customer);
        model.addAttribute("daftarCabang", cabangService.getAll());
        return "customer/pindahkan-customer";
    }

    @JsonView(Views.Customer.class)
    @RequestMapping(value = "/hapus", method = RequestMethod.POST)
    private ModelAndView deleteCustomerSubmit(@RequestParam("id") Integer id, ModelMap model) {
        try {
            String namaCustomer = customerService.getById(id).getNama();
            customerService.deleteById(id);
            model.addAttribute("succmsg", "Customer " + namaCustomer + " berhasil dihapus !");
            return new ModelAndView("redirect:/customer", model);
        } catch (Exception e) {
            model.addAttribute("errmsg", e.toString());
            return new ModelAndView("redirect:/area/profil/" + id, model);
        }
    }

}
