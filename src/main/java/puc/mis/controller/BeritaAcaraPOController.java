package puc.mis.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import puc.mis.dto.beritaAcara.FormBeritaAcaraPODTO;
import puc.mis.dto.invoice.FormInvoiceDTO;
import puc.mis.model.BeritaAcaraPOModel;
import puc.mis.model.Views;
import puc.mis.service.beritaAcara.BeritaAcaraPOService;
import puc.mis.service.invoice.InvoiceService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.status.StatusBeritaAcaraService;
import puc.mis.service.status.StatusInvoiceService;

import java.util.List;

@Controller
@RequestMapping("/berita-acara-po")
public class BeritaAcaraPOController {
    @Autowired
    private BeritaAcaraPOService beritaAcaraPOService;
    @Autowired
    private KaryawanService karyawanService;
    @Autowired
    private StatusBeritaAcaraService statusBeritaAcaraService;
    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private StatusInvoiceService statusInvoiceService;

    @JsonView(Views.BeritaAcaraPO.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST)
    private String add(@ModelAttribute FormBeritaAcaraPODTO beritaAcaraPODTO, Model model) {
        try {
            beritaAcaraPOService.save(beritaAcaraPODTO);
            model.addAttribute("messageTitle", "Penambahan BA PO Berhasil");
            model.addAttribute("messageText", "BA PO dengan nomor BA " + beritaAcaraPODTO.getNomorBa() + " berhasil ditambahkan");
            model.addAttribute("messageUrl", "/berita-acara-po");
            model.addAttribute("messageUrlText", "List BA PO");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraPo/berita-acara-po-error";
        }
    }

    @JsonView(Views.BeritaAcaraPO.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.GET)
    private String addPage(Model model) {
        try {
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            model.addAttribute("daftarStatusBA", statusBeritaAcaraService.getAll());
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            return "beritaAcaraPo/add-berita-po";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraPo/berita-acara-po-error";
        }
    }

    @JsonView(Views.BeritaAcaraPO.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST)
    private String edit(@ModelAttribute FormBeritaAcaraPODTO beritaAcaraPODTO, Model model) {
        try {
            beritaAcaraPOService.update(beritaAcaraPODTO);
            model.addAttribute("messageTitle", "Pengubahan BA PO Berhasil");
            model.addAttribute("messageText", "BA PO dengan nomor BA " + beritaAcaraPODTO.getNomorBa() + " berhasil diubah");
            model.addAttribute("messageUrl", "/berita-acara-po");
            model.addAttribute("messageUrlText", "List BA PO");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraPo/berita-acara-po-error";
        }
    }

    @JsonView(Views.BeritaAcaraPO.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.GET)
    private String editPage(@RequestParam("id") Integer id, Model model) {
        try {
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("beritaAcara", beritaAcaraPOService.getById(id));
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            model.addAttribute("daftarStatusBA", statusBeritaAcaraService.getAll());
            return "beritaAcaraPo/edit-berita-acara-po";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraPo/berita-acara-po-error";
        }
    }

    @JsonView(Views.BeritaAcaraPO.class)
    @RequestMapping(value = "", method = RequestMethod.GET)
    private String getAll(Model model) {
        try {
            List<BeritaAcaraPOModel> daftarBeritaAcaraPo = beritaAcaraPOService.getAll();
            model.addAttribute("daftarBeritaAcaraPo", daftarBeritaAcaraPo);
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            return "beritaAcaraPo/get-all-berita-acara-po";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraPo/berita-acara-po-error";
        }
    }

    @JsonView(Views.BeritaAcaraPO.class)
    @RequestMapping(value = "/hapus", method = RequestMethod.POST)
    private String delete(@RequestParam("id") String id, Model model) {
        try {
            beritaAcaraPOService.deleteById(Integer.parseInt(id));
            model.addAttribute("messageTitle", "Penghapusan BA PO Berhasil");
            model.addAttribute("messageText", "BA PO dengan id " + id + " berhasil dihapus");
            model.addAttribute("messageUrl", "/berita-acara-po");
            model.addAttribute("messageUrlText", "List BA PO");
            return "success";
        } catch (Exception e) {
            return "redirect:/";
        }
    }

    @JsonView(Views.BeritaAcaraPO.class)
    @RequestMapping(value = "/profil/{id}", method = RequestMethod.GET)
    private String profilePage(@PathVariable Integer id, Model model) {
        model.addAttribute("beritaAcara", beritaAcaraPOService.getById(id));
        model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
        return "beritaAcaraPo/profile";
    }

    @JsonView(Views.Invoice.class)
    @RequestMapping(value = "/invoice/tambah", method = RequestMethod.POST)
    private String addInvoicePerBA(@ModelAttribute FormInvoiceDTO invoiceDTO, Model model) {
        try {
            invoiceService.saveInvoicePo(invoiceDTO);
            model.addAttribute("messageTitle", "Penambahan Invoice Berhasil");
            model.addAttribute("messageText", "Invoice dengan nomor  " + invoiceDTO.getNomor() + " berhasil diisi datanya");
            model.addAttribute("messageUrl", "/berita-acara-po");
            model.addAttribute("messageUrlText", "List BA PO");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraPo/berita-acara-po-error";
        }
    }

    @JsonView(Views.Invoice.class)
    @RequestMapping(value = "/invoice/tambah", method = RequestMethod.GET)
    private String addInvoicePerBAPage(@RequestParam("id") Integer id, Model model) {
        try {
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("beritaAcara", beritaAcaraPOService.getById(id));
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            model.addAttribute("daftarStatusBA", statusBeritaAcaraService.getAll());
            model.addAttribute("daftarStatusInvoice", statusInvoiceService.getAll());
            return "beritaAcaraPo/add-invoice-berita-acara-po";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraPo/berita-acara-po-error";
        }
    }

    @JsonView(Views.BeritaAcaraPO.class)
    @RequestMapping(value = "/isi", method = RequestMethod.POST)
    private String fill(@ModelAttribute FormBeritaAcaraPODTO beritaAcaraPODTO, Model model) {
        try {
            beritaAcaraPOService.fill(beritaAcaraPODTO);
            model.addAttribute("messageTitle", "Pengisian BA PO Berhasil");
            model.addAttribute("messageText", "Data BA PO dengan nomor BA " + beritaAcaraPODTO.getNomorBa() + " berhasil diisi");
            model.addAttribute("messageUrl", "/berita-acara-po");
            model.addAttribute("messageUrlText", "List BA PO");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraPo/berita-acara-po-error";
        }
    }

    @JsonView(Views.BeritaAcaraPO.class)
    @RequestMapping(value = "/isi", method = RequestMethod.GET)
    private String fillPage(@RequestParam("id") Integer id, Model model) {
        try {
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("beritaAcara", beritaAcaraPOService.getById(id));
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            model.addAttribute("daftarStatusBA", statusBeritaAcaraService.getAll());
            return "beritaAcaraPo/isi-berita-acara-po";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "beritaAcaraPo/berita-acara-po-error";
        }
    }
}
