package puc.mis.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import puc.mis.dto.cabang.FormCabangDTO;
import puc.mis.model.CabangModel;
import puc.mis.model.Views;
import puc.mis.service.area.AreaService;
import puc.mis.service.cabang.CabangService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.log.LogService;

import java.util.List;

@Controller
@RequestMapping("/cabang")
public class CabangController {

    @Autowired
    private CabangService cabangService;

    @Autowired
    private KaryawanService karyawanService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private LogService logService;

    @JsonView(Views.Cabang.class)
    @RequestMapping(value = "", method = RequestMethod.GET)
    private String allCabang(@RequestParam(required = false) String newCabang,
                             @RequestParam(required = false) String succmsg, Model model) {
        List<CabangModel> daftarCabang = cabangService.getDaftarCabang();
        model.addAttribute("succmsg", succmsg);
        model.addAttribute("newCabang", newCabang);
        model.addAttribute("daftarCabang", daftarCabang);
        model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
        return "cabang/cabangs";
    }

    @JsonView(Views.Cabang.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST)
    private ModelAndView addCabangSubmit(@ModelAttribute FormCabangDTO cabang, ModelMap model) {
        try {
            cabangService.addCabang(cabang);
            model.addAttribute("newCabang", cabang.getNama());
            return new ModelAndView("redirect:/cabang", model);
        } catch (Exception e) {
            model.addAttribute("errmsg", e.toString());
            return new ModelAndView("redirect:/cabang/tambah", model);
        }
    }

    @JsonView(Views.Cabang.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.GET)
    private String addCabangPage(@RequestParam(required = false) String errmsg, Model model) {
        System.out.println(errmsg);
        model.addAttribute("errmsg", errmsg);
        model.addAttribute("daftarKaryawan", karyawanService.getActiveKaryawansKoor());
        return "cabang/add-cabang";
    }

    @JsonView(Views.Cabang.class)
    @RequestMapping(value = "/profil/{idCabang}", method = RequestMethod.GET)
    private String profilePage(@PathVariable Integer idCabang, @RequestParam(required = false) String newArea,
                               @RequestParam(required = false) String newKaryawan, @RequestParam(required = false) String newCustomer,
                               @RequestParam(required = false) String succmsg, @RequestParam(required = false) String errmsg, Model model) {
        model.addAttribute("succmsg", succmsg);
        model.addAttribute("errmsg", errmsg);
        model.addAttribute("newArea", newArea);
        model.addAttribute("newKaryawan", newKaryawan);
        model.addAttribute("newCustomer", newCustomer);
        model.addAttribute("cabang", cabangService.getById(idCabang));
        model.addAttribute("daftarKaryawan", karyawanService.getActiveKaryawans(idCabang));
        model.addAttribute("daftarPo", cabangService.getDaftarPo(idCabang));
        model.addAttribute("daftarKontrakServis", cabangService.getDaftarKontrak(idCabang));
        model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
        model.addAttribute("logs", logService.get(idCabang.toString(), "Cabang"));
        return "cabang/profile";
    }

    @JsonView(Views.Cabang.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST)
    private ModelAndView editCabangSubmit(@ModelAttribute FormCabangDTO cabang, ModelMap model) {
        try {
            cabangService.changeCabang(cabang);
            model.addAttribute("succmsg", "diubah");
            return new ModelAndView("redirect:/cabang/profil/" + cabang.getId(), model);
        } catch (Exception e) {
            model.addAttribute("errmsg", e.toString());
            model.addAttribute("id", cabang.getId());
            return new ModelAndView("redirect:/cabang/ubah", model);
        }
    }

    @JsonView(Views.Cabang.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.GET)
    public String editCabangPage(@RequestParam("id") Integer idCabang, @RequestParam(required = false) String errmsg, Model model) {
        model.addAttribute("errmsg", errmsg);
        model.addAttribute("cabang", cabangService.getById(idCabang));
        return "cabang/edit-cabang";
    }

    @JsonView(Views.Cabang.class)
    @RequestMapping(value = "/pilih-koordinator", method = RequestMethod.POST)
    private ModelAndView koordinatorCabangSubmit(@ModelAttribute FormCabangDTO cabang, ModelMap model) {
        try {
            cabangService.changeKoordinator(cabang);
            model.addAttribute("succmsg", "diganti koordinatornya");
            return new ModelAndView("redirect:/cabang/profil/" + cabang.getId(), model);
        } catch (Exception e) {
            model.addAttribute("errmsg", e.toString());
            model.addAttribute("id", cabang.getId());
            return new ModelAndView("redirect:/cabang/pilih-koordinator", model);
        }
    }

    @JsonView(Views.Cabang.class)
    @RequestMapping(value = "/pilih-koordinator", method = RequestMethod.GET)
    public String koordinatorCabangPage(@RequestParam("id") Integer idCabang, Model model) {
        model.addAttribute("cabang", cabangService.getById(idCabang));
        model.addAttribute("daftarKaryawan", karyawanService.getActiveKaryawansKoor(idCabang));
        return "cabang/edit-koordinator-cabang";
    }

    @JsonView(Views.Cabang.class)
    @RequestMapping(value = "/hapus", method = RequestMethod.POST)
    private ModelAndView deleteCabangSubmit(@RequestParam("id") Integer id, ModelMap model) {
        try {
            String namaCabang = cabangService.getById(id).getNama();
            cabangService.deleteCabang(id);
            model.addAttribute("succmsg", "Cabang " + namaCabang + " berhasil dihapus !");
            return new ModelAndView("redirect:/cabang", model);
        } catch (Exception e) {
            model.addAttribute("errmsg", e.toString());
            return new ModelAndView("redirect:/cabang/profil/" + id, model);
        }
    }
}
