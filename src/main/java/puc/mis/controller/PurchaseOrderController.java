package puc.mis.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import puc.mis.dto.purchaseOrder.FormPurchaseOrderDTO;
import puc.mis.dto.purchaseOrder.FormTambahUnitPO;
import puc.mis.model.DataUnitPOModel;
import puc.mis.model.PurchaseOrderModel;
import puc.mis.model.Views;
import puc.mis.service.customer.CustomerService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.purchaseOrder.PurchaseOrderService;
import puc.mis.service.purchaseOrder.UnitPOService;
import puc.mis.service.status.StatusPOService;
import puc.mis.service.status.StatusSparepartService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping("/purchase-order")
public class PurchaseOrderController {

    private final String location = "purchaseOrder";
    @Autowired
    private PurchaseOrderService purchaseOrderService;
    @Autowired
    private KaryawanService karyawanService;
    @Autowired
    private StatusSparepartService statusSparepartService;
    @Autowired
    private StatusPOService statusPOService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private UnitPOService unitPOService;

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST)
    private String add(@ModelAttribute FormPurchaseOrderDTO purchaseOrder, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            purchaseOrderService.init(purchaseOrder);
            model.addAttribute("messageTitle", "Penambahan Purchase Order Berhasil");
            model.addAttribute("messageText", "Purchase Order dengan nomor " + purchaseOrder.getNomor() + " berhasil ditambahkan");
            model.addAttribute("messageUrl", "/purchase-order");
            model.addAttribute("messageUrlText", "List Purchase Order");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "purchaseOrder/purchase-order-failed";
        }
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST, params = {"addUnit"})
    private String addPageAddUnit(@ModelAttribute FormPurchaseOrderDTO purchaseOrderDTO, Model model) {
        try {

            if (purchaseOrderDTO.getDaftarDataUnitPO() == null) {
                purchaseOrderDTO.setDaftarDataUnitPO(new ArrayList<FormTambahUnitPO>());
            }

            purchaseOrderDTO.getDaftarDataUnitPO().add(new FormTambahUnitPO());
            model.addAttribute("purchaseOrder", purchaseOrderDTO);
            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian PO"));
            model.addAttribute("daftarUnitPO", unitPOService.getAll());
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            return "purchaseOrder/add-purchase-order";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "purchaseOrder/purchase-order-error";
        }
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST, params = {"removeUnit"})
    private String addPageRemoveUnit(@ModelAttribute FormPurchaseOrderDTO purchaseOrderDTO, HttpServletRequest req, Model model) {
        try {

            Integer rowId = Integer.valueOf(req.getParameter("removeUnit"));
            purchaseOrderDTO.getDaftarDataUnitPO().remove(rowId.intValue());

            model.addAttribute("purchaseOrder", purchaseOrderDTO);
            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian PO"));
            model.addAttribute("daftarUnitPO", unitPOService.getAll());
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            return "purchaseOrder/add-purchase-order";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "purchaseOrder/purchase-order-error";
        }
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.GET)
    private String addPage(Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            FormPurchaseOrderDTO purchaseOrderDTO = new FormPurchaseOrderDTO();
            List<FormTambahUnitPO> daftarDataUnitPO = new ArrayList<>();
            FormTambahUnitPO tambahUnitPO = new FormTambahUnitPO();
            daftarDataUnitPO.add(tambahUnitPO);
            purchaseOrderDTO.setDaftarDataUnitPO(daftarDataUnitPO);

            model.addAttribute("purchaseOrder", purchaseOrderDTO);
            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian PO"));
            model.addAttribute("daftarUnitPO", unitPOService.getAll());
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            return "purchaseOrder/add-purchase-order";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "purchaseOrder/purchase-order-failed";
        }
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST)
    private String edit(@ModelAttribute FormPurchaseOrderDTO purchaseOrder, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            purchaseOrderService.update(purchaseOrder);
            model.addAttribute("messageTitle", "Pengubahan Purchase Order Berhasil");
            model.addAttribute("messageText", "Purchase Order dengan nomor " + purchaseOrder.getNomor() + " berhasil diubah");
            model.addAttribute("messageUrl", "/purchase-order");
            model.addAttribute("messageUrlText", "List Purchase Order");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "purchaseOrder/purchase-order-failed";
        }
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST, params = {"addUnit"})
    private String editPageAddUnit(@ModelAttribute FormPurchaseOrderDTO purchaseOrderDTO, Model model) {
        try {

            if (purchaseOrderDTO.getDaftarDataUnitPO() == null) {
                purchaseOrderDTO.setDaftarDataUnitPO(new ArrayList<FormTambahUnitPO>());
            }

            purchaseOrderDTO.getDaftarDataUnitPO().add(new FormTambahUnitPO());
            model.addAttribute("purchaseOrder", purchaseOrderDTO);
            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian PO"));
            model.addAttribute("daftarUnitPO", unitPOService.getAll());
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            model.addAttribute("daftarStatusPO", statusPOService.getAll());
            model.addAttribute("daftarStatusSparepart", statusSparepartService.getAll());
            return "purchaseOrder/edit-purchase-order";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "purchaseOrder/purchase-order-error";
        }
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST, params = {"removeUnit"})
    private String editPageRemoveUnit(@ModelAttribute FormPurchaseOrderDTO purchaseOrderDTO, HttpServletRequest req, Model model) {
        try {

            Integer rowId = Integer.valueOf(req.getParameter("removeUnit"));
            purchaseOrderDTO.getDaftarDataUnitPO().remove(rowId.intValue());

            model.addAttribute("purchaseOrder", purchaseOrderDTO);
            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian PO"));
            model.addAttribute("daftarUnitPO", unitPOService.getAll());
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            model.addAttribute("daftarStatusPO", statusPOService.getAll());
            model.addAttribute("daftarStatusSparepart", statusSparepartService.getAll());
            return "purchaseOrder/edit-purchase-order";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "purchaseOrder/purchase-order-error";
        }
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.GET)
    private String editPage(@RequestParam("id") Integer idPO, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            PurchaseOrderModel purchaseOrder = purchaseOrderService.getById(idPO);
            FormPurchaseOrderDTO purchaseOrderDTO = new FormPurchaseOrderDTO();
            List<FormTambahUnitPO> daftarDataUnitPO = new ArrayList<>();
            FormTambahUnitPO tambahUnitPO;

            for (DataUnitPOModel dataUnitPO : purchaseOrder.getDaftarUnit()) {
                tambahUnitPO = new FormTambahUnitPO();
                tambahUnitPO.setId(dataUnitPO.getId());
                tambahUnitPO.setJumlahDipesan(dataUnitPO.getJumlahDipesan());
                tambahUnitPO.setUnitPOId(dataUnitPO.getUnitPo().getId());
                daftarDataUnitPO.add(tambahUnitPO);
            }

            purchaseOrderDTO.setId(purchaseOrder.getId());
            purchaseOrderDTO.setDaftarDataUnitPO(daftarDataUnitPO);
            purchaseOrderDTO.setNomor(purchaseOrder.getNomor());
            purchaseOrderDTO.setAlamatPemasangan(purchaseOrder.getAlamatPemasangan());
            purchaseOrderDTO.setCustomerId(purchaseOrder.getCustomer().getId());
            purchaseOrderDTO.setJenisPekerjaan(purchaseOrder.getJenisPekerjaan());
            purchaseOrderDTO.setKeterangan(purchaseOrder.getKeterangan());
            purchaseOrderDTO.setNomorFaktur(purchaseOrder.getNomorFaktur());
            purchaseOrderDTO.setPenanggungjawabId(purchaseOrder.getPenanggungjawab().getUuid());
            purchaseOrderDTO.setStatusPOId(purchaseOrder.getStatusPo().getId());
            purchaseOrderDTO.setStatusSparepartId(purchaseOrder.getStatusSparepart().getId());
            if (purchaseOrder.getTanggalPemasanganSparepart() != null) {
                purchaseOrderDTO.setTanggalPemasanganSparepart(purchaseOrder.getTanggalPemasanganSparepart().toString());
            }
            purchaseOrderDTO.setTanggalPengirimanSparepart(purchaseOrder.getTanggalPengirimanSparepart().toString());
            purchaseOrderDTO.setTanggalTerima(purchaseOrder.getTanggalTerima().toString());
            model.addAttribute("purchaseOrder", purchaseOrderDTO);
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("daftarUnitPO", unitPOService.getAll());
            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian PO"));
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            model.addAttribute("daftarStatusPO", statusPOService.getAll());
            model.addAttribute("daftarStatusSparepart", statusSparepartService.getAll());
            return "purchaseOrder/edit-purchase-order";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "purchaseOrder/purchase-order-failed";
        }
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "", method = RequestMethod.GET)
    private String getAllPurchaseOrder(Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            List<PurchaseOrderModel> daftarPurchaseOrder = purchaseOrderService.getAll();
            model.addAttribute("daftarPurchaseOrder", daftarPurchaseOrder);
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            return "purchaseOrder/get-all-purchase-order";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "purchaseOrder/purchase-order-failed";
        }
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/hapus", method = RequestMethod.POST)
    private String deletePurchaseOrder(@RequestParam("id") String id, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            purchaseOrderService.deleteById(Integer.parseInt(id));
            model.addAttribute("messageTitle", "Penghapusan Purchase Order Berhasil");
            model.addAttribute("messageText", "Purchase Order dengan id " + id + " berhasil dihapus");
            model.addAttribute("messageUrl", "/purchase-order");
            model.addAttribute("messageUrlText", "List Purchase Order");
            return "success";
        } catch (Exception e) {
            return "redirect:/";
        }
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/profil/{idPO}", method = RequestMethod.GET)
    private String profilePage(@PathVariable Integer idPO, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        model.addAttribute("purchaseOrder", purchaseOrderService.getById(idPO));
        model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
        model.addAttribute("daftarUnitPO", purchaseOrderService.getById(idPO).getDaftarUnit());
        model.addAttribute("daftarBeritaAcaraPO", purchaseOrderService.getById(idPO).getDaftarBeritaAcara());
        return "purchaseOrder/profile";
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/ubah-status-sparepart", method = RequestMethod.POST)
    private String editStatusSparepart(@ModelAttribute FormPurchaseOrderDTO purchaseOrder, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            purchaseOrderService.updateStatusSparepart(purchaseOrder);
            model.addAttribute("messageTitle", "Pengubahan Status Sparepart PO Berhasil");
            model.addAttribute("messageText", "Status Sparepart PO dengan nomor " + purchaseOrder.getNomor() + " berhasil diubah");
            model.addAttribute("messageUrl", "/purchase-order");
            model.addAttribute("messageUrlText", "List Purchase Order");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "purchaseOrder/purchase-order-failed";
        }
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/ubah-status-sparepart", method = RequestMethod.GET)
    private String editStatusSparepartPage(@RequestParam("id") Integer idPO, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("purchaseOrder", purchaseOrderService.getById(idPO));
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            model.addAttribute("daftarStatusPO", statusPOService.getAll());
            model.addAttribute("daftarStatusSparepart", statusSparepartService.getAll());
            return "purchaseOrder/edit-status-sparepart-po";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "purchaseOrder/purchase-order-failed";
        }
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/batalkan-status-po", method = RequestMethod.POST)
    private String editStatusPo(@ModelAttribute FormPurchaseOrderDTO purchaseOrder, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            purchaseOrderService.cancelStatusPO(purchaseOrder);
            model.addAttribute("messageTitle", "Pembatalan Status PO Berhasil");
            model.addAttribute("messageText", "Status PO dari PO dengan id " + purchaseOrder.getId() + " berhasil dibatalkan");
            model.addAttribute("messageUrl", "/purchase-order");
            model.addAttribute("messageUrlText", "Daftar Purchase Order");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "purchaseOrder/purchase-order-failed";
        }
    }

    @JsonView(Views.PurchaseOrder.class)
    @RequestMapping(value = "/ubah-status-po", method = RequestMethod.GET)
    private String editStatusPo(@RequestParam("id") Integer idPO, Model model) {
        model.addAttribute("location", location);
        model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
        try {
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("purchaseOrder", purchaseOrderService.getById(idPO));
            model.addAttribute("daftarKaryawan", karyawanService.getByRole("Bagian PO"));
            model.addAttribute("daftarCustomer", customerService.getCustomerInCabang(karyawanService.getUserLogin().getCabang().getId()));
            model.addAttribute("daftarStatusPO", statusPOService.getAll());
            model.addAttribute("daftarStatusSparepart", statusSparepartService.getAll());
            return "purchaseOrder/edit-status-po";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "purchaseOrder/purchase-order-failed";
        }
    }
}
