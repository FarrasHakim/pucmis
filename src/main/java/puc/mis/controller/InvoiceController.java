package puc.mis.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import puc.mis.dto.invoice.FormInvoiceDTO;
import puc.mis.model.InvoiceModel;
import puc.mis.model.Views;
import puc.mis.service.invoice.InvoiceService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.status.StatusInvoiceService;

import java.util.List;

@Controller
@RequestMapping("/invoice")
public class InvoiceController {
    @Autowired
    private KaryawanService karyawanService;
    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private StatusInvoiceService statusInvoiceService;

    @JsonView(Views.Invoice.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST)
    private String add(@ModelAttribute FormInvoiceDTO invoiceDTO, Model model) {
        try {
            invoiceService.save(invoiceDTO);
            model.addAttribute("messageTitle", "Penambahan Invoice Berhasil");
            model.addAttribute("messageText", "Invoice dengan nomor " + invoiceDTO.getNomor() + " berhasil ditambahkan");
            model.addAttribute("messageUrl", "/invoice");
            model.addAttribute("messageUrlText", "List Invoice");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "invoice/invoice-error";
        }
    }

    @JsonView(Views.Invoice.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.GET)
    private String addPage(Model model) {
        try {
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("daftarStatusInvoice", statusInvoiceService.getAll());
            return "invoice/add-invoice";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "invoice/invoice-error";
        }
    }

    @JsonView(Views.Invoice.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST)
    private String edit(@ModelAttribute FormInvoiceDTO invoiceDTO, Model model) {
        try {
            invoiceService.update(invoiceDTO);
            model.addAttribute("messageTitle", "Pengubahan Invoice Berhasil");
            model.addAttribute("messageText", "Invoice dengan nomor " + invoiceDTO.getNomor() + " berhasil diubah");
            model.addAttribute("messageUrl", "/invoice");
            model.addAttribute("messageUrlText", "List Invoice");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "invoice/invoice-error";
        }
    }

    @JsonView(Views.Invoice.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.GET)
    private String editPage(@RequestParam("id") Integer idPO, Model model) {
        try {
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("invoice", invoiceService.getById(idPO));
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            model.addAttribute("daftarStatusInvoice", statusInvoiceService.getAll());
            return "invoice/edit-invoice";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "invoice/invoice-error";
        }
    }

    @JsonView(Views.Invoice.class)
    @RequestMapping(value = "", method = RequestMethod.GET)
    private String getAll(Model model) {
        try {
            List<InvoiceModel> daftarInvoice = invoiceService.getAll();
            model.addAttribute("daftarInvoice", daftarInvoice);
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            return "invoice/get-all-invoice";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "invoice/invoice-error";
        }
    }

    @JsonView(Views.Invoice.class)
    @RequestMapping(value = "/hapus", method = RequestMethod.POST)
    private String delete(@RequestParam("id") String id, Model model) {
        try {
            invoiceService.deleteById(Integer.parseInt(id));
            model.addAttribute("messageTitle", "Penghapusan Invoice Berhasil");
            model.addAttribute("messageText", "Invoice dengan id " + id + " berhasil dihapus");
            model.addAttribute("messageUrl", "/invoice");
            model.addAttribute("messageUrlText", "List Invoice");
            return "success";
        } catch (Exception e) {
            return "redirect:/";
        }
    }

    @JsonView(Views.Invoice.class)
    @RequestMapping(value = "/profil/{id}", method = RequestMethod.GET)
    private String profilePage(@PathVariable Integer id, Model model) {
        model.addAttribute("invoice", invoiceService.getById(id));
        model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
        return "invoice/profile";
    }

    @JsonView(Views.Invoice.class)
    @RequestMapping(value = "/ubah-status", method = RequestMethod.POST)
    private String editStatus(@ModelAttribute FormInvoiceDTO invoiceDTO, Model model) {
        try {
            invoiceService.updateStatus(invoiceDTO);
            model.addAttribute("messageTitle", "Pengubahan Status Invoice Berhasil");
            model.addAttribute("messageText", "Status Invoice dengan id " + invoiceDTO.getId() + " berhasil diubah");
            model.addAttribute("messageUrl", "/invoice");
            model.addAttribute("messageUrlText", "List Invoice");
            return "success";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "invoice/invoice-error";
        }
    }

    @JsonView(Views.Invoice.class)
    @RequestMapping(value = "/ubah-status", method = RequestMethod.GET)
    private String editStatusPage(@RequestParam("id") Integer idPO, Model model) {
        try {
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("invoice", invoiceService.getById(idPO));
            model.addAttribute("daftarKaryawan", karyawanService.getAllExceptAdmin());
            model.addAttribute("daftarStatusInvoice", statusInvoiceService.getAll());
            return "invoice/edit-status-invoice";
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.toString());
            return "invoice/invoice-error";
        }
    }
}
