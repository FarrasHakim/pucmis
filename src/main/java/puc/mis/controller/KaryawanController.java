package puc.mis.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import puc.mis.dto.karyawan.FormKaryawanDTO;
import puc.mis.model.KaryawanModel;
import puc.mis.model.Views;
import puc.mis.service.cabang.CabangService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.log.LogService;
import puc.mis.service.role.RoleService;
import puc.mis.service.status.StatusKaryawanService;

import java.util.List;

@Controller
@RequestMapping("/karyawan")
public class KaryawanController {

    @Autowired
    private KaryawanService karyawanService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private CabangService cabangService;

    @Autowired
    private LogService logService;

    @Autowired
    private StatusKaryawanService statusKaryawanService;

    @JsonView(Views.Karyawan.class)
    @RequestMapping(value = "", method = RequestMethod.GET)
    private String allKaryawan(@RequestParam(required = false) String newKaryawan,
                               @RequestParam(required = false) String succmsg, Model model) {
        List<KaryawanModel> daftarKaryawan = karyawanService.getAll();
        model.addAttribute("succmsg", succmsg);
        model.addAttribute("newKaryawan", newKaryawan);
        model.addAttribute("daftarKaryawan", daftarKaryawan);
        model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
        return "karyawan/karyawans";
    }

    @JsonView(Views.Karyawan.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.POST)
    private ModelAndView addUserSubmit(@ModelAttribute FormKaryawanDTO karyawan, @RequestParam String source, ModelMap model) {
        String password = karyawan.getPassword();
        try {
            karyawanService.addKaryawan(karyawan);
            model.addAttribute("newKaryawan", karyawan.getNama());
            return new ModelAndView("redirect:" + source, model);
        } catch (IllegalArgumentException e) {
            model.addAttribute("errmsg", e.toString());
            model.addAttribute("source", source);
            model.addAttribute("idCabang", karyawan.getCabang());
            return new ModelAndView("redirect:/karyawan/tambah", model);
        }
    }

    @JsonView(Views.Karyawan.class)
    @RequestMapping(value = "/tambah", method = RequestMethod.GET)
    public String addUserPage(@RequestParam(required = false) Integer idCabang,
                              @RequestParam(required = false) String errmsg, @RequestParam String source, Model model) {
        model.addAttribute("source", source);
        model.addAttribute("errmsg", errmsg);
        model.addAttribute("cabang", (idCabang != null) ? cabangService.getById(idCabang) : null);
        model.addAttribute("listRole", roleService.findAll());
        model.addAttribute("listCabang", cabangService.getAll());
        model.addAttribute("idCabang", (idCabang != null) ? idCabang : "");
        return "karyawan/add-user";
    }

    @JsonView(Views.Karyawan.class)
    @RequestMapping(value = "/pindah-cabang", method = RequestMethod.POST)
    private ModelAndView changeCabangSubmit(@ModelAttribute FormKaryawanDTO karyawan, ModelMap model) {
        try {
            karyawanService.changeCabang(karyawan.getUuid(), karyawan.getCabang());
            model.addAttribute("succmsg", "dipindahkan");
            return new ModelAndView("redirect:/karyawan/profil/" + karyawan.getUsername(), model);
        } catch (IllegalArgumentException e) {
            model.addAttribute("errmsg", e.toString());
            model.addAttribute("username", karyawan.getUsername());
            return new ModelAndView("redirect:/karyawan/pindah-cabang", model);
        }
    }

    @JsonView(Views.Karyawan.class)
    @RequestMapping(value = "/pindah-cabang", method = RequestMethod.GET)
    public String changeCabangPage(@RequestParam("username") String username, @RequestParam(required = false) String errmsg, Model model) {
        model.addAttribute("karyawan", karyawanService.getKaryawanByUsername(username));
        model.addAttribute("listCabang", cabangService.getAll());
        model.addAttribute("errmsg", errmsg);
        return "karyawan/change-cabang";
    }

    @JsonView(Views.Karyawan.class)
    @RequestMapping(value = "/ganti-jabatan", method = RequestMethod.POST)
    private ModelAndView changeRoleSubmit(@ModelAttribute FormKaryawanDTO karyawan, ModelMap model) {
        try {
            karyawanService.changeRole(karyawan.getUuid(), karyawan.getRole());
            model.addAttribute("succmsg", "berganti jabatan");
            return new ModelAndView("redirect:/karyawan/profil/" + karyawan.getUsername(), model);
        } catch (IllegalArgumentException e) {
            model.addAttribute("errmsg", e.toString());
            model.addAttribute("username", karyawan.getUsername());
            return new ModelAndView("redirect:/karyawan/ganti-jabatan", model);
        }
    }

    @JsonView(Views.Karyawan.class)
    @RequestMapping(value = "/ganti-jabatan", method = RequestMethod.GET)
    public String changeRolePage(@RequestParam("username") String username, @RequestParam(required = false) String errmsg, Model model) {
        model.addAttribute("karyawan", karyawanService.getKaryawanByUsername(username));
        model.addAttribute("listRole", roleService.findAll());
        model.addAttribute("errmsg", errmsg);
        return "karyawan/change-role";
    }

    @JsonView(Views.Karyawan.class)
    @RequestMapping(value = "/profil/{username}", method = RequestMethod.GET)
    public String profilePage(@PathVariable String username, @RequestParam(required = false) String succmsg,
                              @RequestParam(required = false) String errmsg, Model model) {
        try {
            KaryawanModel karyawan = karyawanService.getKaryawanByUsername(username);
            model.addAttribute("succmsg", succmsg);
            model.addAttribute("errmsg", errmsg);
            model.addAttribute("karyawan", karyawan);
            model.addAttribute("userLogin", karyawanService.getUserLogin());
            model.addAttribute("logs", logService.get(karyawan.getUuid(), "Karyawan"));
            return "karyawan/profile";
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Karyawan tidak ditemukan");
        }
    }

    @JsonView(Views.Karyawan.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.POST)
    private ModelAndView editUserSubmit(@ModelAttribute FormKaryawanDTO karyawan, ModelMap model) {
        String password = karyawan.getPassword();
        try {
            karyawanService.changeKaryawan(karyawan);
            model.addAttribute("succmsg", "diubah");
            return new ModelAndView("redirect:/karyawan/profil/" + karyawan.getUsername(), model);
        } catch (IllegalArgumentException e) {
            model.addAttribute("errmsg", e.toString());
            model.addAttribute("username", karyawan.getUsername());
            return new ModelAndView("redirect:/karyawan/ubah", model);
        }
    }

    @JsonView(Views.Karyawan.class)
    @RequestMapping(value = "/ubah", method = RequestMethod.GET)
    public String editUserPage(@RequestParam("username") String username, @RequestParam(required = false) String errmsg, Model model) {
        model.addAttribute("errmsg", errmsg);
        model.addAttribute("karyawan", karyawanService.getKaryawanByUsername(username));
        model.addAttribute("listRole", roleService.findAll());
        model.addAttribute("listCabang", cabangService.getAll());
        model.addAttribute("listStatus", statusKaryawanService.getAll());
        model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
        return "karyawan/edit-user";
    }

    @JsonView(Views.Karyawan.class)
    @RequestMapping(value = "/hapus", method = RequestMethod.POST)
    private ModelAndView deleteUserSubmit(@RequestParam("uuid") String uuid, ModelMap model) {

        try {
            String namaKaryawan = karyawanService.getKaryawanById(uuid).getNama();
            karyawanService.deleteKaryawan(uuid);
            model.addAttribute("succmsg", "Karyawan " + namaKaryawan + " berhasil dihapus !");
            return new ModelAndView("redirect:/karyawan", model);
        } catch (Exception e) {
            String username = karyawanService.getKaryawanById(uuid).getUsername();
            model.addAttribute("errmsg", e.toString());
            return new ModelAndView("redirect:/karyawan/profil/" + username, model);
        }
    }

}