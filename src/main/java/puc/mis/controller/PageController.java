package puc.mis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import puc.mis.model.BacaNotifikasiModel;
import puc.mis.model.KaryawanModel;
import puc.mis.service.area.AreaService;
import puc.mis.service.cabang.CabangService;
import puc.mis.service.customer.CustomerService;
import puc.mis.service.karyawan.KaryawanService;
import puc.mis.service.notifikasi.BacaNotifikasiService;

import java.util.List;
import java.util.NoSuchElementException;

@Controller
public class PageController {

    @Autowired
    private AreaService areaService;

    @Autowired
    private CabangService cabangService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private KaryawanService karyawanService;

    @Autowired
    private BacaNotifikasiService bacaNotifikasiService;

    @RequestMapping("/")
    public String home(Model model) {
        try {
            KaryawanModel user = karyawanService.getUserLogin();
            List<BacaNotifikasiModel> daftarNotifikasi = bacaNotifikasiService.getDaftarBacaNotif(user);
            model.addAttribute("daftarNotifikasi", daftarNotifikasi);
            model.addAttribute("daftarArea", areaService.getAll());
            model.addAttribute("daftarCabang", cabangService.getAll());
            model.addAttribute("daftarCustomer", customerService.getAll());
            model.addAttribute("daftarKaryawan", karyawanService.getActiveKaryawans());
            model.addAttribute("role", karyawanService.getUserLogin().getRole().getNama());
            model.addAttribute("userName", karyawanService.getUserLogin().getUsername());
            model.addAttribute("location", "home");
            return "dashboard";
        } catch (NoSuchElementException e) {
            return "home";
        }
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/base")
    public String base(Model model) {
        return "base";
    }

}
