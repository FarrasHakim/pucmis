package puc.mis.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import puc.mis.model.KaryawanModel;
import puc.mis.repository.KaryawanDb;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private KaryawanDb karyawanDb;

    @Override
    public CustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        KaryawanModel user = karyawanDb.findByUsername(username).get();

        return new CustomUserDetails(user.getNama(), user.getUsername(), user.getPassword(), user.getRole().getNama());
    }
}
