package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.StatusInvoiceModel;

@Repository
public interface StatusInvoiceDb extends JpaRepository<StatusInvoiceModel, Integer> {
}
