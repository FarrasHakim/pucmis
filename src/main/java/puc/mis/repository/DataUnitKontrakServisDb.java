package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import puc.mis.model.DataUnitKontrakServisModel;

public interface DataUnitKontrakServisDb extends JpaRepository<DataUnitKontrakServisModel, Integer> {
}
