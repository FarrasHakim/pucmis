package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.RoleModel;

import java.util.Optional;

@Repository
public interface RoleDb extends JpaRepository<RoleModel, Integer> {
    Optional<RoleModel> findByNama(String nama);
}
