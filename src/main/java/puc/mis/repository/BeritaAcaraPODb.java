package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.BeritaAcaraPOModel;

@Repository
public interface BeritaAcaraPODb extends JpaRepository<BeritaAcaraPOModel, Integer> {
}
