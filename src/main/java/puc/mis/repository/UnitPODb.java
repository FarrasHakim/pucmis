package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import puc.mis.model.UnitPOModel;

public interface UnitPODb extends JpaRepository<UnitPOModel, Integer> {
}
