package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.StatusJadwalServisModel;

@Repository
public interface StatusJadwalServisDb extends JpaRepository<StatusJadwalServisModel, Integer> {
}
