package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.BacaNotifikasiModel;
import puc.mis.model.KaryawanModel;

import java.util.List;

@Repository
public interface BacaNotifikasiDb extends JpaRepository<BacaNotifikasiModel, Integer> {
    List<BacaNotifikasiModel> findAllByKaryawanOrderByNotifikasiDesc(KaryawanModel karyawanModel);
}
