package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.StatusPOModel;

import java.util.Optional;

@Repository
public interface StatusPODb extends JpaRepository<StatusPOModel, Integer> {
    Optional<StatusPOModel> findByStatus(String status);
}
