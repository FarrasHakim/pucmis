package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.LogModel;

import java.util.List;

@Repository
public interface LogDb extends JpaRepository<LogModel, Integer> {
    List<LogModel> findAllByIdSumberAndSumber(String idSumber, String sumber);
}
