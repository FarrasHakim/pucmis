package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.PurchaseOrderModel;

import java.util.List;

@Repository
public interface PurchaseOrderDb extends JpaRepository<PurchaseOrderModel, Integer> {
    List<PurchaseOrderModel> findAll();
}
