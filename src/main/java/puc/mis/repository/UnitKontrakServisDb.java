package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import puc.mis.model.UnitKontrakServisModel;

public interface UnitKontrakServisDb extends JpaRepository<UnitKontrakServisModel, Integer> {
}
