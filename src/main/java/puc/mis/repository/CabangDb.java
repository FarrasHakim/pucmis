package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.CabangModel;

import java.util.Optional;

@Repository
public interface CabangDb extends JpaRepository<CabangModel, Integer> {
    Optional<CabangModel> findByNama(String nama);
}
