package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.BeritaAcaraServisModel;
import puc.mis.model.KontrakServisModel;

import java.util.List;

@Repository
public interface BeritaAcaraServisDb extends JpaRepository<BeritaAcaraServisModel, Integer> {
    List<BeritaAcaraServisModel> findAllByKontrakServis(KontrakServisModel kontrakServis);
}
