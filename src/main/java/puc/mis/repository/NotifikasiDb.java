package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.NotifikasiModel;

@Repository
public interface NotifikasiDb extends JpaRepository<NotifikasiModel, Integer> {
}
