package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.AreaModel;
import puc.mis.model.CabangModel;

import java.util.List;

@Repository
public interface AreaDb extends JpaRepository<AreaModel, Integer> {
    List<AreaModel> findAllByCabang(CabangModel cabang);
}
