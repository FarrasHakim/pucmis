package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.CabangModel;
import puc.mis.model.CustomerModel;

import java.util.List;

@Repository
public interface CustomerDb extends JpaRepository<CustomerModel, Integer> {
    List<CustomerModel> findAllByCabang(CabangModel cabang);
}
