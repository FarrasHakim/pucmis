package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.StatusBeritaAcaraModel;

@Repository
public interface StatusBeritaAcaraDb extends JpaRepository<StatusBeritaAcaraModel, Integer> {
}
