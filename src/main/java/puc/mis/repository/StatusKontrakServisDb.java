package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.StatusKontrakServisModel;

@Repository
public interface StatusKontrakServisDb extends JpaRepository<StatusKontrakServisModel, Integer> {
}
