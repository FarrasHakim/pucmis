package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.InvoiceModel;

@Repository
public interface InvoiceDb extends JpaRepository<InvoiceModel, Integer> {
}
