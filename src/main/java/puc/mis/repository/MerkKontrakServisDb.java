package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import puc.mis.model.MerkKontrakServisModel;

public interface MerkKontrakServisDb extends JpaRepository<MerkKontrakServisModel, Integer> {
}
