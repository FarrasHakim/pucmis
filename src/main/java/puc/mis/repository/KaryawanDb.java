package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.CabangModel;
import puc.mis.model.KaryawanModel;
import puc.mis.model.RoleModel;
import puc.mis.model.StatusKaryawanModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface KaryawanDb extends JpaRepository<KaryawanModel, Integer> {
    Optional<KaryawanModel> findByUuid(String uuid);

    Optional<KaryawanModel> findByUsername(String username);

    List<KaryawanModel> findAllByRole(RoleModel role);

    List<KaryawanModel> findAllByRoleAndCabang(RoleModel role, CabangModel cabang);

    List<KaryawanModel> findAllByStatus(StatusKaryawanModel status);

    List<KaryawanModel> findAllByCabangAndStatus(CabangModel cabang, StatusKaryawanModel status);

    List<KaryawanModel> findAllByRoleNotLike(RoleModel role);
}
