package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.StatusKaryawanModel;

import java.util.Optional;

@Repository
public interface StatusKaryawanDb extends JpaRepository<StatusKaryawanModel, Integer> {
    Optional<StatusKaryawanModel> findByStatus(String status);
}
