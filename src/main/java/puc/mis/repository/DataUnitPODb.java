package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import puc.mis.model.DataUnitPOModel;

public interface DataUnitPODb extends JpaRepository<DataUnitPOModel, Integer> {
}
