package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.StatusSparepartModel;

@Repository
public interface StatusSparepartDb extends JpaRepository<StatusSparepartModel, Integer> {
}
