package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.JadwalServisModel;
import puc.mis.model.KontrakServisModel;

import java.util.List;

@Repository
public interface JadwalServisDb extends JpaRepository<JadwalServisModel, Integer> {
    List<JadwalServisModel> findAllByKontrakServis(KontrakServisModel kontrakServis);
}
