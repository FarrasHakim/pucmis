package puc.mis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import puc.mis.model.KontrakServisModel;

@Repository
public interface KontrakServisDb extends JpaRepository<KontrakServisModel, Integer> {
}
